require 'sketchup.rb'
require "dl"
require "dl/import"
require "dl/struct"




#make this version global for now.Needed to allow legacy scripted joints to work.
def getKeyState(key)
    MSketchyPhysics3::getKeyState(key)    
end

#GetKeys.getKeyState(VK_LCONTROL) whenever you want to check for control key and
#GetKeys.getKeyState(VK_LSHIFT) whenever you want to check for shift

module MSketchyPhysics3

require 'sketchyphysics3/virtualkeycodes.rb'


if(PLATFORM=="i386-mswin32")
    savePath=$LOAD_PATH
    $LOAD_PATH[0,0]=Sketchup.find_support_file("plugins")+"/SketchyPhysics3/"
    require 'Win32API' #needed to call the user32.dll
    $LOAD_PATH.delete_at(0)
    
    $win32GetKeyStateFunc=Win32API.new("user32.dll", "GetKeyState", ['N'], 'N')

    def self.getKeyState(key)
        return (($win32GetKeyStateFunc.call(key)>>16)!=0)
    end
else
      extend DL::Importable
      dlload(Sketchup.find_support_file("GetKeys.dylib","plugins/SketchyPhysics3/"))
      extern "BOOL TestForKeyDown(short)"
    	
      def self.getKeyState(key)
          if testForKeyDown(key) then
#              puts(key.to_s + "key is down")
              return(true)
          else
#              puts(key.to_s + "key is not down")
              return(false)
          end
      end
end
end #module MSketchyPhysics3

module JoyInput
   extend DL::Importable

   DJOYSTATE2 = struct [
    "int lX",
    "int lY",
    "int lZ",
    "int lRx",
    "int lRy",
    "int lRz",
    "int rglSlider[2]",
    "int rgdwPOV[4]",
    "char rgbButtons[128]",
    "int lVX",
    "int lVY",
    "int lVZ",
    "int lVRx",
    "int lVRy",
    "int lVRz",
    "int rglVSlider[2]",
    "int lAX",
    "int lAY",
    "int lAZ",
    "int lARx",
    "int lARy",
    "int lARz",
    "int rglASlider[2]",
    "int lFX",
    "int lFY",
    "int lFZ",
    "int lFRx",
    "int lFRy",
    "int lFRz",
    "int rglFSlider[2]"]

   
   @curJoyState=DJOYSTATE2.malloc
   #puts @curJoyState
   def self.state()
      return @curJoyState
   end
   
   if(PLATFORM=="i386-mswin32")
      dlload(Sketchup.find_support_file("WinInput.dll","plugins/SketchyPhysics3/"))
   else
      dlload(Sketchup.find_support_file("MacInput.dylib","plugins/SketchyPhysics3/"))
   end      
   
   #extern "int initDirectInput()"
   #extern "int readJoystick(float *)"
   #extern "void freeDirectInput()"

   def self.updateInput()
      readJoystick(@curJoyState.to_ptr)
      return @curJoyState
   end

   extern "int initInput()"
   extern "int readJoystick(void*)"
   extern "void freeInput()"
end #module JoyInput
