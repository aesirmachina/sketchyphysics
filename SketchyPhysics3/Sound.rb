require 'sketchup.rb'
require "dl"
require "dl/import"
require "dl/struct"


def loadSound()
    puts SDL.sDL_Init(0x00000010)
    puts SDL.mix_OpenAudio(22050, 0x8010,  2, 4096)
    
    #f=File.new("c:\\temp\\lame\\angstrom_c1.mp3","rb")
    #data=f.read
    #$cSound=SDL.mix_LoadWAV_RW(SDL.sDL_RWFromMem(data.to_ptr,data.length),0)
    #f.close
    
    $cSound=SDL.mix_LoadWAV_RW(SDL.sDL_RWFromFile("c:\\temp\\lame\\angstrom_c1.wav", "rb"),0)
    $dSound=SDL.mix_LoadWAV_RW(SDL.sDL_RWFromFile("c:\\temp\\lame\\angstrom_d1.mp3", "rb"),0)
end
def testSound
    #f=SDL.sDL_RWFromFile("c:\\temp\\lame\\angstrom_c1.wav", "rb")
    #rw=SDL.sDL_RWFromMem(data.to_ptr,data.length)
    #snd=SDL.mix_LoadWAV_RW(rw,1);
    #puts SDL.sDL_GetError(); 
    SDL.mix_PlayChannelTimed(-1, $cSound, 0,-1);
    SDL.mix_PlayChannelTimed(-1, $dSound, 0,-1);
end    

module SDLWrapper
    #extend DL::Importable
    #dlload(Sketchup.find_support_file("SDLWrapper.dll","plugins/"))
    #extern "void SDLPlaySound(char*)"

end
module SDL
    if(PLATFORM=="i386-mswin32")    

        extend DL::Importable
        dlload(Sketchup.find_support_file("SDL.dll","plugins/Sketchyphysics3/sdl/"))
        extern "int SDL_Init(int)"
        extern "void* SDL_RWFromFile(char*, char*)"
        extern "char* SDL_GetError()"
        
        dlload(Sketchup.find_support_file("SDL_mixer.dll","plugins/Sketchyphysics3/sdl/"))
        extern "int Mix_OpenAudio(int,int,int,int)"
        extern "void* Mix_LoadWAV_RW(void*,int)"
        extern "int Mix_PlayChannelTimed(int, void*,int,int)"
        extern "void* SDL_RWFromMem(void *,int)"
    end
end 

def embedSound(fileName)
    name=File.basename(fileName)
    f=File.new(fileName,"rb")
    data=f.read
    f.close
    data=data.unpack("C*")
    Sketchup.active_model.set_attribute("SPSounds",name,data)
end

def exportEmbeddedSounds()
    dict=Sketchup.active_model.attribute_dictionary("SPSounds")
    dict.each{|name,data|
        if(data!=nil)
            data=data.pack("C*")
            outpath=Sketchup.find_support_file("plugins/SketchyPhysics3/Sounds/cache")
            outname=outpath+'/'+name
            puts ("writting:"+outname)
            f=File.new(outname,"wb")
            f.write(data)
            f.close
#            UI.play_sound(outname)
        end
    }
    
end

def playEmbedSound(name)
    
    name=File.basename(name, ".wav")
    
    data=Sketchup.active_model.get_attribute("SPSounds",name,nil)

    if(data!=nil)
        data=data.pack("C*")
        outpath=Sketchup.find_support_file("plugins/SketchyPhysics3/Sounds/cache")
        outname=outpath+'/'+name
        puts ("writting:"+outname)
        f=File.new(outname,"wb")
        f.write(data)
        f.close
        UI.play_sound(outname)
    end
end