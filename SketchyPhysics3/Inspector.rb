require 'sketchup.rb'

#~ $LOAD_PATH[0,0]=Sketchup.find_support_file("plugins")+"/SketchyPhysics3/cgi"
#~ require "cgi"
#~ $LOAD_PATH.delete_at(0)

module MSketchyPhysics3

def array_to_HTMLselect()
    html='<select>'
    self.each{|ae|
        html+='<OPTION>'+ae.to_s
    }
    html+='</select>'
    return html 
 end  
class PhysicsAppObserver <  Sketchup::AppObserver
    
    def createSelectionObserver()
        puts("createSelectionObserver")
        $physicsSelectionObserver=PhysicsSelectionObserver.new()
        Sketchup.active_model.selection.add_observer($physicsSelectionObserver)
        $physicsSessionID=rand(100000000)
$physicsSessionID=0
        $UniqueObjects=Hash.new()        
    end
    def initialize
        createSelectionObserver()
    end
    def onNewModel(xx)
        puts("new model"+xx.to_s)
        createSelectionObserver()
    end
    def onOpenModel(xx)
        puts("open model"+xx.to_s)
        createSelectionObserver()
    end
end

class PhysicsSelectionObserver <  Sketchup::SelectionObserver
    def initialize
        if($physicsInspectorDialog!=nil)
            $physicsInspectorDialog.cleanup()
        end

        $physicsInspectorDialog=PhysicsObjectInspector.new();
    end
    def onSelectionAdded (selection ,aa)
        puts('onSelectionAdded') 
        onSelectionBulkChange (selection)
	end
 	def onSelectionBulkChange (selection )
        puts('onSelectionBulkChange'+selection.length.to_s) 

        $physicsInspectorDialog.selectionChanged(selection)
	end
    def onSelectionCleared (selection )
        puts('onSelectionCleared') 
	end
    def onSelectionRemoved (selection )
        puts('onSelectionRemoved') 
	end
end
#addWatermarkText(0,0,"97<48:123456789",component="webdingtext.skp")


class SketchyPhysics
    CurrentVersion=$sketchyphysics_version_loaded.to_s
    VersionText="Requires SketchyPhysics"+ CurrentVersion+" RC1"
    
    def self.addWatermarkText(x,y,text,component="versiontext.skp",name="watermark")
        path=Sketchup.find_support_file(component ,"plugins/SketchyPhysics3/Components");
        cd=Sketchup.active_model.definitions.load(path);
        view=Sketchup.active_model.active_view
        ray=view.pickray(x,y)
        loc=ray[0]+ray[1]

        Sketchup.active_model.start_operation "Add watermark text"
        ci=Sketchup.active_model.entities.add_instance(cd,Geom::Transformation.new(loc))
        tt=ci.explode[0]
        tt.text=text
        tt.set_attribute("SketchyPhysics","name",name)
        tt.material.color="green"
        Sketchup.active_model.commit_operation 
        return tt
    end

    def self.updateWaterMark(name,newText)
       Sketchup.active_model.entities.each{|ent| 
        if(ent.get_attribute("SketchyPhysics","name",nil)==name)
            ent.text=newText
        end
        } 
    end
    
    def self.checkVersion
        version=Sketchup.active_model.get_attribute("sketchyphysics","version",nil)
        if(version==nil || version!=CurrentVersion)
            if(version==nil)
                puts "No version."     
                Sketchup.active_model.set_attribute("sketchyphysics","version",CurrentVersion)
                addWatermarkText(10,12,VersionText,component="versiontext.skp",name="VersionWaterMark")
            elsif (version!=CurrentVersion)
                puts "Converting #{version} to "+CurrentVersion
#show conversion warning
#(disabled)
keypress = UI.messagebox "This model was made with SketchyPhysics version #{version}. Your version is #{CurrentVersion}. Model may not work correctly", MB_OK, "Warning"
                Sketchup.active_model.set_attribute("sketchyphysics","version",CurrentVersion)
                updateWaterMark("VersionWaterMark",VersionText)
            end
        end
        puts "Inspecting model for errors"
        Sketchup.active_model.start_operation "Fix physics errors"
        $physicsInspectorDialog.inspectModel() 
        Sketchup.active_model.commit_operation 
        return(version)        
    end
    
    def update()
    
    end
    def updateBodies()
        @bodies=[]
        Sketchup.active_model.entities.each { | ent | 
            if (isBody(ent))
                @bodies.push(ent)
            end
            }
    end
    def updateCollision(group)
    end

    def inspectCollision(group)
        
        #detect flips and scales.
        
        shape=group.get_attribute( "SPOBJ", "shape", nil)
        return [shape],[] if(shape!=nil)

        shapes=[]
        joints=[]
        group.entities.each { | ent | 
            if (isJoint(ent))
                joints.push(ent.get_attribute("SPJOINT","name",nil))
            elsif (isShape(ent))
                shapes.push(inspectCollision(ent))
            end
        }
        
        shapes.push("default") if(shapes.length<1)
        return shapes,joints         
    end
    
end


 
class PhysicsObjectInspector 
    def isPotentialPhysicsObject(ent)
        if (ent.class== Sketchup::Group ||ent.class== Sketchup::ComponentInstance)
            if(!ent.get_attribute("SPOBJ", "ignore", false))
                return true
            end
        end
        return false	
    end

    def isShape(ent)
        if (ent.class== Sketchup::Group ||ent.class== Sketchup::ComponentInstance)
            if(!ent.get_attribute("SPOBJ", "ignore", false))
                return true
            end
        end
        return false	
    end

    def isBody(ent)
        if (ent.class== Sketchup::Group || ent.class== Sketchup::ComponentInstance && ent.parent==Sketchup.active_model)
            if(!ent.get_attribute("SPOBJ", "ignore", false))
                return true
            end
        end
        return false	
    end
    
    def isJoint(ent)
        if (ent.class== Sketchup::Group || ent.class== Sketchup::ComponentInstance && ent.parent==Sketchup.active_model)
            if(ent.get_attribute("SPJOINT","name",nil)!=nil)
                return true
            end
        end
        return false	
    end


    @selectedObject=nil
    @PhysicsInspectorDialog=nil

    def initialize
        @idToGroup= Hash.new()
        @allJoints=Hash.new()
            #unselect any non physics objects.
        Sketchup.active_model.selection.each{|ent|
            if(ent.class!=Sketchup::Group && ent.class!=Sketchup::ComponentInstance)
                Sketchup.active_model.selection.remove(ent)
            end
        }
        selectionChanged(Sketchup.active_model.selection)
        #showDialog() if(Sketchup.read_default("SketchyPhysics", "InspectorVisible",false))
        
        puts "PhysicsObjectInspector initialized"    

        
    end
    
    def cleanup()
        @PhysicsInspectorDialog.close() if(@PhysicsInspectorDialog!=nil)
        @PhysicsInspectorDialog=nil
    end



    #anytime you see a joint register it with this function.improves speed.
    def registerJoint()
        name=grp.get_attribute("SPJOINT","name",nil)
        raise if(name==nil)
        @allJoints[name]=grp
                 
    end
    def findJoint(name)
        grp=@allJoints[name]
        return grp if(grp!=nil&& !grp.deleted?)
        
        #puts "searching for joint "+name
        
        findAllJoints()
        
        grp=@allJoints[name]
        return grp
        #puts("Error locating joint:#{name}. Delete?")
        
    end
    
    def makeJointsUnique(group)
        
        puts group.parent.instances.length
        while(group.parent.instances.length>1)
            nent=group.parent.instances[1]
            nent.make_unique
                        
            nname=group.get_attribute("SPJOINT","type",nil)+nent.entityID.to_s+rand(1000).to_s
            nent.set_attribute("SPJOINT","name",nname)
            puts "new unique joint:"+nname
            nent.name=nname    
        end
    end
    def findAllJoints()
        @allJoints=Hash.new()
        Sketchup.active_model.definitions.each{ |cd|
            cd.instances.each{|ci|

                if(ci.get_attribute("SPJOINT","name",nil)!=nil)
                    jname=ci.get_attribute("SPJOINT","name",nil)
                    #confirm joint is unique
                    if(ci.parent.class!=Sketchup::Model && ci.parent.instances.length>1)
 
        #makeJointsUnique(ci)
                        
                        #~ puts "warning "+ci.parent.instances.length.to_s
                        #~ nent=ci.parent.instances[1]
                        #~ nent.make_unique
                        
                        #~ nname=ci.get_attribute("SPJOINT","type",nil)+nent.entityID.to_s+rand(1000).to_s
                        #~ nent.set_attribute("SPJOINT","name",nname)
                        #~ nent.name=nname
                        
                        #oi=ci.parent.instances[1]
                        #jname=oi.get_attribute("SPJOINT","type",nil)+oi.entityID.to_s
                        #oi.parent.instances[1].set_attribute("SPJOINT","name",jname)
                    end
                    if(@allJoints[jname]!=nil)
  #                      puts "Duplicated joint NAME #{jname} Renaming."
                        #rename joint.
  #                      jname=ci.get_attribute("SPJOINT","type",nil)+ci.entityID.to_s
   #                     ci.set_attribute("SPJOINT","name",jname)
  #                     ci.name=jname
                    end
                    @allJoints[jname]=ci

                end
            }
        }
        return @allJoints
    end
    
    def onDialogSelectJoint(id,name)
        puts id
        group=@idToGroup[id]
        
        if(group==nil)
           group=findJoint(name) 
           if(group==nil)
                puts "Error cant find joint"
                return;
            end                
        end
        
        puts group        
        inspectJointProperties(group) if(group!=nil)
            
    end

    
    def scheduleUpdate()
        if(($lastUpdateTime==nil || Time.now-$lastUpdateTime>0.25) && $dirtyField==nil)
            @PhysicsInspectorDialog.execute_script_safe("clearForm();") if(@PhysicsInspectorDialog!=nil)  
            inspectGroup(@selectedObject)
            puts "Webdialog set to"+@selectedObject.to_s
        elsif($updateTimer==nil)
            $updateTimer=UI.start_timer(0.25,true) {
                if(Time.now-$lastUpdateTime>0.25)
                    if($dirtyField!=nil)
                        puts "Webdialog is still dirty!!!!!"
                    else
                        UI.stop_timer($updateTimer)
                        $updateTimer=nil;
                        puts "Webdialog set to"+@selectedObject.to_s 
                        @PhysicsInspectorDialog.execute_script_safe("clearForm();") if(@PhysicsInspectorDialog!=nil)  
                        inspectGroup(@selectedObject)
                    end
                end
                } 
        end   
        $lastUpdateTime=Time.now()        
    end
        
    def selectionChanged(selection)
        
        #@PhysicsInspectorDialog.execute_script_safe("clearForm();") if(@PhysicsInspectorDialog!=nil)  

        return if (selection.length>1)
        grp=Sketchup.active_model.selection[0]
        return if (grp==nil)
        return if (grp.class!= Sketchup::Group &&grp.class!= Sketchup::ComponentInstance)
            

        @idToGroup[grp.entityID.to_s]=grp 
        
        @selectedObject=grp
        scheduleUpdate()  
        #inspectGroup(grp)
    end
    def toggleDialog()
        if(@PhysicsInspectorDialog==nil)
            #unselect any non physics objects.
            Sketchup.active_model.selection.each{|ent|
                if(ent.class!=Sketchup::Group && ent.class!=Sketchup::ComponentInstance)
                    Sketchup.active_model.selection.remove(ent)
                end
            }
            selectionChanged(Sketchup.active_model.selection)
            showDialog()
            #Sketchup.write_default("SketchyPhysics", "InspectorVisible",true)
        else
            #Sketchup.write_default("SketchyPhysics", "InspectorVisible",false)
            @PhysicsInspectorDialog.close()
            @PhysicsInspectorDialog=nil
        end
    end
    
    def dialogCheckDirty()
        if($dirtyField!=nil)
            puts "Web dialog is dirty"+$dirtyField.inspect
            return true
        end
        return false
    end
    def dialogMarkDirty(id,dict,key,value)
        if($dirtyField!=nil)
            $dirtyField=[id,dict,key,value]
            puts "Marking dirty"+$dirtyField.inspect
        end
    end
    def dialogClearDirty(id,dict,key,value)
        if($dirtyField!=nil)
            if([id,dict,key,value]!=$dirtyField)
                puts "Warn:dirty value do not match:"+$dirtyField.inspect
            end
            $dirtyField=nil
            puts "Cleared dirty"
        end
    end
    def dialogForceUpdate()
        if($dirtyField!=nil)
            java='document.getElementsByName("myInput")'
            
            @PhysicsInspectorDialog.execute_script_safe(java) 
                    
        end
    end    
    def showDialog()
        @PhysicsInspectorDialog=UI::WebDialog.new("SketchyPhysics Inspector", true,"SP3 Inspectoraa")
        fn= File.dirname(__FILE__) + "/SketchyUI/Inspector.html" #?entity= +grp.entityID.to_s
        #puts fn if $debug
        @PhysicsInspectorDialog.set_file fn;
        if(PLATFORM=="i386-mswin32")   
            @PhysicsInspectorDialog.show {selectionChanged(Sketchup.active_model.selection)}
        else
            @PhysicsInspectorDialog.show_modal {selectionChanged(Sketchup.active_model.selection)}
        end
        @PhysicsInspectorDialog.set_on_close { @PhysicsInspectorDialog=nil }
        @PhysicsInspectorDialog.add_action_callback("puts") {|d,p|puts p}
        @PhysicsInspectorDialog.add_action_callback("valueUpdated") {|d,p|
            path=p.split('=')[0].split('.')
            
            id=path[0]
            dict=path[1]
            key=path[2]

            value=p.split("=",2)[1].to_s
            #puts value
            value=@PhysicsInspectorDialog.get_element_value(value)
            #puts "valueUpdated:"+key+"="+value
            puts "valueUpdated:"+id+"."+dict+"."+key+"="+value.to_s
            $dirtyField=[id,dict,key,value]
            
            group=@idToGroup[id]
            #puts group
        }        
        @PhysicsInspectorDialog.add_action_callback("setAttribute") {|d,p|
            path=p.split('.')
            id=path[0]
            dict=path[1]
            key=path[2].split("=")[0]

            value=p.split("=")[1].to_s.to_f

            group=@idToGroup[id]
            #puts group
$dirtyField=nil            
            puts "setting:"+group.to_s+id+"."+dict+"."+key+"="+value.to_s
            group.set_attribute(dict,key,value);

            }


        @PhysicsInspectorDialog.add_action_callback("setAttributeBool") {|d,p|
            path=p.split('.')
            id=path[0]
            dict=path[1]
            key=path[2].split("=")[0]

            value=p.split("=")[1].to_s
            if(value=="true")
                value=true;
            else
                value=false;
            end

            group=@idToGroup[id]
            group.set_attribute(dict,key,value);
            puts "setting bool:"+group.to_s+id+"."+dict+"."+key+"="+value.to_s
            #puts p
            }
            
        @PhysicsInspectorDialog.add_action_callback("setAttributeString") {|d,p|
            #path=p.split("=")[0]
            #path=path.split('.')
            #puts "direct"            
            #puts @PhysicsInspectorDialog.get_element_value("DesiredRotation")            
            path=p.split('=')[0].split('.')
            
            id=path[0]
            dict=path[1]
            key=path[2]

            value=p.split("=",2)[1].to_s
            #puts value
            value=@PhysicsInspectorDialog.get_element_value(value)
if($dirtyField!=nil && value!=$dirtyField[3])
    puts("******Value correction")            
    puts(path.inspect+" value:"+value.to_s)
    puts("dirtyvalue:"+$dirtyField.inspect)
    #value!=$dirtyField[3]
    
end
$dirtyField=nil

            group=@idToGroup[id]
            #puts group

begin
if($gExperimentalFeatures)
    cc=MSketchyPhysics3::SP3xControllerContext.new()
else
    cc=MSketchyPhysics3::ControllerContext.new()
end   
    $curEvalGroup=group
    cbinding=cc.getBinding(0)
        #fix hard spaces.
    value=value.gsub(/[\xC2\xA0]/," ")
    #0.upto(nvalue.length-1){|i|puts [nvalue[i],nvalue[i].chr]}
    result=eval(value,cbinding)
    if(group.get_attribute(dict,key,nil)!=value)
        puts "setting:"+id+"."+dict+"."+key+"="+value.to_s
        group.set_attribute(dict,key,value);
    else
        #puts "Values are same"
    end
rescue SyntaxError, NameError => boom
  Sketchup::UI.messagebox "Formula doesn't compile: \n#{value}\n" + boom
rescue StandardError => bang
  Sketchup::UI.messagebox "Formula Error: " + bang
rescue
    Sketchup::UI.messagebox "!!Formula Error:"+$!
    #puts "------------------------"
end


            }

        @PhysicsInspectorDialog.add_action_callback("onSelectJoint") {|d,p|

            param=p.split(',')
            id=param[0]
            name=param[1]
            puts "select #{id} #{name} "
            onDialogSelectJoint(id,name)
        
            puts "select "+p
            }   
        
        @PhysicsInspectorDialog.add_action_callback("windowClosed") { 
            puts "dialog closed"
            #Sketchup.write_default("SketchyPhysics", "InspectorVisible",false)
            }
        
    end

    def clearPropertyGrid()
        @PhysicsInspectorDialog.execute_script_safe( "clearPropertyGrid()")
    end
    def propertyGridAddHeader(name)
        @PhysicsInspectorDialog.execute_script_safe( "addPropertyGridHeader('#{name}')")
    end
    def propertyGridAddRow(id,name,value,type)
        str=" addPropertyGridRow('#{id}','#{name}','#{value}','#{type}') "
        @PhysicsInspectorDialog.execute_script_safe( str)                        
    end
    def inspectorAddObjectRef(container,id,name)
        str=" addObjectRef('#{container}','#{id}','#{name}') ;"
        @PhysicsInspectorDialog.execute_script_safe( str)               

    end
    def appendHTML(id,html)
        java="document.getElementById('#{id}').innerHTML+='"+html+"';"
        #puts java if $debug
        @PhysicsInspectorDialog.execute_script_safe(java) 
    end
    
    def insertCell(tableId,text)
        
    end

    def inspectConnections(group)
        
        jnames=JointConnectionTool.getParentJointNames(group)
        return if jnames.empty?
        java="addFlowTableHeader('childJointsGrid','<b>Connected To</b>');"
        @PhysicsInspectorDialog.execute_script_safe(java) 
    
        jnames.each{|jn|
            joint=findJoint(jn)
            #puts "found "+id if $debug
            if(joint==nil)
                #puts "Parent #{jn} not found. Delete connection?"
                JointConnectionTool.disconnectJointNamed(group,jn)
            else
                id=joint.entityID.to_s
                java="addFlowTableCell('childJointsGrid','#{id}','#{jn}','#{jn}');"
                #puts java if $debug
                @PhysicsInspectorDialog.execute_script_safe(java) 
                
                @idToGroup[id]=group
                @idToGroup[id]=joint                 
                #puts "Connected to "+jn if $debug
            end
        }

    end
        
    def inspectJoint(group)
        name=group.get_attribute("SPJOINT","name","error")
        type=group.get_attribute("SPJOINT","type","error")
        
        id=group.entityID.to_s
        java="addFlowTableCell('childJointsGrid','#{id}','#{name}','#{name}');"
        #puts java if $debug
        @PhysicsInspectorDialog.execute_script_safe(java) 
        
        @idToGroup[id]=group 
    end
    
    def inspectEvents(group)
        
        %w(timer onFreeze onUnfreeze onValueChanged).each do |key|
            value=group.get_attribute("SPEVENTS",key,nil)
        end           
    end
    
    def escapeHTML(str)
        #puts str.class if $debug
        return str if(str.class.to_s!="String")
        str=str.gsub(/&/n, '&amp;').gsub(/\"/n, '&quot;').gsub(/>/n, '&gt;').gsub(/</n, '&lt;').gsub(/\n/n,"\\n").gsub(/\r/n,"").gsub("_", '&#95;').gsub(/\$/n,'&#36;').gsub(/ /n, '&nbsp;')#.gsub("'", '&#39;').gsub("\r", '\r').gsub("\n", '\n')
        #puts str
        return str
    end
    
    def checkJointVersion(joint)
        controller=joint.get_attribute("SPJOINT","controller",nil)
        return if(controller==nil)
        MSketchyPhysics3::convertControlledJoint(joint)
    end
    def oldcheckJointVersion(group)
        type=group.get_attribute("SPJOINT","type",nil)
        case type
            when "hinge"
                if(group.get_attribute("SPJOINT","Controller",nil)==nil)
                    group.set_attribute("SPJOINT","Controller","")
                end                    
                if(group.get_attribute("SPJOINT","DesiredRotation",nil)==nil)
                    group.set_attribute("SPJOINT","DesiredRotation","")
                end                    
            when "servo"
                if(group.get_attribute("SPJOINT","DesiredRotation",nil)==nil)
                    group.set_attribute("SPJOINT","DesiredRotation","")
                end                    
            when "slider"
                if(group.get_attribute("SPJOINT","Controller",nil)==nil)
                    group.set_attribute("SPJOINT","Controller","")
                    group.set_attribute("SPJOINT","accel",0.0)
                    group.set_attribute("SPJOINT","damp",0.0)
                end                    
                if(group.get_attribute("SPJOINT","DesiredPosition",nil)==nil)
                    group.set_attribute("SPJOINT","DesiredPosition","")
                end                    
            when "piston"
                if(group.get_attribute("SPJOINT","DesiredPosition",nil)==nil)
                    group.set_attribute("SPJOINT","DesiredPosition","")
                end                    
        end  
    end

    def inspectJointProperties(group)
        
        #convert to new style hinge. 
        #!!!!!!!This probably belongs elsewhere.
        #~ Sketchup.active_model.start_operation("Convert joint")
        checkJointVersion(group)
                           
        tableBody=""
 
        %w(min max accel maxAccel strength damp duration Controller onTick breakingForce ConnectedCollide GearConnectedCollide range falloff delay rate eachFrame gearjoint ratio).each do |key|
            value=group.get_attribute("SPJOINT",key,nil)
            if(value!=nil)
                #value=escapeHTML(value)
            #why was this here?
            #propertyGridAddRow(group.entityID,"spjoint."+key,value,"string") 
                #tableBody+="<tr><td>#{key}:#{value.class}</td>"
                tableBody+="<tr><td>#{key}</td>"
                #onclick="puts('#{group.entityID}');puts(this.name);puts(this.innerHTML);"
                
                fullName=group.entityID.to_s+".SPJOINT."+key
                
                tableBody+="<td  >"
                if(value.class==::Array)
                    tableBody+=array_to_HTMLselect(value)
                    
                elsif(value.class==::TrueClass || value.class==::FalseClass)
                    #value=group.get_attribute("SPOBJ",sn,false).to_s
                    fullName=group.entityID.to_s+".SPJOINT."+key
                    onclick="setSkpAttributeBool(this.name,this.checked)"
                    checked=""
                    checked="checked" if(value)#=='true')
                    tableBody+="<input type='checkbox' onClick='#{onclick}' name='#{fullName}' #{checked}>"+""+'</input>'
                elsif(value.class==::String)
                    onchange="setSkpAttributeString(this.name,this.id)"
                    onkeydown="tab_to_tab(event,this)"
                   #puts onchange if $debug
                    if(key=="Controller")
                        value=escapeHTML(value)
                        tableBody+="<input type='text' class='formulaInput'  size='15' id='#{key}' onchange='#{onchange}' onblur='onchange()' onkeyup='markDirty(this,this.name,this.id);' name='#{fullName}' value=#{value}>"
                    else
                        tableBody+="<input type='text' id='#{key}' size='15' onchange='#{onchange}' onblur='onchange()' id='#{key}' onkeyup='markDirty(this,this.name,this.id);' name='#{fullName}' value=#{value}>"
                    end
                 else    
                    onchange="setSkpAttribute(this.name,this.value)"
                    tableBody+="<input type='text' size='15' onchange='#{onchange}' onblur='onchange()' id='#{key}' onkeypress='validateNumber();' onkeyup='markDirty(this,this.name,this.id);'  name='#{fullName}' value=#{value}>"
                end

                tableBody+="</td>"
                tableBody+="</tr>"
            end                    
            
        end    

        html="<TABLE  class='propertyGrid' style='width:99%;'>"+tableBody+"</table>"
        
#html="<tr><td>Key</td><input type='text'  size='30'    value='123'>"        
        java='document.getElementById("childJointProperties").innerHTML="'+html+'"'
#java='document.getElementById("childJointProperties").innerHTML="TEST"'

        
        #puts java if $debug
        @PhysicsInspectorDialog.execute_script_safe(java) 
        #@PhysicsInspectorDialog.bring_to_front()
    end
    
    def xxinspectJoint(group)
        name=group.get_attribute("SPJOINT","name","error")
        str="Joint:"+name
        propertyGridAddHeader(str)
        
        type=group.get_attribute("SPJOINT","type","error")
        desc=""
        
        #create a new property grid

        tableBody=""
        #tableBody+='<th colspan=2>'+type+'</th>'
        #@PhysicsInspectorDialog.execute_script_safe( java) 
 
        #inspectorAddObjectRef('parentJointsContainer',group.entityID,group.get_attribute("SPJOINT","type","error")+'<br>')
        group.attribute_dictionary("SPJOINT").each_pair { | key, value | 
            if(key!="name"&&key!="type")
                propertyGridAddRow(group.entityID,"spjoint."+key,value,"string") 

                tableBody+="<tr><td>#{key}:#{value.class}</td>"
                #onclick="puts('#{group.entityID}');puts(this.name);puts(this.innerHTML);"
                
                fullName=group.entityID.to_s+".SPJOINT."+key
                
                tableBody+="<td  >"
                if(value.class==::Array)
                    tableBody+=array_to_HTMLselect(value)
                elsif(value.class==::String)
                    onchange="setSkpAttributeString(this.name,this.value)"
                    tableBody+="<input type='text' size='8' onchange='#{onchange}' onblur='onchange()' onkeyup='markDirty(this,this.name,this.id);'  onselect='puts(document.selection.createRange().text)' name='#{fullName}' value=#{value}>"
                 else    
                    onchange="setSkpAttribute(this.name,this.value)"
                    tableBody+="<input type='text' size='8' onchange='#{onchange}' onblur='onchange()' onkeypress='validateNumber();' onkeyup='markDirty(this,this.name,this.id);' onselect='puts(document.selection.createRange().text)' name='#{fullName}' value=#{value}>"
                end
#controllerTypes=["LAxisUD","LAxisLR","RAxisUD","RAxisLR"]
               #tableBody+=["none","LAxisUD","LAxisLR","RAxisUD","RAxisLR"].to_HTMLselect()
            
#to_html(name,type,selectedIndex,onChange

                tableBody+="</td>"
                tableBody+="</tr>"
            end      
        }
        desc=type

        tableDef="<img src='../images/#{type}.png' onclick='toggleDiv(\\\"#{name}\\\")' style='width:16px;height:16px%'>#{desc}<img src='../images/plus.gif' onclick='toggleDiv(\\\"#{name}\\\")'>"
        tableDef+="<div id='#{name}' style='display:none'><TABLE  class='propertyGrid' style='width:99%;'>"
        html=tableDef+tableBody+"</table></div>"
        
        java='document.getElementById("childJointsContainer").innerHTML+="'+html+'"'
        #puts java if $debug
        @PhysicsInspectorDialog.execute_script_safe(java) 
        
        @idToGroup[group.entityID.to_s]=group 

        #inspectorAddObjectRef('parentJointsContainer',group.entityID,desc)
    end
    
    def inspectShape(group)
        
        shape=group.get_attribute("SPOBJ","shape","default")
        #str="shape "+shape+group.to_s
        
        html="<img src='../images/#{shape}.png' style='width:16px;height:16px%'>#{shape}</img>"
        
#id=group.entityID.to_s
#@idToGroup[id]=group 
#java="addFlowTableCell('childJointsGrid','#{id}','#{shape}','#{shape}');"
                    #puts java
#@PhysicsInspectorDialog.execute_script_safe(java)         
        
        
        java='document.getElementById("childObjectsContainer").innerHTML+="'+html+'"'
        @PhysicsInspectorDialog.execute_script_safe(java) 
        #inspectorAddObjectRef('childObjectsContainer',group.entityID,shape)

        propertyGridAddRow(group.entityID,"Shape",shape,"static")

    end

#SPParents

#Jan 12, 09 Unused??
    #~ def inspectState(group)
        #~ java="addFlowTableHeader('childJointsGrid','<b>State</b>');"
        #~ #puts java if $debug
        #~ @PhysicsInspectorDialog.execute_script_safe(java)   
        #~ statenames=["ignore","frozen","static","staticmesh","showcollision"]
        #~ statenames.each{|sn|
            #~ value=group.get_attribute("SPOBJ",sn,false).to_s
            #~ checked=""
            #~ checked="checked" if(value=='true')

            #~ html="<input type='checkbox' onClick='#{onclick}' name='#{fullName}' #{checked}>"+sn+'</input>'
            #~ puts html
            #~ java="addFlowTableCell('childJointsGrid','#{0}','','#{sn}');"
            #~ puts java if $debug
            #~ @PhysicsInspectorDialog.execute_script_safe(java)  
        #~ }            
    #~ end

    #class PhysicsView
      #def update(model)
    #
    
    def inspectCollision(group)
        
        #detect flips and scales.
        
        shape=group.get_attribute( "SPOBJ", "shape", nil)
        return [shape],[] if(shape!=nil)

        shapes=[]
        joints=[]
        group.entities.each { | ent | 
            if (isJoint(ent))
                joints.push(ent.get_attribute("SPJOINT","name",nil))
            elsif (isShape(ent))
                shapes.push(inspectCollision(ent))
            end
        }
        
        shapes.push("default") if(shapes.length<1)
        return shapes,joints         
    end
    def inspectModel
        #puts "ROOT"
        #puts " Version"
        #puts " Settings"
        #puts " Bodies"
        #bodies=[]
#findAllJoints()        

        #puts " Joints"
        allJoints=Hash.new()
        Sketchup.active_model.definitions.each{ |cd|
                cd.instances.each{|ci|
                        #Convert old style joint connections.
                    if(ci.get_attribute("SPOBJ","numParents",nil)!=nil)
                        JointConnectionTool.convertConnections(ci)
                    end

                    if( isJoint(ci) )
                        checkJointVersion(ci)
                        name=ci.get_attribute("SPJOINT","name",nil)
                        #puts "  Joint:#{name}"
                        puts "!!Duplicated joint #{name}" if(allJoints[name]!=nil)
                        allJoints[name]=ci
                    end
                }
            }    

        children=[]
        Sketchup.active_model.entities.each { | ent | 
        
            if (isBody(ent))
                
                #ent.make_unique
                #bodies.push(ent)
                
                #puts "  Body:#{ent} Name:#{ent.name}" if $debug
                #puts "  version:" if $debug
                shapes,joints=inspectCollision(ent)
                
                    #remove any hieiarchy.                
                shapes.flatten!
                joints.flatten!

                #puts "  shapes:"+shapes.inspect if (shapes.length>0) if $debug
                #puts "  joints:"+joints.inspect if (joints.length>0) if $debug
                
                parents=JointConnectionTool.getParentJointNames(ent)
                if(ent.definition.instances[0]!=ent && (parents.length>0 ||joints.length>0))
                    if(parents.length>0)
                        puts "!!Dupe body with connections!" 
                        #JointConnectionTool.disconnectAllJoints(ent)
                    end
                    if(joints.length>0)
                        puts "!!Dupe body with joints!"
                        ent.make_unique
                        ent.entities.each{|pe|
                            if(pe.get_attribute("SPJOINT","type",nil)!=nil)
                                #rename joint.
                                #pe.make_unique
                                nname=pe.get_attribute("SPJOINT","type",nil)+(20000+rand(80000)).to_s
                                pe.set_attribute("SPJOINT","name",nname)
                                pe.name=nname
                                puts "Renaming to #{nname}."
                            end
                        }
                    end
                end 
                    #~ #if a not instance[0] then this objecs is a dupe.
                #~ if(ent.definition.instances[0]!=ent && joints.length>0)
                    #~ puts "!!Dupe body with joints!"
                    #~ ent.make_unique
                    #~ ent.entities.each{|pe|
                        #~ if(pe.get_attribute("SPJOINT","type",nil)!=nil)
                            #~ #rename joint.
                            #~ #pe.make_unique
                            #~ nname=pe.get_attribute("SPJOINT","type",nil)+(20000+rand(80000)).to_s
                            #~ pe.set_attribute("SPJOINT","name",nname)
                            #~ pe.name=nname
                            #~ puts "Renaming to #{nname}."
                        #~ end
                    #~ }
                #~ end               
                common=parents & joints
                if(common.length>0)
                    puts "!!Body is connected to #{common.length} of its own joints. Disconnecting."
                    common.each{|jn|
                        JointConnectionTool.disconnectJointNamed(ent,jn)
                        }
                end
                
                #puts "  connected to:"+parents.inspect if (parents.length>0) if $debug
                children.push(ent) if (parents.length>0)
            end
            if (isJoint(ent))
                name=ent.get_attribute("SPJOINT","name",nil)
                #puts "  Joint:#{ent} Name:#{name}" if $debug
            end

        }                    
        
        #puts " Connections" if $debug
        children.each{|ent|
            parents=JointConnectionTool.getParentJointNames(ent)
            parents.each{|parentName|
                joint=allJoints[parentName]
                if joint==nil
                    puts "!!Joint not found:#{parentName}"
                    JointConnectionTool.disconnectJointNamed(ent,parentName)
                    #fix
                else
                    #puts "  Body #{ent} connected to #{parentName}" if $debug
                end
                
            }
        }
        
return nil
    end
    

    def inspectGroup(group)
        if(@PhysicsInspectorDialog==nil)
            return
        end
        #puts "inspecting "+group.to_s if $debug

        propertyGridAddHeader("State")

        ######HACK. Convert old style static meshes to new. 
        if(group.get_attribute("SPOBJ","shape",nil)=='staticmesh')
            #group.set_attribute("SPOBJ","shape",nil);
            #group.set_attribute("SPOBJ","staticmesh",true);
        end

        str="" 
        #str+="setObjectShape(\""+@selectedObject.get_attribute("SPOBJ","shape","default")+"\");"
        count=0;
        statenames=["ignore","frozen","static","staticmesh","showcollision","noautofreeze","magnetic","nocollison"]
        statenames.each{|sn|
            value=group.get_attribute("SPOBJ",sn,false).to_s
            fullName=group.entityID.to_s+".SPOBJ."+sn
            onclick="setSkpAttributeBool(this.name,this.checked)"
           
            checked=""
            checked="checked" if(value=='true')
            html="<input type='checkbox' onClick='#{onclick}' name='#{fullName}' #{checked}>"+sn+'</input>'
            str+='document.getElementById("stateContainer").innerHTML+="'+html+'";'
        }
        @PhysicsInspectorDialog.execute_script_safe(str)       

        str=""
        if(!isJoint(group))
            #statenames=["magnet","thruster","emitter"]#,"materialid"]
            statenames=["magnet","thruster","emitter","touchable", "tickable"]#,"materialid"]
            statenames.each{|sn|
                value=group.get_attribute("SPOBJ",sn,false).to_s
                fullName=group.entityID.to_s+".SPOBJ."+sn
                onclick="setSkpAttributeBool(this.name,this.checked)"
                
                checked=""
                checked="checked" if(value=='true')

                if(sn=="magnet")
                    strength=group.get_attribute("SPOBJ","strength",0.0).to_s
                    strength=escapeHTML(strength)
                    #puts strength
                    strname=group.entityID.to_s+".SPOBJ."+"strength"
                    onchange="setSkpAttributeString(this.name,this.id)"
                    html="<div class='propertyGrid'><table width='100%'><tr><td width='25%' valign='bottom'>"
                    html+="<input type='checkbox' onClick='#{onclick}' name='#{fullName}' #{checked}>Magnet"+'</input></td>'
                    html+="<td width='25%' align='right' valign='bottom'>Strength</td>"
                    html+="<td><input type='text' class='formulaInput' id='strength' style='width:98%' onchange='#{onchange}' onblur='onchange()' onkeyup='markDirty(this,this.name,this.id);' name='#{strname}' value=#{strength}></input>"
                    html+="</td></tr></table></div>"
                    #html="<br>Magnet Strength<input type='text' size='6' onBlur='#{onchange}' name='#{strname}' value='"+strength.to_s+"'></input>"
                elsif(sn=="thruster")
                    #next if(group.get_attribute( "SPJOINT", "type", nil)!=nil)
                    strength=group.get_attribute("SPOBJ","tstrength",0.0).to_s
                    strength=escapeHTML(strength)
                    #puts strength
                    strname=group.entityID.to_s+".SPOBJ."+"tstrength"
                    onchange="setSkpAttributeString(this.name,this.id)"
                    html="<div class='propertyGrid'><table width='100%'><tr><td width='25%'>"
                    html+="<input type='checkbox' onClick='#{onclick}' name='#{fullName}' #{checked}>Thruster"+'</input>'
                    html+="<td width='25%' align='right' valign='bottom'>Strength</td>"
                    html+="<td><input type='text' class='formulaInput' id='tstrength' style='width:98%' onchange='#{onchange}' onblur='onchange()' onkeyup='markDirty(this,this.name,this.id);' name='#{strname}' value=#{strength}></input>"
                    html+="</td></tr></table></div>"
                elsif(sn=="materialid")
                    #next if(group.get_attribute( "SPJOINT", "type", nil)!=nil)
                    matName=group.get_attribute("SPOBJ","materialid",0).to_s
                    matName=escapeHTML(matName)
                    #puts strength
                    strname=group.entityID.to_s+".SPOBJ."+"materialid"
                    onchange="setSkpAttribute(this.name,this.value)"
                    html="<div class='propertyGrid'><table width='100%'><tr><td width='50%'>"
                    html+="</td><td>"
                    html+="</td><td>MaterialID<input type='text' size='5' onchange='#{onchange}' onblur='onchange()'  onkeypress='validateNumber();' onkeyup='markDirty(this,this.name,this.id);'  name='#{strname}' value=#{matName}>"
                    #html+="ID<input type='text' class='formulaInput' id='materialid' size='12' onBlur='#{onchange}' name='#{strname}' value=#{matName}></input>"
                    html+="</td></tr></table></div>"
                elsif(sn=="emitter")
                    #next if(group.get_attribute( "SPJOINT", "type", nil)!=nil)
                    #puts strength
                    html="<div class='propertyGrid'><table width='100%'><tr><td width='25%'>"
                    html+="<input type='checkbox' onClick='#{onclick}' name='#{fullName}' #{checked}>Emitter"+'</input></td>'
                    
                    onchange="setSkpAttributeString(this.name,this.id)"
                    strname=group.entityID.to_s+".SPOBJ."+"emitterstrength"
                    strength=group.get_attribute("SPOBJ","emitterstrength",0.0).to_s
                    strength=escapeHTML(strength)
                    html+="<td width='25%' align='right' valign='bottom'>Strength</td>"
                    html+="<td><input type='text' class='formulaInput' id='emitterstrength' style='width:98%' onchange='#{onchange}' onblur='onchange()'  onkeyup='markDirty(this,this.name,this.id);' name='#{strname}' value=#{strength}></input>"
                    html+="</td></tr>"

                    html+="<tr><table width='100%'><tr>"

                    lifetime=group.get_attribute("SPOBJ","lifetime",0).to_i
                    lifetime=escapeHTML(lifetime)
                    strname=group.entityID.to_s+".SPOBJ."+"lifetime"                
                    onchange="setSkpAttribute(this.name,this.value)"
                    html+="<td width='1%' valign='bottom'>Lifetime</td>"
                    html+="<td><input type='text' style='width:98%' onchange='#{onchange}' onblur='onchange()' id='lifetime' onkeypress='validateNumber();' onkeyup='markDirty(this,this.name,this.id);' name='#{strname}' value=#{lifetime}>"
                    html+="</td>"
                    
                    onchange="setSkpAttributeString(this.name,this.id)"
                    rate=group.get_attribute("SPOBJ","emitterrate",0).to_s
                    rate=escapeHTML(rate)
                    ratestrname=group.entityID.to_s+".SPOBJ."+"emitterrate"
                    html+="<td width='1%' align='right' valign='bottom'>Rate</td><td><input type='text' class='formulaInput' id='emitterrate' style='width:98%' onchange='#{onchange}' onblur='onchange()'  onkeyup='markDirty(this,this.name,this.id);' name='#{ratestrname}' value=#{rate}></input>"
                    html+="</td>"
                    

                    html+="</tr></table></tr></table></div>"
                elsif(sn=="touchable")
                    #next if(group.get_attribute( "SPJOINT", "type", nil)!=nil)
                    #puts strength
                    toggleclick=onclick+';setDivVisibility(\"ontouch\",this.checked)'
                    html="<div class='propertyGrid' ><table width='100%'><tr>"
                    html+="<td width='50%'><input type='checkbox' onClick='#{toggleclick}' name='#{fullName}' #{checked}>OnTouch"+'</input></td>'

                    onchange="setSkpAttributeString(this.name,this.id)"
                    rate=group.get_attribute("SPOBJ","touchrate",0).to_s
                    rate=escapeHTML(rate)
                    ratestrname=group.entityID.to_s+".SPOBJ."+"touchrate"
                    html+="<td width='1%' align='right' valign='bottom'>Rate</td><td><input type='text' id='touchrate' style=' width:98%' onkeypress='validateNumber();'  onkeyup='markDirty(this,this.name,this.id);' onchange='#{onchange}' onblur='onchange()' name='#{ratestrname}' value=#{rate}></input>"
                    html+="</td>"
                    
                    strname=group.entityID.to_s+".SPOBJ."+"ontouch"
                    ontouch=group.get_attribute("SPOBJ","ontouch",0.0).to_s
                    ontouch=escapeHTML(ontouch)
                    
                    checked!="checked"? vis='none': vis='block'
                    html+="<table width='100%'><tr width='100%'><td width='100%'><textarea class='formulaInput' type='text' cols=30 ROWS=5 style='width:100%;display:#{vis}' class='formulaInput' id='ontouch' onchange='#{onchange}' onblur='onchange()' onkeyup='markDirty(this,this.name,this.id);' name='#{strname}' value=#{ontouch}>#{ontouch}</textarea>"
                    html+="</td></tr></table></div>"
                elsif(sn=="tickable")

                    toggleclick=onclick+';setDivVisibility(\"ontick\",this.checked)'
                    html="<div class='propertyGrid'><table width='100%'><tr>"
                    html+="<td width='50%'><input type='checkbox' onClick='#{toggleclick}' name='#{fullName}' #{checked}>OnTick"+'</input></td>'

                    onchange="setSkpAttributeString(this.name,this.id)"
                    rate=group.get_attribute("SPOBJ","tickrate",0).to_s
                    rate=escapeHTML(rate)
                    ratestrname=group.entityID.to_s+".SPOBJ."+"tickrate"
                    html+="<td width='1%' align='right' valign='bottom'>Rate</td><td><input type='text' id='tickrate' style=' width:98%' onkeypress='validateNumber();' onchange='#{onchange}' onblur='onchange()'  onkeyup='markDirty(this,this.name,this.id);' name='#{ratestrname}' value=#{rate}></input>"
                    html+="</td>"
                    
                    strname=group.entityID.to_s+".SPOBJ."+"ontick"
                    ontick=group.get_attribute("SPOBJ","ontick",0.0).to_s
                    ontick=escapeHTML(ontick)
                    
                    checked!="checked"? vis='none': vis='block'
                    html+="<table width='100%'><tr width='100%'><td width='100%'><textarea class='formulaInput' type='text' cols=30 ROWS=5 style='width:100%;display:#{vis}' class='formulaInput' id='ontick' onchange='#{onchange}' onblur='onchange()' onkeyup='markDirty(this,this.name,this.id);' name='#{strname}' value=#{ontick}>#{ontick}</textarea>"
                    html+="</td></tr></table></div>"
                end

                str+=html
            }
            str='document.getElementById("propertiesContainer").innerHTML="'+str+'";'
            @PhysicsInspectorDialog.execute_script_safe(str)        
        end
       
        str=""     
        if(isJoint(group))
            puts("Selection is joint ")
            str='document.getElementById("selectedObjectName").innerHTML+="('+group.get_attribute("SPJOINT","type","error")+')";'     
            @PhysicsInspectorDialog.execute_script_safe(str)  
                #set dialog name field        
            str='document.getElementById("selectedObjectName").innerHTML="Joint:'+group.name+'";'     
            @PhysicsInspectorDialog.execute_script_safe(str)  

            inspectJoint(group)
            onDialogSelectJoint(group.entityID,group.get_attribute("SPJOINT","name","error"))
            return
        end

        inspectConnections(group)   

#java="addFlowTableHeader('childJointsGrid','<b>Shapes</b>');"
#@PhysicsInspectorDialog.execute_script_safe(java) 

        shape=group.get_attribute("SPOBJ", "shape", nil)
        if(shape!=nil)
            propertyGridAddHeader("Shape:"+shape)  
            str='document.getElementById("selectedObjectName").innerHTML+="('+shape+')";'     
            @PhysicsInspectorDialog.execute_script_safe(str)              
            inspectShape(group)
                #set dialog name field        
            str='document.getElementById("selectedObjectName").innerHTML="Body:'+group.name+'";'     
            @PhysicsInspectorDialog.execute_script_safe(str)  
            return
        end
        
            #case if user selected an object in a group.
        if(group.parent!=Sketchup.active_model)
            propertyGridAddHeader("Shape:default")
            inspectShape(group)
            str='document.getElementById("selectedObjectName").innerHTML="Shape:'+group.name+'";'     
            @PhysicsInspectorDialog.execute_script_safe(str)  
            return;
        end

        bIsCompound=false

        group.entities.each { | ent | 
            if (isShape(ent))
                if(!bIsCompound)
                    bIsCompound=true 
                    propertyGridAddHeader("Compound Object")
                    str='document.getElementById("selectedObjectName").innerHTML="Compound Object:'+group.name+'";'     
                    @PhysicsInspectorDialog.execute_script_safe(str)  
                    #puts "Body is compound" if $debug
                end
                inspectShape(ent)
            end
            #add joint check here.
        }
        

        java="addFlowTableHeader('childJointsGrid','<b>Internal</b>');"
        #puts java if $debug
        @PhysicsInspectorDialog.execute_script_safe(java) 
        
        #now find joints. Probably want to do this at the same time as above.
        group.entities.each { | ent | 
            if(isJoint(ent))
                inspectJoint(ent)
            end
        }

        if(shape==nil && !bIsCompound)
            propertyGridAddHeader("Body is default shape")
            str='document.getElementById("selectedObjectName").innerHTML="Body:(box):'+group.name+'";'     
            @PhysicsInspectorDialog.execute_script_safe(str)  
        end

    end
 
    def dialogSelect(grp)

    end
    
    def inspectSelection()
            dialogSelect(Sketchup.active_model.selection[0])
    end
        
    def onLButtonDown(flags, x, y, view)
        @dragCount=0;
        @lButtonDown=true;
        
        @inputPoint = Sketchup::InputPoint.new
        @inputPoint.pick view, x, y
        
        ph=view.pick_helper
        num=ph.do_pick x,y
        ent=ph.best_picked
    
        if(ent.class==Sketchup::Group || ent.class==Sketchup::ComponentInstance)
            Sketchup.active_model.selection.clear()
            Sketchup.active_model.selection.add(ent)
            inspectSelection()

        end

    end
    
    def deactivate(view)         
        puts "deactivate called"
        @PhysicsInspectorDialog.close() if @PhysicsInspectorDialog!=nil

        Sketchup::set_status_text("")
    end

        def draw(view)
            #@relatedBounds.each{|rb|
            #    view.draw(GL_LINE_STRIP,rb)
            #    }
        end
        
        def getExtents       
            bb = Geom::BoundingBox.new
            if(@inputPoint!=nil)
                bb.add @inputPoint.position
                bb.add @inputPoint.position
            end
        end

        # This is called followed directly by onRButtonDown
        def getMenu(menu)
        end

        def onCancel(reason, menu)                
            puts "onCancel called"
        end

        def onKeyDown(key, rpt, flags, view)
            if( key == COPY_MODIFIER_KEY && rpt == 1 )
		        @ctrlDown=true
            end
            if( key == CONSTRAIN_MODIFIER_KEY && rpt == 1 )
		        @shiftDown=true
            end
        end

        def onKeyUp(key, rpt, flags, view)
            if( key == COPY_MODIFIER_KEY)
		        @ctrlDown=false
	        end
            if( key == CONSTRAIN_MODIFIER_KEY)
		        @shiftDown=false
	        end
        end
        
        def pickEmbeddedJoint(x,y,view)
            ph=view.pick_helper
            num=ph.do_pick x,y
            
            item=nil
            path=ph.path_at(1)
            
            return item if (path==nil)

            path.length.downto(0){|i|
                if(path[i].class==Sketchup::Group && 
                        (path[i].parent==Sketchup.active_model || (path[i].get_attribute("SPJOINT","name",nil)!=nil)))
                    item=path[i]
                    #puts "ParentGroup="+item.to_s if $debug
                    break
                end
            }
            return item;
        end
        
  
        
        # NOTE: Called after onLButtonDown and onLButtonUp. 
        def onLButtonDoubleClick(flags, x, y, view)
        end

        def onLButtonUp(flags, x, y, view)
       end    
        def onMouseMove(flags, x, y, view) 
        end
        #def onMouseEnter(view)
        #end
        #def onMouseLeave(view)
        #end
        # NOTE: Called after onrButtonDown and onRButtonUp.
        def onRButtonDoubleClick(flags, x, y, view)
        end
        def onMButtonDoubleClick(flags, x, y, view)
        end
        def onRButtonDown(flags, x, y, view)
        end
        def onMButtonDown(flags, x, y, view)
        end
        def onMButtonUp(flags, x, y, view)
        end

        # Called not only right after a onRButtonDown, but after a onRButtonDoubleClick?
        def onRButtonUp(flags, x, y, view)
        end

        # NOTE: onReturn is followed directly by onKeyDown
        def onReturn(view)
        end
                                               
        def onUserText(text, view)
        end

        # Called when I double-click middle mouse button (exits out of orbit)
        def resume(view)
            @ctrlDown=false 
            @shiftDown=false
        end

        # Called when I press middle mouse button (goes into orbit)
        def suspend(view)

        end

end # end of class JointConnectionTool

$physicsAppObserver=PhysicsAppObserver.new()
Sketchup.add_observer($physicsAppObserver)

end #module MSketchyPhysics3
