require 'sketchup.rb'

def fakeExport()
    
    #Get animation accessor
    sr=SketchyReplay::SketchyReplay.new()
    
    #Check for animation in the file
    return if (sr.lastFrame==0)
    
    #set objects pos and camera for first frame.
    sr.start()
    
    #export first frame here
    
    0.upto(sr.lastFrame){
        #advance object and camera positions
        sr.nextFrame()
        puts sr.frame
        #export frame here:
    }
    
    #cleanup
    sr.rewind()

end

module SketchyReplay

#~ class Array #Array to Hash. Nice! found on codesnipets.com
  #~ def to_h(&block)
    #~ Hash[*self.collect { |v|
      #~ [v, block.call(v)]
    #~ }.flatten]
  #~ end
#~ end

class SketchyReplay
    attr_reader :frame
    attr_reader :lastFrame

    def initialize
		@frame=0;
        @lastFrame=0
		@bPaused=false;
		@bStopped=true;
		@AnimationObjectList=nil
        findAnimationObjects()
		@animationRate=1
	end

	def findAnimationObjects
		@AnimationObjectList=[]
        @lastFrame=0
		Sketchup.active_model.entities.each{|ent| 
			if(ent.class==Sketchup::Group ||ent.class==Sketchup::ComponentInstance)						
				if(ent.attribute_dictionaries!=nil && ent.attribute_dictionaries["SPTAKE"]!=nil)
                    samps=ent.get_attribute("SPTAKE","samples",nil)
                    #fill in empty keys with an index to the last good key.
                    #this allows reverse and seek to work smoothly
                    lastkey=0
                    samps.each_index{|si|
                        if(samps[si]==nil)
                            samps[si]=lastkey
                        else
                            lastkey=si
                        end
                    }
                    #puts samps.length
                    @lastFrame=samps.length if(samps.length>@lastFrame)
					@AnimationObjectList.push([ent,samps]) unless ent.deleted?	#used in update to lookup object.
				end
			end
		}
	end

    def export()
        findAnimationObjects()
        startFrame=0
        endFrame=@lastFrame
        rate=@animationRate
        saveType="Skp"
        
        prompts = ["Start frame", "End frame", "Rate","Save as"]
        values = [startFrame, endFrame, @animationRate,saveType]
        results = inputbox(prompts, values,[[],[],[],["Skp|Png|Jpg"]], "Export settings.")
        if (!results)
            return
        else
            startFrame, endFrame, @animationRate, saveType=results
        end
        path=Sketchup.active_model.path
        path=File.basename(path, ".skp")
        sf=UI.savepanel("Export Animation",nil,path)
        #puts sf
        return if sf==nil
        dir=File.dirname(sf)
        fn=File.basename(sf, ".skp")
        dir.gsub!(/\\/,'/')# change \ to /. I HATE how ruby needs regexpr for this crap!!!!!!

        Sketchup.active_model.start_operation "Export animation"
        expFrame=0
        begin
            start()
            
            @AnimationObjectList.each {|ent|ent[0].attribute_dictionaries.delete("SPTAKE")}
            
            while(@frame<endFrame)
                nextFrame(nil)
                fname="#{dir}/#{fn}_%06d.#{saveType}" % expFrame
                if(saveType=="Skp")
                    Sketchup.active_model.save(fname)
                else
                    Sketchup.active_model.active_view.write_image(fname)
                end
                expFrame+=1
            end            
            setFrame(0)
        rescue
            UI.messagebox("Error:"+$!,MB_OK,"Error exporting animation")
        end
    
        Sketchup.active_model.abort_operation 
        
    end
	def start
		@bPaused=false
		@bStopped=false
		camera = Sketchup.active_model.active_view.camera
		@cameraRestore=Sketchup::Camera.new camera.eye, camera.target, camera.up
		setCameraToPage(Sketchup.active_model.pages.selected_page)
        Sketchup.active_model.active_view.show_frame()
	end
	
	# Start the animation
	def play
        @animationRate=@animationRate.abs
		@bPaused=!@bPaused
		if(!@bPaused and @bStopped==true)
			start
		end
        Sketchup.active_model.active_view.show_frame()
	end

	def pause
		@bPaused=true
        Sketchup.active_model.active_view.show_frame()
	end
	def isPaused
		return @bPaused
	end

	def reverse
        return if (@frame<=0)
        @bPaused=false
		@animationRate=-@animationRate
        Sketchup.active_model.active_view.show_frame()
	end
		
	def rewind
		@bPaused=true;
		
		setCameraToPage(Sketchup.active_model.pages.selected_page)

		cameraPreFrame()
		setFrame(0)
		updateCamera()
		@bStopped=true
		if(@cameraRestore)
			Sketchup.active_model.active_view.camera=@cameraRestore
		end
		@AnimationObjectList=nil
	end
	
	def setFrame(frameNumber)
		@frame=frameNumber
        #Sketchup.vcb_value=frameNumber
        Sketchup::set_status_text "Frame", SB_VCB_LABEL
        Sketchup::set_status_text "#{@frame}", SB_VCB_VALUE
		#if needed find objects.
		if(@AnimationObjectList==nil)
			findAnimationObjects()
		end
        
		@AnimationObjectList.each {|objData| 
            grp=objData[0]
            next if grp.deleted?
            
            ar=objData[1]
            next if (ar==nil || ar.length<frameNumber)
            #if we reached this point there is still some frames left in this animation.
           
            mat=ar[frameNumber]
            if(mat.class==Fixnum)#is this an index to a previous key?
                mat=ar[mat]
            end
            #Sketchup.active_model.entities.add_instance(grp.definition,mat) unless mat==nil
            #ngrp=grp.copy
            grp.move!(Geom::Transformation.new(mat)) unless mat==nil 
        }
        

    end
	
	def nextFrame(view=nil)
        #puts [@frame,@bPaused,@animationRate]
		if(!@bPaused)
            @frame=@frame+@animationRate
			cameraPreFrame()
            setFrame(@frame)
            updateCamera()
            view.show_frame if view!=nil
            Sketchup::set_status_text "Frame(#{@frame}/#{@lastFrame})"
		end

	    return true
	end



	@cameraParent=nil
	@cameraTarget=nil
	@cameraType=nil		#type=fixed,relative,drag
	def findComponentNamed(name)
		if(name==nil)
			return nil
		end
		Sketchup.active_model.definitions.each{|cd| 
			cd.instances.each{|ci|					
				if(ci.name.casecmp(name) == 0)
					return ci
				end
			} 	
		}
		return nil
	end	

#camera follow , target, whatever.
#duration 
#start frame, end frame.
#next /prev frame name (optional for out of sequence cuts.


	def findPageNamed(name)
		if(name!=nil)
			Sketchup.active_model.pages.each{|p|
				if(p.name.casecmp(name) == 0)
					return p
				end
			}
		end
		return nil
	end	
		

	def setCameraToPage(page)
        
        return if !$bSPMovieMode
        
		@cameraParent=nil
		@cameraTarget=nil
		@cameraType=nil
		@cameraNextPage=nil
		@cameraFrameEnd=nil
			
        if(page==nil)
            return
        end
        
		#Sketchup.active_model.pages.selected_page.description.downcase.gsub(/ /,"").split(";")
		paramArray=page.description.downcase.gsub(/ /,"").split(";")
		params=Hash[*paramArray.collect { |v| 
			[v.split("=")[0], v.split("=")[1]]
		}.flatten]
	
		#if series
		#find right page in series
		#set transition frame and next page
		@cameraParent=findComponentNamed(params["parent"])#follow
		@cameraTarget=findComponentNamed(params["target"])#track

#@cameraParent=findComponentNamed(params["follow"])#follow
#@cameraTarget=findComponentNamed(params["track"])#track
		@cameraType=params["type"]
		@cameraNextPage=findPageNamed(params["nextpage"])

		#defaults to first (0) and last frame in animation
		@cameraEndFrame=params["endframe"]
		@cameraStartFrame=params["startFrame"]
		
		
		if(params["setframe"]!=nil)
			@frame=params["setframe"].to_i
		end
		if(params["pauseframe"]!=nil)
			@pauseFrame=params["pauseframe"].to_i
		else
			@pauseFrame=nil
		end
		if(params["animationrate"]!=nil)
			@animationRate=params["animationrate"].to_i
		else
			@animationRate=1
		end
		#print @cameraNextPage,",",@cameraEndFrame.to_i
		Sketchup.active_model.active_view.camera=page.camera
		
	end
    def onUserText(text,view)
        puts "onUserText"+text
    end


	def findCameras()
		@cameraEntity=nil
		@cameraTargetEntity=nil
		@cameraPreMoveOffset=nil
		begin
			params=Sketchup.active_model.pages.selected_page.description.downcase.split(';')
			if(pageDesc.include? "parent=")
				pageDesc.chomp!
				targetname=pageDesc.split("=")[1]
				Sketchup.active_model.entities.each{|ent| 
					if(ent.typename.downcase == "componentinstance" and ent.name.downcase == targetname )
						@cameraTargetEntity=ent
						camera = Sketchup.active_model.active_view.camera
					end
					}
			end
		rescue
		end
	end

	def cameraPreFrame
		if(@cameraEndFrame!=nil and @frame>@cameraEndFrame.to_i and @cameraNextPage!=nil)
			setCameraToPage(@cameraNextPage)
		end
			
		if(@pauseFrame!=nil and @frame>@pauseFrame.to_i)
			pause
		end
        if(@frame>@lastFrame)
            @frame=@lastFrame
            pause
        end            
        
		if(@animationRate<0 and @frame<0)
            @frame=0
			reverse
			pause
		end
		
		if(@cameraParent!=nil)
#			@cameraPreMoveOffset=Sketchup.active_model.active_view.camera.eye-@cameraParent.transformation.origin;
			@cameraPreMoveOffset=Sketchup.active_model.active_view.camera.eye-@cameraParent.bounds.center;
		end
	end
	def updateCamera()

#Sketchup.active_model.selection.first.curve.vertices.each{|v| print v.position}
#Sketchup.active_model.selection.first.curve.vertices[curVert].each{|v| print v.position}

		def calcPointAlongCurve (curve,percent)
			curve=Sketchup.active_model.selection.first.curve
			totalLength=0
			curve.edges.each{|e| 
				totalLength+=e.length
			}

			dist=(1.0/totalLength)*percent
			curve.edges.each{|e| 
				dist=dist-e.length
				if dist<0
					return e.line[0]+(e.line[1].length=(e.length-dist))
				end
			}
		end



		camera = Sketchup.active_model.active_view.camera
		if(@cameraParent!=nil)
			#if(@cameraParent.description.downcase.include?("animationpath"))
			#	dest=calcPointAlongCurve(@cameraPath,1.0/(frameEnd-frameStart))+@cameraPreMoveOffset
			#else			
				#dest=@cameraParent.transformation.origin+@cameraPreMoveOffset
				dest=@cameraParent.bounds.center+@cameraPreMoveOffset
			#end
			camera.set(dest, dest+camera.direction, Geom::Vector3d.new(0, 0, 1))
		end
		if(@cameraTarget!=nil)
			target=@cameraTarget
			camera.set(camera.eye, @cameraTarget.bounds.center, Geom::Vector3d.new(0, 0, 1))
		end
	end

	
	# The stop method will be called when SketchUp wants an animation to stop
	# this method is optional.
	def stop
	end

##################################################start of tool################################################

	# The activate method is called when a tool is first activated.  It is not
	# required, but it is a good place to initialize stuff.
	def activate
	end

	def onLButtonDown(flags, x, y, view)
	end

	# onLButtonUp is called when the user releases the left mouse button
	def onLButtonUp(flags, x, y, view)
	end

	# draw is optional.  It is called on the active tool whenever SketchUp
	# needs to update the screen.
	#def draw(view)
	#end
	#def onSetCursor()
	#end

##################################################end of tool################################################



end #class SketchyReplay

def self.createSketchReplayToolbar()

##################
#return;

    toolbar = UI::Toolbar.new "Sketchy Replay"

    $bSPDoRecord=false
    cmd = UI::Command.new("Record") { $bSPDoRecord=!$bSPDoRecord}

    #~ def validme()
     #~ return MF_GRAYED   
    #~ end
    cmd.set_validation_proc {
        if $bSPDoRecord==true
            MF_CHECKED
        else
            MF_ENABLED
        end
    } 

    path = Sketchup.find_support_file "SketchyPhysics-StopButton.png", "plugins/SketchyPhysics3/Images/"
    cmd.small_icon = path
    cmd.large_icon = path
    cmd.tooltip = "Toggle recording"
    cmd.status_bar_text = "Toggle recording"
    cmd.menu_text = "Toggle recording"
    toolbar.add_item cmd

    replaycmd = UI::Command.new("Play") { 
        #animation=
        if @@replayAnimation==nil
            @@replayAnimation=SketchyReplay.new
        end
        Sketchup.active_model.active_view.animation=@@replayAnimation
        @@replayAnimation.play
            
            
        #~ if(Sketchup.active_model.active_view.animation=@@replayAnimation and @@replayAnimation.isPaused )
            #~ Sketchup.active_model.active_view.animation = nil
        #~ end
        }
        
    path = Sketchup.find_support_file "SketchyReplay-PlayPauseButton.png", "plugins/SketchyPhysics3/Images/"

    replaycmd.small_icon = path
    replaycmd.large_icon = path
        
    replaycmd.tooltip = "Play animation"
    replaycmd.menu_text = "Play"
    toolbar.add_item replaycmd

    cmd = UI::Command.new("Rewind") { 
    #@@replayAnimation.setFrame(1)
    #return;
        @@replayAnimation.rewind
        Sketchup.active_model.active_view.animation = nil
        }

    path = Sketchup.find_support_file "SketchyReplay-RewindButton.png", "plugins/SketchyPhysics3/Images/"

    cmd.small_icon = path
    cmd.large_icon = path
    cmd.tooltip = "Rewind to first frame"
    cmd.status_bar_text = "Rewind to first frame"
    cmd.menu_text = "Rewind"
    toolbar.add_item cmd

    cmd = UI::Command.new("Reverse") { 
        @@replayAnimation.reverse
        }
        path = Sketchup.find_support_file "SketchyReplay-ReverseButton.png", "plugins/SketchyPhysics3/Images/"

    cmd.small_icon = path
    cmd.large_icon = path

    cmd.tooltip = "Reverse"
    cmd.status_bar_text = "Reverse"
    cmd.menu_text = "Reverse"
    toolbar.add_item cmd


    $bSPMovieMode=false
    cmd = UI::Command.new("Record") { $bSPMovieMode=!$bSPMovieMode}

    #~ def validme()
     #~ return MF_GRAYED   
    #~ end
    cmd.set_validation_proc {
        if $bSPMovieMode==true
            MF_CHECKED
        else
            MF_ENABLED
        end
    } 

    path = Sketchup.find_support_file "SketchyReplay-MovieButton.png", "plugins/SketchyPhysics3/Images/"
    cmd.small_icon = path
    cmd.large_icon = path
    cmd.tooltip = "Toggle movie mode"
    cmd.status_bar_text = "Toggle movie mode"
    cmd.menu_text = "Toggle movie mode"
    cmd.menu_text = "Toggle movie mode"
    #toolbar.add_item cmd


	submenu=UI.menu("Plugins")
	smenu =submenu.add_submenu("SketchyReplay")
    item = smenu.add_item("Export animation...") { 
        sr=SketchyReplay.new
        sr.export()
        }
     item = smenu.add_item("Erase animation...") { 
		Sketchup.active_model.definitions.each{|cd| 
			cd.instances.each{|ci|					
				if(ci.get_attribute("SPTAKE","samples",nil)!= nil)
					ci.set_attribute("SPTAKE","samples",nil)
				end
			} 	
		}
    }
    #~ cmd = UI::Command.new("Export") { 
        #~ sr=SketchyReplay.new
        #~ sr.export()
        #~ }
        #~ path = Sketchup.find_support_file "SketchyReplay-ReverseButton.png", "plugins/SketchyPhysics3/Images/"

    #~ cmd.small_icon = path
    #~ cmd.large_icon = path

    #~ cmd.tooltip = "Export"
    #~ cmd.status_bar_text = "Export"
    #~ cmd.menu_text = "Export"
    #~ toolbar.add_item cmd

    #~ cmd = UI::Command.new("ClearAnimation") { 
        #~ #@@replayAnimation.rewind
        #~ @@replayAnimation.clearAllAnimation
        #~ Sketchup.active_model.active_view.animation = nil
        #~ }
        
    #~ path = Sketchup.find_support_file "SketchyReplay-ClearAnimationButton.png", "plugins/SketchyPhysics3/Images/"

    #~ cmd.small_icon = path
    #~ cmd.large_icon = path	
    #~ cmd.tooltip = "Clear all animation data"
    #~ cmd.status_bar_text = "Clear all animation data"
    #~ cmd.menu_text = "ClearAnimation"
    #~ toolbar.add_item cmd

    toolbar.show


end



#-----------------------------------------------------------------------------
# Add an physics menu items to the Tools menu
if( not file_loaded?("SketchyReplay.rb") )
    #add_separator_to_menu("Help")
    #SketchyReplayClient_menu = UI.menu("Help")
    #SketchyReplayClient_menu.add_item("About SketchyReplay") {UI.messagebox "Version V01.\nWritten by C Phillips"}
end

@@replayAnimation=nil
createSketchReplayToolbar()

file_loaded("SketchyReplay.rb")

end