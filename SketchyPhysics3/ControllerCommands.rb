require 'sketchup.rb'
module MSketchyPhysics3

class ControllerContext
    def initialize
        @vars=Hash.new
        @namedGroups=Hash.new
    end
    def slider(sname,defaultValue=0.5,min=0.0,max=1.0)
        return defaultValue if($controlSliders==nil)
        if($controlSliders[sname]==nil)
            MSketchyPhysics3::createController(sname,defaultValue,min,max)   
          end
          $controlSliders[sname].value
        return $controlSliders[sname].value    
    end
    def setVar(name,value)
        @vars[name]=value    
    end
    def getVar(name)
        return 0.0 if(@vars[name]==nil)
        return @vars[name]    
    end
    def getSetVar(name,value=0)
        return 0.0 if(@vars[name]==nil)
        v= @vars[name]
        @vars[name]=value
        return v        
    end    
    
    def key(k)
        k=KeyNames[k.upcase]
        return (1.0) if (k!=nil && getKeyState(k) )
        return(0.0)
    end
    def playsound(name)
        fn=Sketchup.find_support_file(name, "plugins/SketchyPhysics3/sounds/cache")
        if(fn==nil)
            fn=Sketchup.find_support_file(name, "plugins/SketchyPhysics3/sounds/")    
        end
        UI.play_sound(fn) if(fn!=nil)
    end
    def joy(name)
        name.downcase!
        deadzone=100
        case name
            when "leftx"
                if(@joystate.lX.abs>deadzone)
                    return (@joystate.lX/2000.0)+0.5
                else
                    return 0.5+(getKeyState(VK_D)?0.5:0.0)+(getKeyState(VK_A)?-0.5:0.0)
                end                    
            when "lefty"
                if(@joystate.lY.abs>deadzone)
                    return (@joystate.lY/2000.0)+0.5
                else
                    return 0.5+(getKeyState(VK_S)?0.5:0.0)+(getKeyState(VK_W)?-0.5:0.0)
                end                    
            when "rightx"
                if(@joystate.lRx.abs>deadzone)
                    return (@joystate.lRx/2000.0)+0.5
                else
                    return 0.5+(getKeyState(VK_RIGHT)?0.5:0.0)+(getKeyState(VK_LEFT)?-0.5:0.0)
                end                    
            when "righty"
                if(@joystate.lRy.abs>deadzone)
                    return (@joystate.lRy/2000.0)+0.5
                else
                    return 0.5+(getKeyState(VK_DOWN)?0.5:0.0)+(getKeyState(VK_UP)?-0.5:0.0)
                end                    
        end
        return(0.5)
    end
    @@joyButtonMapping=Hash["a",0,"b",1,"x",2,"y",3,"lb",4,"rb",5,"back",6,"start",7,"leftb",8,"rightb",9]
    def joybutton(name)
        v=@@joyButtonMapping[name.downcase]
        return @joystate.rgbButtons[v]/-128 if v!=nil
        return 0
    end
    #mousecursorloc?
    def vectorTo(name)
        
    end
    
    def sample(array,rate=1)
        v=array[(frame/rate)]
        return v if(v.class==Fixnum or v.class==Float)
        return 0
    end
    def oscillator(rate)
        inc=(2*3.141592)/rate
        pos=Math.sin(inc*(@frame))
        return (pos/2.0)+0.5
    end
    def body(name)
        #return body
    end
    def setCamera
        return if($sketchyPhysicsToolInstance==nil || $curEvalGroup==nil)
        xform=$curEvalGroup.transformation
        Sketchup.active_model.active_view.camera.set(xform.origin, xform.zaxis, [0,0,1])
    end

    
    def lookAt(target,stiff=10.0,damp=10.0)
            #if simulation isnt running then we are verifying
        if $sketchyPhysicsToolInstance==nil
            return
        end
        #get this grp.
        grp=$curEvalGroup
        xform=grp.transformation
        body=DL::PtrData.new(grp.get_attribute("SPOBJ","body",nil))

        #see if this already has a lookAtJoint
        jnt=grp.get_attribute("SPOBJ","__lookAtJoint",nil)
        if(jnt==nil)
            #create gyro and attach to the body.
            puts "creating lookat joint"
            limits=[0,0,stiff,damp]
            pinDir=xform.zaxis.to_a
            jnt=NewtonServer.createJoint("gyro",xform.origin.to_a.pack("f*"),
                pinDir.pack("f*"),
                body,nil, 
                limits.pack("f*") 
            )
                #this type of body cant freeze.
                #Hack! to force unfreeze of body
                NewtonServer.setBodyMagnetic(body,1)
                NewtonServer.setBodyMagnetic(body,0)
                #hack!         
            if(jnt!=0)
                grp.set_attribute("SPOBJ","__lookAtJoint",jnt.to_i) 
            end              
        end
        #find target.
        if target.is_a?(Array)
            pinDir=xform.origin.vector_to(target).to_a
        else
            #calc vector to target.
            targetgrp=$sketchyPhysicsToolInstance.FindGroupNamed(target)
            return if(targetgrp==nil)
            pinDir=xform.origin.vector_to(targetgrp.transformation.origin).to_a
        end
            #set pin dir to vector.
        NewtonServer.setGyroPinDir(DL::PtrData.new(grp.get_attribute("SPOBJ","__lookAtJoint",0)),pinDir.pack("f*"));
        #puts pinDir.inspect
    end
    def every(count)
        1*(@frame%count)    
    end
    def kick(body,direction,speed)
    end
    def copy(pos=nil,kick=nil,lifetime=0)
        #body=$sketchyPhysicsToolInstance.FindBodyNamed(name)
        return if($sketchyPhysicsToolInstance==nil)
        pos=Geom::Transformation.new(pos) if(pos!=nil)
        grp=$sketchyPhysicsToolInstance.copyBody($curEvalGroup,pos,lifetime)
        
        $sketchyPhysicsToolInstance.pushBody(grp,kick) if kick!=nil
        return grp
        #puts $sketchyPhysicsToolInstance.FindBodyNamed(name)
        #return newBody
    end
    def push(kick=nil)
        return if($sketchyPhysicsToolInstance==nil)
        $sketchyPhysicsToolInstance.pushBody($curEvalGroup,kick) if kick!=nil        
    end
    def showConfigDialog()
        @controllerMapping={ "leftx" => "lX", "lefty" => "lY",
                                "rightx" => "lRx", "righty" => "lRy" }

        prompts = ["leftx","lefty", "rightx", "righty"]
        values = [@controllerMapping["leftx"],@controllerMapping["lefty"],
                  @controllerMapping["rightx"],@controllerMapping["righty"]]
        results =  inputbox(prompts, values, ["lX|lY|lRx|lRy"],"Input configuration.")
        
        if (!results)
            return
        else
    #        enabled,density, linearViscosity, angularViscosity,current[0],current[1],current[2]=results
        end
    end

    def getBinding(frame)
        @frame=frame
        @joystate=JoyInput.state

        #axis range from -1000 to 1000.
        #scale to 0.0 to 1.0.
        leftx=(@joystate.lX/2000.0)+0.5
        lefty=(@joystate.lY/2000.0)+0.5
        rightx=(@joystate.lRx/2000.0)+0.5
        righty=(@joystate.lRy/2000.0)+0.5
                
        a=@joystate.rgbButtons[0]/-128
        b=@joystate.rgbButtons[1]/-128
        x=@joystate.rgbButtons[2]/-128
        y=@joystate.rgbButtons[3]/-128
        
        #keyboard emulation of joystick.
        #delta=0.05
        leftx=0.5+(getKeyState(VK_D)?0.5:0.0)+(getKeyState(VK_A)?-0.5:0.0) if(leftx>0.5-0.05 && leftx<0.5+0.05)
        lefty=0.5+(getKeyState(VK_S)?0.5:0.0)+(getKeyState(VK_W)?-0.5:0.0) if(lefty>0.5-0.05 && lefty<0.5+0.05)
        rightx=0.5+(getKeyState(VK_RIGHT)?0.5:0.0)+(getKeyState(VK_LEFT)?-0.5:0.0) if(rightx>0.5-0.05 && rightx<0.5+0.05)
        righty=0.5+(getKeyState(VK_DOWN)?0.5:0.0)+(getKeyState(VK_UP)?-0.5:0.0) if(righty>0.5-0.05 && righty<0.5+0.05)

        joyLX=leftx
        joyLY=lefty
        joyRX=0.5+(@joystate.lRz/2000.0)
        joyRY=0.5+(@joystate.lZ/2000.0)
        return binding
        
        #one of the triggers goes from 0 to 1000 the other 0 to -1000
        lt=state.lZ/1000.0
        rt=state.lRz/-1000.0

            #buttons are 0 when up and -128 when down (wierd huh?)    

        lb=state.rgbButtons[4]/-128
        rb=state.rgbButtons[5]/-128
        back=state.rgbButtons[6]/-128
        start=state.rgbButtons[7]/-128
        leftb=state.rgbButtons[8]/-128
        rightb=state.rgbButtons[9]/-128

            #dpad is wierd
        dup=state.rgdwPOV[0]
        ddown=state.rgdwPOV[0]
        dleft=state.rgdwPOV[0]
        dright=state.rgdwPOV[0]

                                       
        return binding #turn  local vars into a binding.

    end #createControllerBinding

end

#events
#  returns=time of next tick?
#onTouch(other), touching(other),unTouch(other)
#onInit,OnBegin,OnEnd
#onTick 
#onKey(key,state)

#contants 
#up/down/north/south/east/west.

#Commands
#setCamera
#moveTo
#setTarget
#toggleVar
#lookAt #pos or named target, up,
#turnTo() #name, or location
#

#turn()# rel amount. yaw pitc roll 
#face() #abs direction.
#thrust() #rel vector
#push() #abs vector
#teleport
#rotation=

class SP3xControllerContext<  ControllerContext
    def position()
        return[0,0,0] if($sketchyPhysicsToolInstance==nil)
        return $curEvalGroup.transformation.origin.to_a    
    end

    def setAnimation(name,frame)
        return if($sketchyPhysicsToolInstance==nil || $curEvalGroup==nil)
        for ent in $curEvalGroup.definition.entities
            if ent.definition.name == name+("%02d"%frame)
                ent.hidden = false
            else
                ent.hidden = true
            end
        end
    end    
    
end
end#module