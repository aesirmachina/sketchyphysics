require 'sketchup.rb'

$LOAD_PATH[0,0]=Sketchup.find_support_file("plugins/SketchyPhysics3")
puts $LOAD_PATH
require "dl"
require "dl/import"
require "dl/struct.rb"
$LOAD_PATH.delete_at(0)

#Dump top level faces
#recursivly dump the definitons
#create instances

def dumpit()
    geom=[]
    instances=[]
    definitions=Hash.new
    Sketchup.active_model.entities.each{|ent|
        if(ent.class==Sketchup::Face)
            pts=[]
            ent.mesh.points.each{|p|pts.push(p.to_a)}
            geom.push([pts.length,ent.normal.to_a,pts])
        elsif(ent.class==Sketchup::Group ||ent.class==Sketchup::ComponentInstance)
            dguid=ent.definition.guid
            if(definitions[dguid]==nil)
                out=[]
                dumpents(ent.definition.entities,out)
                definitions[dguid]=out
            end
            instances.push(ent)
        end
    }

    puts ["Geom",geom.length].inspect
    puts ["Definitions",definitions.length].inspect
    puts ["Instances",instances.length].inspect
    #dumpents(Sketchup.active_model.entities,out)
    return [geom,definitions,instances]
end
def dumpents(ents,out)
    ents.each{|ent|
        if(ent.class==Sketchup::Face)
            pts=[]
            ent.mesh.points.each{|p|pts.push(p.to_a)}
            out.push([pts.length,ent.normal.to_a,pts])
            #puts [ent.class,ent.vertices.length].inspect
        elsif(ent.class==Sketchup::Group ||ent.class==Sketchup::ComponentInstance)
            dumpents(ent.definition.entities,out)
        end
    }
end


def testRender()
    group=Sketchup.active_model.selection[0]
    tris=[]
    normals=[]
    group.entities.each{|ent|
        if(ent.typename == "Face")
            normals=normals+ent.normal.to_a
            normals=normals+ent.normal.to_a
            normals=normals+ent.normal.to_a
            ent.mesh.polygons.each_index{|pi|
                pts=ent.mesh.polygon_points_at( pi+1 ).each{|pt|
                    tris=tris+pt.to_a
                    }
                }
        end
    }
    return SketchyRender.buildDisplayList(tris.to_a.pack('f*'),normals.to_a.pack('f*'),tris.length/3).to_i     
end

module SketchyRender
    extend DL::Importable
    if(PLATFORM=="i386-mswin32")    
      #dlload(Sketchup.find_support_file("SketchyRender.dll","plugins\\SketchyPhysics3\\"))
    else
      #dlload(Sketchup.find_support_file("libNewtonServer3.dylib","plugins/SketchyPhysics3/"))
    end

	#extern "int buildDisplayList(float*,float*,int)"
end

module NewtonServer
    extend DL::Importable
    if(PLATFORM=="i386-mswin32")    
      dlload(Sketchup.find_support_file("NewtonServer3.dll","plugins\\SketchyPhysics3\\"))
      #extern "int readJoystick(float *)"
      #extern "int initDirectInput()"
      #extern "void freeDirectInput()"
    else
      dlload(Sketchup.find_support_file("libNewtonServer3.dylib","plugins/SketchyPhysics3/"))
    end

	extern "void init()"
	extern "void stop()"
	extern "void update(int)"
	extern "int fetchSingleUpdate(float*)"
	extern "char* fetchAllUpdates()"
	extern "void requestUpdate(int)"
	extern "char* NewtonCommand(char*, char*)"
	extern "int CreateBallJoint(float*,NewtonBody*,NewtonBody*)"
	extern "int CreateJoint(char*, float*,float*,NewtonBody*,NewtonBody*,float*)"
	extern "void BodySetMagnet(NewtonBody*,CMagnet* ,float*)"
	extern "int GetBodyCollision(NewtonBody*,float*,int)"
	extern "void SetBodyCenterOfMass (NewtonBody*,float *)"
	extern "void BodySetFreeze(NewtonBody*,int)"
	extern "void setJointData(void *,float*)"
	extern "void addImpulse(NewtonBody*,float*,float*)"
	extern "void MagnetMove(CMagnet* ,float*)"
	extern "CGlobalForce *addGlobalForce(float *,float *, int ,int ,int)"
    extern "CGlobalForce *addForce(NewtonBody*,float)"
    extern "void setForceStrength(CGlobalForce*,float)"
    extern "void setBodyMagnetic(NewtonBody*,int)"
    extern "void setBodySolid(NewtonBody*,int)"
    extern "void BodySetMaterial(NewtonBody*,int)"
    
	extern "CMagnet *MagnetAdd(float*)"
	extern "NewtonCollision* CreateCollision(char* ,float *,float *,float *)"
	extern "NewtonCollision* CreateCollisionMesh(float*  ,int)"
    
	extern "void DestroyBody(NewtonBody*)"    
	extern "NewtonBody* CreateBody(int ,NewtonCollision* ,int ,int, float*, float *)"
	extern "void setupBouyancy(float* ,float* ,float*)"
  extern "int CreateGear(char*,float*,float*,NewtonBody*,NewtonBody*,float)"
	extern "void setJointRotation(ControlledJoint *,float*)"
	extern "void setJointPosition(ControlledJoint *,float*)"
	extern "void setJointAccel(ControlledJoint *,float*)"
	extern "void setJointDamp(ControlledJoint *,float*)"
  extern "void setJointCollisionState(NewtonCustomJoint*,int)"

  extern "void bodySetThrust(NewtonBody*,float)"
  extern "void setGyroPinDir(CustomUpVector*,float*)"
  extern "void setBodyCollideCallback(NewtonBody*,void*)"
  extern "void glueBodies(NewtonBody*,NewtonBody*,float)"
    def doCollideCallback(body0, body1)
        #NewtonServer.glueBodies(body1,body0,3300.0)
        $sketchyPhysicsToolInstance.handleOnTouch([body0,body1])
        #puts "CollideCallback: #{body0.to_i} #{body1.to_i} "
    end
    COLLIDE_CALLBACK = (callback "void doCollideCallback(body*, body*)") 
    
    
end