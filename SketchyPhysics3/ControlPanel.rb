require 'sketchup.rb'
module MSketchyPhysics3
$PhysicsControlPanelDialog=nil

def self.showControlPanel()
    if($PhysicsControlPanelDialog==nil)
        $PhysicsControlPanelDialog=UI::WebDialog.new("SketchyPhysics Control Panel", true,"bzasdfa2ctrasaasdfa123")
    else
        updateControllerSliders()
    end

    $PhysicsControlPanelDialog.set_file( File.dirname(__FILE__)+'/SketchyUI/ControlPanel.html')
    $PhysicsControlPanelDialog.show {
            updateControllerSliders()
#puts        'document.getElementById("version").src="http://sketchyphysics2.googlecode.com/files/v30rc1.jpg?'+$physicsSessionID.to_s
            $PhysicsControlPanelDialog.execute_script_safe(
            'document.getElementById("version").src="http://sketchyphysics2.googlecode.com/files/v30rc1.jpg?'+$physicsSessionID.to_s+'";'
            )
    
    }
    $PhysicsControlPanelDialog.set_on_close { $PhysicsControlPanelDialog=nil }
    $PhysicsControlPanelDialog.add_action_callback("puts") {|d,p|puts p}

    $PhysicsControlPanelDialog.add_action_callback("setSliderValue") {|d,p|
        key=p.split("=")[0]
        value=p.split("=")[1].to_s.to_f
        $controlSliders[key].value=value;
        }
end
def self.closeControlPanel()
    $PhysicsControlPanelDialog.close() if($PhysicsControlPanelDialog!=nil)
    $PhysicsControlPanelDialog=nil
end


def self.updateControllerSliders()
    
    if($PhysicsControlPanelDialog==nil)
        return;
    end
            
    $PhysicsControlPanelDialog.execute_script_safe("while(controlSlidersTable.rows.length>0){controlSlidersTable.deleteRow(0);}")
            
    $controlSliders.each { | k,jc | 
        key=jc.name
        value=jc.value

        min=jc.min
        max=jc.max
        html="<div class='carpe_horizontal_slider_track'> "
        html+="<div class='carpe_slider_slit' ></div> "
        html+="<div class='carpe_slider' id='#{key}_slider' distance='140' display='#{key}' style='left:70px;'></div> "
        html+="</div> "
        #html+="<input id='#{key}' type='text' class='carpe_slider_display' value='#{value}' from='#{min}' to='#{max}' decimals='2'/> "

        java="row = document.getElementById('controlSlidersTable').tBodies[0].insertRow(-1);"
        java+='row.insertCell(0).innerHTML="'+key+'";'
        java+='row.insertCell(1).innerHTML="'+html+'";'
        
        html="<input id='#{key}' type='text' class='carpe_slider_display' value='#{value}' from='#{min}' to='#{max}' decimals='2'/> "
        java+='row.insertCell(1).innerHTML="'+html+'";'
        #html="<button id='#{key}'/> "
        #java+='row.insertCell(1).innerHTML="'+html+'";'
        #puts java if $debug
        $PhysicsControlPanelDialog.execute_script_safe(java)
        
    
    }
    java='setupSliders();'
    $PhysicsControlPanelDialog.execute_script_safe(java) 
end

def self.initJointControllers()
    $controlSliders=Hash.new
end
    
JointControllerStruct = Struct.new(:name,:value, :min, :max)
def self.createController(name,value,min,max)

    if($controlSliders[name]!=nil)
        return;#already have a controller with this name.
    end
    

    $controlSliders[name]=JointControllerStruct.new(name,value,min,max);
    updateControllerSliders()
end

end #module MSketchyPhysics3