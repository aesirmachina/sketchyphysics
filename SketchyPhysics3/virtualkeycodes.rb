if (PLATFORM=="i386-mswin32") #using a windows machine#
  
  VK_LBUTTON=0x01	#Left mouse button
  VK_RBUTTON=0x02	#Right mouse button
  VK_CANCEL=0x03	#Control-break processing
  VK_MBUTTON=0x04	#Middle mouse button on a three-button mouse
  VK_BACK=0x08	#BACKSPACE key
  VK_TAB=0x09	#TAB key
  VK_CLEAR=0x0C	#CLEAR key
  VK_RETURN=0x0D	#ENTER key
  VK_SHIFT=0x10	#SHIFT key
  VK_CONTROL=0x11	#CTRL key
  VK_LOPTION=0x11	#CTRL key #Option key on Mac Support.
  VK_MENU=0x12	#ALT key
  VK_PAUSE=0x13	#PAUSE key
  VK_CAPSLOCK=0x14	#CAPS LOCK key
  VK_ESCAPE=0x1B	#ESC key
  VK_SPACE=0x20	#SPACEBAR
  VK_PRIOR=0x21	#PAGE UP key
  VK_NEXT=0x22	#PAGE DOWN key
  VK_END=0x23	#END key
  VK_HOME=0x24	#HOME key
  VK_LEFT=0x25	#LEFT ARROW key
  VK_UP=0x26	#UP ARROW key
  VK_RIGHT=0x27	#RIGHT ARROW key
  VK_DOWN=0x28	#DOWN ARROW key
  VK_SELECT=0x29	#SELECT key
  VK_EXECUTE=0x2B	#EXECUTE key
  VK_SNAPSHOT=0x2C	#PRINT SCREEN key
  VK_INSERT=0x2D	#INS key
  VK_DELETE=0x2E	#DEL key
  VK_HELP=0x2F	#HELP key
  VK_LWIN=0x5B	#Left Windows key on a Microsoft Natural Keyboard
  VK_RWIN=0x5C	#Right Windows key on a Microsoft Natural Keyboard
  VK_APPS=0x5D	#Applications key on a Microsoft Natural Keyboard
  VK_NUMPAD0=0x60	#Numeric keypad 0 key
  VK_NUMPAD1=0x61	#Numeric keypad 1 key
  VK_NUMPAD2=0x62	#Numeric keypad 2 key
  VK_NUMPAD3=0x63	#Numeric keypad 3 key
  VK_NUMPAD4=0x64	#Numeric keypad 4 key
  VK_NUMPAD5=0x65	#Numeric keypad 5 key
  VK_NUMPAD6=0x66	#Numeric keypad 6 key
  VK_NUMPAD7=0x67	#Numeric keypad 7 key
  VK_NUMPAD8=0x68	#Numeric keypad 8 key
  VK_NUMPAD9=0x69	#Numeric keypad 9 key
  VK_MULTIPLY=0x6A	#Multiply key
  VK_ADD=0x6B	#Add key
  VK_SEPARATOR=0x6C	#Separator key
  VK_SUBTRACT=0x6D	#Subtract key
  VK_DECIMAL=0x6E	#Decimal key
  VK_DIVIDE=0x6F	#Divide key
  VK_F1=0x70	#F1 key
  VK_F2=0x71	#F2 key
  VK_F3=0x72	#F3 key
  VK_F4=0x73	#F4 key
  VK_F5=0x74	#F5 key
  VK_F6=0x75	#F6 key
  VK_F7=0x76	#F7 key
  VK_F8=0x77	#F8 key
  VK_F9=0x78	#F9 key
  VK_F10=0x79	#F10 key
  VK_F11=0x7A	#F11 key
  VK_F12=0x7B	#F12 key
  VK_F13=0x7C	#F13 key
  VK_F14=0x7D	#F14 key
  VK_F15=0x7E	#F15 key
  VK_F16=0x7F	#F16 key
  VK_F17=0x80	#F17 key
  VK_F18=0x81	#F18 key
  VK_F19=0x82	#F19 key
  VK_F20=0x83	#F20 key
  VK_F21=0x84	#F21 key
  VK_F22=0x85	#F22 key
  VK_F23=0x86	#F23 key
  VK_F24=0x87	#F24 key
  VK_NUMLOCK=0x90	#NUM LOCK key
  VK_SCROLL=0x91	#SCROLL LOCK key
  VK_LSHIFT=0xA0	#Left SHIFT
  VK_RSHIFT=0xA1	#Right SHIFT
  VK_LCONTROL=0xA2	#Left CTRL
  VK_RCONTROL=0xA3	#Right CTRL
  VK_LMENU=0xA4	#Left ALT
  VK_RMENU=0xA5	#Right ALT
  VK_ATTN=0xF6	#ATTN key
  VK_CRSEL=0xF7	#CRSEL key
  VK_EXSEL=0xF8	#EXSEL key
  VK_EREOF=0xF9	#Erase EOF key
  VK_PLAY=0xFA	#PLAY key
  VK_ZOOM=0xFB	#ZOOM key
  VK_PA1=0xFD	#PA1 key
  VK_OEM_CLEAR=0xFE	#CLEAR key
  
    VK_A=0x41
    VK_B= 0x42
    VK_C= 0x43
    VK_D= 0x44
    VK_E= 0x45
    VK_F= 0x46
    VK_G= 0x47
    VK_H= 0x48
    VK_I= 0x49
    VK_J= 0x4A
    VK_K= 0x4B
    VK_L= 0x4C
    VK_M= 0x4D
    VK_N= 0x4E
    VK_O= 0x4F
    VK_P= 0x50
    VK_Q= 0x51
    VK_R= 0x52
    VK_S= 0x53
    VK_T= 0x54
    VK_U= 0x55
    VK_V= 0x56
    VK_W= 0x57
    VK_X= 0x58
    VK_Y= 0x59
    VK_Z= 0x5A

    KeyNames=Hash[
    "LBUTTON",            VK_LBUTTON,             
    "RBUTTON",            VK_RBUTTON,             
    "CANCEL",             VK_CANCEL,              
    "MBUTTON",            VK_MBUTTON,             
    "BACK",               VK_BACK,                
    "TAB",                VK_TAB,                 
    "CLEAR",              VK_CLEAR,               
    "RETURN",             VK_RETURN,              
    "SHIFT",              VK_SHIFT,               
    "CONTROL",            VK_CONTROL,             
    "LOPTION",            VK_LOPTION,             
    "MENU",               VK_MENU,                
    "PAUSE",              VK_PAUSE,               
    "CAPSLOCK",           VK_CAPSLOCK,            
    "ESCAPE",             VK_ESCAPE,              
    " ",              VK_SPACE,               
    "SPACE",              VK_SPACE,               
    "PRIOR",              VK_PRIOR,               
    "NEXT",               VK_NEXT,                
    "END",                VK_END,                 
    "HOME",               VK_HOME,                
    "LEFT",               VK_LEFT,                
    "UP",                 VK_UP,                  
    "RIGHT",              VK_RIGHT,               
    "DOWN",               VK_DOWN,                
    "SELECT",             VK_SELECT,              
    "EXECUTE",            VK_EXECUTE,             
    "SNAPSHOT",           VK_SNAPSHOT,            
    "INSERT",             VK_INSERT,              
    "DELETE",             VK_DELETE,              
    "HELP",               VK_HELP,                
    "LWIN",               VK_LWIN,                
    "RWIN",               VK_RWIN,                
    "APPS",               VK_APPS,                
    "NUMPAD0",            VK_NUMPAD0,             
    "NUMPAD1",            VK_NUMPAD1,             
    "NUMPAD2",            VK_NUMPAD2,             
    "NUMPAD3",            VK_NUMPAD3,             
    "NUMPAD4",            VK_NUMPAD4,             
    "NUMPAD5",            VK_NUMPAD5,             
    "NUMPAD6",            VK_NUMPAD6,             
    "NUMPAD7",            VK_NUMPAD7,             
    "NUMPAD8",            VK_NUMPAD8,             
    "NUMPAD9",            VK_NUMPAD9,             
    "MULTIPLY",           VK_MULTIPLY,            
    "ADD",                VK_ADD,                 
    "SEPARATOR",          VK_SEPARATOR,           
    "SUBTRACT",           VK_SUBTRACT,            
    "DECIMAL",            VK_DECIMAL,             
    "DIVIDE",             VK_DIVIDE,              
    "F1",                 VK_F1,                  
    "F2",                 VK_F2,                  
    "F3",                 VK_F3,                  
    "F4",                 VK_F4,                  
    "F5",                 VK_F5,                  
    "F6",                 VK_F6,                  
    "F7",                 VK_F7,                  
    "F8",                 VK_F8,                  
    "F9",                 VK_F9,                  
    "F10",                VK_F10,                 
    "F11",                VK_F11,                 
    "F12",                VK_F12,                 
    "F13",                VK_F13,                 
    "F14",                VK_F14,                 
    "F15",                VK_F15,                 
    "F16",                VK_F16,                 
    "F17",                VK_F17,                 
    "F18",                VK_F18,                 
    "F19",                VK_F19,                 
    "F20",                VK_F20,                 
    "F21",                VK_F21,                 
    "F22",                VK_F22,                 
    "F23",                VK_F23,                 
    "F24",                VK_F24,                 
    "NUMLOCK",            VK_NUMLOCK,             
    "SCROLL",             VK_SCROLL,              
    "LSHIFT",             VK_LSHIFT,              
    "RSHIFT",             VK_RSHIFT,              
    "LCONTROL",           VK_LCONTROL,            
    "RCONTROL",           VK_RCONTROL,            
    "LMENU",              VK_LMENU,               
    "RMENU",              VK_RMENU,               
    "ATTN",               VK_ATTN,                
    "CRSEL",              VK_CRSEL,               
    "EXSEL",              VK_EXSEL,               
    "EREOF",              VK_EREOF,               
    "PLAY",               VK_PLAY,                
    "ZOOM",               VK_ZOOM,                
    "PA1",                VK_PA1,                 
    "OEM_CLEAR",          VK_OEM_CLEAR,           
    "A",                  VK_A,                   
    "B",                  VK_B,                   
    "C",                  VK_C,                   
    "D",                  VK_D,                   
    "E",                  VK_E,                   
    "F",                  VK_F,                   
    "G",                  VK_G,                   
    "H",                  VK_H,                   
    "I",                  VK_I,                   
    "J",                  VK_J,                   
    "K",                  VK_K,                   
    "L",                  VK_L,                   
    "M",                  VK_M,                   
    "N",                  VK_N,                   
    "O",                  VK_O,                   
    "P",                  VK_P,                   
    "Q",                  VK_Q,                   
    "R",                  VK_R,                   
    "S",                  VK_S,                   
    "T",                  VK_T,                   
    "U",                  VK_U,                   
    "V",                  VK_V,                   
    "W",                  VK_W,                   
    "X",                  VK_X,                   
    "Y",                  VK_Y,                   
    "Z",                  VK_Z
    ]
    
else #must be using a mac on osx so here are the codes#
  
  VK_A = 0x00
  VK_B = 0x0B
  VK_C = 0x08
  VK_D = 0x02
  VK_E = 0x0E
  VK_F = 0x03
  VK_G = 0x05
  VK_H = 0x04
  VK_I = 0x22
  VK_J = 0x26
  VK_K = 0x28
  VK_L = 0x25
  VK_M = 0x2E
  VK_N = 0x2D
  VK_O = 0x1F
  VK_P = 0x23
  VK_Q = 0x0C
  VK_R = 0x0F
  VK_S = 0x01
  VK_T = 0x11
  VK_U = 0x20
  VK_V = 0x09
  VK_W = 0x0D
  VK_X = 0x07
  VK_Y = 0x10
  VK_Z = 0x06

  VK_1 = 0x12
  VK_2 = 0x13
  VK_3 = 0x14
  VK_4 = 0x15
  VK_5 = 0x17
  VK_6 = 0x16
  VK_7 = 0x1A
  VK_8 = 0x1C
  VK_9 = 0x19
  VK_0 = 0x1D
  
  VK_EQUALS = 0x18
  VK_MINUS = 0x1B
  VK_CLOSEBRACE = 0x1E
  VK_OPENBRACE = 0x21
  VK_ENTER = 0X24
  VK_QUOTE = 0x27
  VK_SEMICOLON = 0x29
  VK_BACKSLASH = 0x2A
  VK_COMMA = 0x2B
  VK_SLASH = 0x2C
  VK_PERIOD = 0x2F
  VK_TAB = 0x30
  VK_SPACE = 0x31
  VK_BACKQUOTE = 0x32
  VK_BACKSPACE = 0x33
  VK_ENTER = 0x34
  VK_RETURN = 0x34
  VK_ESC = 0x35
  VK_RCOMMAND = 0x36
  VK_LCOMMAND = 0x37
  VK_LSHIFT = 0x38
  VK_CAPSLOCK = 0x39
  VK_LOPTION = 0x3A
  VK_LCONTROL = 0x3B
  VK_CONTROL = 0x3B
  VK_RSHIFT = 0x3C
  VK_ROPTION = 0x3D
  VK_RCONTROL = 0x3E
  VK_PERIOD_PAD = 0x41
  VK_ASTERISK_PAD = 0x43
  VK_PLUS_PAD = 0x45
  VK_NUMLOCK = 0x47
  VK_SLASH_PAD = 0x4B
  VK_ENTER_PAD = 0x4C
  VK_MINUS_PAD = 0x4E
  VK_EQUALS_PAD = 0x51
	
  VK_0_PAD = 0x52
  VK_1_PAD = 0x53
  VK_2_PAD = 0x54
  VK_3_PAD = 0x55
  VK_4_PAD = 0x56
  VK_5_PAD = 0x57
  VK_6_PAD = 0x58
  VK_7_PAD = 0x59
  VK_8_PAD = 0x5B
  VK_9_PAD = 0x5C
	
  VK_F1 = 0x7A
  VK_F2 = 0x78
  VK_F4 = 0x76
  VK_F5 = 0x60
  VK_F6 = 0x61
  VK_F7 = 0x62
  VK_F3 = 0x63
  VK_F8 = 0x64
  VK_F9 = 0x65
  VK_F10 = 0x6D
  VK_F11 = 0x67
  VK_F12 = 0x6F
	
  VK_PRTSCR = 0x69
  VK_SCRLOCK = 0x6B
  VK_PAUSE = 0x71
  VK_INSERT = 0x72
  VK_HOME = 0x73
  VK_PGUP = 0x74
  VK_DEL = 0x75
  VK_END = 0x77
  VK_PGDN = 0x79
  VK_LEFT = 0x7B
  VK_RIGHT = 0x7C
  VK_DOWN = 0x7D
  VK_UP = 0x7E

  KeyNames=Hash[          
  "TAB",                VK_TAB,                             
  "RETURN",             VK_RETURN,              
  "SHIFT",              VK_SHIFT,               
  "CONTROL",            VK_CONTROL,             
  "LOPTION",            VK_LOPTION,             
  "CAPSLOCK",           VK_CAPSLOCK,            
  "ESCAPE",             VK_ESC,              
  " ",                  VK_SPACE,               
  "SPACE",              VK_SPACE,               
  "LEFT",               VK_LEFT,                
  "UP",                 VK_UP,                  
  "RIGHT",              VK_RIGHT,               
  "DOWN",               VK_DOWN,
  "END",                VK_END,
  "DELETE",             VK_DEL,              
  "NUMPAD0",            VK_0_PAD,             
  "NUMPAD1",            VK_1_PAD,             
  "NUMPAD2",            VK_2_PAD,             
  "NUMPAD3",            VK_3_PAD,             
  "NUMPAD4",            VK_4_PAD,             
  "NUMPAD5",            VK_5_PAD,             
  "NUMPAD6",            VK_6_PAD,             
  "NUMPAD7",            VK_7_PAD,             
  "NUMPAD8",            VK_8_PAD,             
  "NUMPAD9",            VK_9_PAD,             
  "MULTIPLY",           VK_ASTERISK_PAD,            
  "ADD",                VK_PLUS_PAD,                 
  "SUBTRACT",           VK_MINUS,            
  "DECIMAL",            VK_PERIOD_PAD,             
  "DIVIDE",             VK_SLASH_PAD,              
  "F1",                 VK_F1,                  
  "F2",                 VK_F2,                  
  "F3",                 VK_F3,                  
  "F4",                 VK_F4,                  
  "F5",                 VK_F5,                  
  "F6",                 VK_F6,                  
  "F7",                 VK_F7,                  
  "F8",                 VK_F8,                  
  "F9",                 VK_F9,                  
  "F10",                VK_F10,                 
  "F11",                VK_F11,                 
  "F12",                VK_F12,                 
  "LSHIFT",             VK_LSHIFT,              
  "RSHIFT",             VK_RSHIFT,              
  "LCONTROL",           VK_LCONTROL,            
  "RCONTROL",           VK_RCONTROL,            
  "A",                  VK_A,                   
  "B",                  VK_B,                   
  "C",                  VK_C,                   
  "D",                  VK_D,                   
  "E",                  VK_E,                   
  "F",                  VK_F,                   
  "G",                  VK_G,                   
  "H",                  VK_H,                   
  "I",                  VK_I,                   
  "J",                  VK_J,                   
  "K",                  VK_K,                   
  "L",                  VK_L,                   
  "M",                  VK_M,                   
  "N",                  VK_N,                   
  "O",                  VK_O,                   
  "P",                  VK_P,                   
  "Q",                  VK_Q,                   
  "R",                  VK_R,                   
  "S",                  VK_S,                   
  "T",                  VK_T,                   
  "U",                  VK_U,                   
  "V",                  VK_V,                   
  "W",                  VK_W,                   
  "X",                  VK_X,                   
  "Y",                  VK_Y,                   
  "Z",                  VK_Z
  ]

end
                    




