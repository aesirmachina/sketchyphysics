require 'sketchup.rb'
module MSketchyPhysics3
def PickGroupEmbedded(x,y,view)
	ph=view.pick_helper
	num=ph.do_pick x,y
	
	item=nil
	
	path=ph.path_at(1)
    
    return item if (path==nil)
    
	#puts path
	path.length.downto(0){|i|
		if(path[i].class==Sketchup::Group && 
                (path[i].parent==Sketchup.active_model || (path[i].get_attribute("SPJOINT","name",nil)!=nil)))
			item=path[i]
			#puts "ParentGroup="+item.to_s
			break
		end
	}
	
	return item;
	
	puts "element_at 1"+ph.element_at(1).to_s
	topGroup=ph.element_at(1)
	if(topGroup.class==Sketchup::Group)
		puts "topGroup xform:"+topGroup.transformation.origin.to_s
	
		xform=ph.transformation_at(1)
		puts "xform at 1:"+xform.origin.to_s
		
		xform=xform*topGroup.transformation.inverse
		puts "xform 1 inverse:"+xform.origin.to_s
		
		face=ph.picked_face
		if(face.parent.class==Sketchup::ComponentDefinition)
			face.parent.instances.each{|ci|
				puts ci.transformation.origin
				if(ci.transformation.origin==xform.origin)
					#Sketchup.active_model.selection.clear
					#Sketchup.active_model.selection.add ci
					puts "Found" + ci.to_s
					return(ci)
				end
			
			}
		end
	end		
	puts "Found topGroup " + topGroup.to_s
	return topGroup
end


def self.IsValidContextMenuItem(selection)

	selection.each{|ent|
		if(ent.class==Sketchup::ComponentInstance ||ent.class==Sketchup::Group)
			return true
		end
	}
	
	return false
end
def self.togglePhysicsAttribute(selection,attrib,defaultValue) 
	selection.each{|ent|  
		if(ent.class==Sketchup::ComponentInstance || ent.class==Sketchup::Group) #for each selected component.
			bNewState=ent.get_attribute("SPOBJ", attrib, defaultValue)
			bNewState=!bNewState
			
			ent.set_attribute("SPOBJ", attrib, bNewState)
			#puts "Set "+ent.to_s+" "+attrib+"="+value.to_s
		end
	}
end
def self.setPhysicsAttribute(selection,attrib,value) 
	#puts "Set "+attrib+"="+value
	if(selection.class==Sketchup::ComponentInstance || selection.class==Sketchup::Group)#is it really a single group component
		selection.set_attribute("SPOBJ", attrib, value)
		#puts "Set "+selection.to_s+" "+attrib+"="+value.to_s
        if(attrib=="shape"&& value!=nil)
            selection.name=value  #change group name to be shape.
        end        
		return
	end
	
	selection.each{|ent|  
		if(ent.class==Sketchup::ComponentInstance || ent.class==Sketchup::Group) #for each selected component.
			ent.set_attribute("SPOBJ", attrib, value)
			#puts "Set "+ent.to_s+" "+attrib+"="+value.to_s
            if(attrib=="shape" && value!=nil)
                #puts("setting shape")
                ent.name=value  #change group name to be shape.
            end        
		end
	}
end
def self.validate_physicsAttribute(key,defaultValue)
	selection=Sketchup.active_model.selection
	#return MF_GRAYED if not selection.single_object? 
	if( selection.first.get_attribute("SPOBJ", key, defaultValue))
		return MF_CHECKED
	end
	return MF_UNCHECKED 
end
def self.validate_physicsAttributeString(key,value)
	selection=Sketchup.active_model.selection
	#return MF_GRAYED if not selection.single_object? 
	val=selection.first.get_attribute("SPOBJ", key, "")
	if( val==value )
		return MF_CHECKED
	end
	return MF_UNCHECKED 
end

def makePhysicsCylinder(radius,depth)
    
	group=Sketchup.active_model.active_entities.add_group 
    circle = group.entities.add_circle([0,0,0], [0,0,1], radius, 24)
    base = group.entities.add_face circle
    depth = -depth if base.normal.dot(Z_AXIS) < 0.0
    base.pushpull depth
	MSketchyPhysics3::setPhysicsAttribute(group,"shape","cylinder")
    group.name="cylinder"
    return(group)
end
def createDefaultScene()
return #broken.
	Sketchup.active_model.start_operation "Create Default Physics Scene"
	MSketchyPhysics3::checkModelUnits()
	cube=makePhysicsCube(Sketchup.active_model.active_entities,1000,1000,-20)
	cube.transform!(Geom::Transformation.new([0,0,-10]))
	MSketchyPhysics3::setPhysicsAttribute(cube,"shape","staticmesh")#Change to static mesh.
    cube.set_attribute("SPOBJ", "staticmesh", true)
    cube.name="staticmesh"
    
	cube=makePhysicsCube(Sketchup.active_model.active_entities,12,12,12)
	cube.transform!(Geom::Transformation.new([-24,0,6]))

	cyl=makePhysicsCylinder(12/2,12)
	cyl.transform!(Geom::Transformation.new([-36,-14,0]))
    
    grp=create_cone(Sketchup.active_model.active_entities,6,12,10,0) #rad,hei,segs,taper
    grp.set_attribute( "SPOBJ", "shape", "cone")
    grp.transform!(Geom::Transformation.new([-24,-24,0], [0,0,1]))

	grp=create_sphere(6,12)#last number is seg count
    grp.set_attribute( "SPOBJ", "shape", "sphere")
    n=Geom::Vector3d.new([0,0,1])
    n.length=12/2
    center=Geom::Point3d.new([0,0,0])+n
    grp.transform!(Geom::Transformation.new(center, n))
    grp.transform!(Geom::Transformation.new([-24,-36,0], [0,0,1]))

	Sketchup.active_model.commit_operation
end

def disconnectJoint(child,jointName)
	puts "delete "+jointName
	puts Sketchup.active_model.selection.first.get_attribute("SPOBJ",jointName,nil)
	Sketchup.active_model.selection.first.delete_attribute("SPOBJ",jointName)
	puts Sketchup.active_model.selection.first.get_attribute("SPOBJ",jointName,nil)
end
def putsAllAttributes
    Sketchup.active_model.definitions.each{ |cd|
        cd.instances.each{|ci|
            puts ci.to_s+"={"
            if(ci.attribute_dictionaries!=nil)
                ci.attribute_dictionaries.each{|atd|
                puts atd.name+":"
                atd.each_pair { | key, value | puts key.to_s+"="+value.to_s }
                }
            puts "}"
            end
        }
    #get_attribute("SPOBJ",jointName,nil)
        #componentDef.attribute_dictionary("SPOBJ")
    }    
    
end

#~ def findJoint(jointName)
    #~ joints=[]
    #~ Sketchup.active_model.definitions.each{ |cd|
        #~ cd.instances.each{|ci|
            #~ jn=ci.get_attribute("SPJOINT","name",nil)
            #~ if(jn!=nil && jn==jointName)
                #~ joints.push(ci)
            #~ end
            #~ }
        #~ }
    #~ #puts joints
    #~ Sketchup.active_model.selection.add(joints)
    #~ return joints    
#~ end
#~ def disconnectAllJoints()
	#~ pn=0
	#~ while(Sketchup.active_model.selection.first.get_attribute("SPOBJ","jointParent"+pn.to_s,nil)!=nil)
		#~ #pname="jointParent"+pn.to_s
		#~ Sketchup.active_model.selection.first.delete_attribute("SPOBJ","jointParent"+pn.to_s)
		#~ pn=pn+1;
	#~ end
	#~ Sketchup.active_model.selection.first.set_attribute("SPOBJ","numParents",0)
#~ end
#~ def selectAllJoints()
	#~ count=0
    #~ ent=Sketchup.active_model.selection.first
    #~ np=0 #ent.get_attribute("SPOBJ","numParents",0)
    #~ Sketchup.active_model.selection.clear
	#~ while(np<10)
		#~ #pname="jointParent"+pn.to_s
        #~ jn=ent.get_attribute("SPOBJ","jointParent"+np.to_s)
        #~ if(jn!=nil)
            #~ findJoint(jn)
        #~ end
        #~ np=np+1
	#~ end
#~ end



def purgeAllAnimation
    Sketchup.active_model.entities.each{|ent| 
        if(ent.attribute_dictionaries!=nil)
            ent.attribute_dictionaries.delete("xxanimationdictionary")
            ent.attribute_dictionaries.delete("animationdictionary")
            ent.attribute_dictionaries.delete("SRPAnimation")
        end
        }
end


def self.editPhysicsSettings()
    dict=Sketchup.active_model.attribute_dictionary("SPSETTINGS")
    if(dict==nil)
        setDefaultPhysicsSettings()
        dict=Sketchup.active_model.attribute_dictionary("SPSETTINGS")
    end
    
        #get framerate or set default.
    @frameRate=Sketchup.active_model.set_attribute( "SPSETTINGS", "framerate", 
        Sketchup.active_model.get_attribute( "SPSETTINGS", "framerate", 3))
    
    prompts=dict.keys		
    values=dict.values		
    results = inputbox prompts, values,"Physics Settings"
    if (results != values && results != false)
        0.upto(prompts.length-1) do |xx|
           dict[prompts[xx]]=results[xx]
        end
    end
end
        
if( not file_loaded?("SketchyPhysicsUtil.rb") )
	#submenu=UI.menu("Plugins")
	#submenu.add_separator
#	item =submenu.add_item("SketchyPhysics"){}
#	item = submenu.add_item("  Physics Settings") {editPhysicsSettings()}
	#item = submenu.add_item("  Create Default Scene") {createDefaultScene() }
	#item = submenu.add_item("  Purge embedded animation") {purgeAllAnimation() }
	#item = submenu.add_item("  Reload scripts") {reload() }
	
	#item = submenu.add_item("  Create Wheel") {}
	#item = submenu.add_item("  Create Door") {}

	#submenu.add_separator		
			

	#submenu.set_validation_proc(item) {return MF_CHECKED }
	
	
	UI.add_context_menu_handler { |menu|
		selection=Sketchup.active_model.selection
		if(MSketchyPhysics3::IsValidContextMenuItem(selection))
			submenu=menu.add_submenu("SketchyPhysics")
			menu=submenu
			#submenu.add_separator
			#submenu.add_item("SketchyPhysics"){}
#			menu=submenu.add_submenu("SketchyPhysics")
            
            primList=["box","sphere","cylinder","cone","capsule","chamfer","convexhull","staticmesh"]
		
			if(!selection.single_object?)
				stateSubmenu=menu.add_submenu("Group State Change")
				item = stateSubmenu.add_item("Freeze") { MSketchyPhysics3::setPhysicsAttribute(selection,"frozen",true) }
				item = stateSubmenu.add_item("UnFreeze") { MSketchyPhysics3::setPhysicsAttribute(selection,"frozen",false) }
				item = stateSubmenu.add_item("Static") { MSketchyPhysics3::setPhysicsAttribute(selection,"static",true) }
				item = stateSubmenu.add_item("Movable") { MSketchyPhysics3::setPhysicsAttribute(selection,"static",false) }
				item = stateSubmenu.add_item("Ignore") { MSketchyPhysics3::setPhysicsAttribute(selection,"ignore",true) }
				item = stateSubmenu.add_item("Clear Ignore flag") { MSketchyPhysics3::setPhysicsAttribute(selection,"ignore",false) }
				item = stateSubmenu.add_item("Magnetic") { MSketchyPhysics3::setPhysicsAttribute(selection,"magnetic",true) }
				item = stateSubmenu.add_item("Not magnetic") { MSketchyPhysics3::setPhysicsAttribute(selection,"magnetic",false) }
				item = stateSubmenu.add_item("Auto freeze") { MSketchyPhysics3::setPhysicsAttribute(selection,"noautofreeze",false) }
				item = stateSubmenu.add_item("No auto freeze") { MSketchyPhysics3::setPhysicsAttribute(selection,"noautofreeze",true) }
				#item = stateSubmenu.add_item("Embed geometry") { setPhysicsAttribute(selection,"embedgeometry",true) }
				#item = stateSubmenu.add_item("Do not embed geometry") { setPhysicsAttribute(selection,"embedgeometry",false) }
				
                shapeSubmenu=menu.add_submenu("Group Shape Change")
                item=shapeSubmenu.add_item("default") {MSketchyPhysics3::setPhysicsAttribute(selection,"shape",nil)} 	
                primList.each{|prim|
					item=shapeSubmenu.add_item(prim) { 
                    	MSketchyPhysics3::setPhysicsAttribute(selection,"shape",prim)
						} 	
						
					}		
					
			
			else
			#if(selection.single_object?)
				selected=selection.first

				tstr="State:"
				if(selected.get_attribute("SPOBJ", "frozen", false))
					tstr+="Frozen "
				end
				if(selected.get_attribute("SPOBJ", "static", false))
					tstr+="Static "
				end
				if(selected.get_attribute("SPOBJ", "ignore", false))
					tstr+="Ignore "
				end
				if(selected.get_attribute("SPOBJ", "nocollide", false))
					tstr+="Nocollide "
				end
				stateSubmenu=menu.add_submenu(tstr)
				item = stateSubmenu.add_item("Frozen") { MSketchyPhysics3::togglePhysicsAttribute(selection,"frozen",false) }
				stateSubmenu.set_validation_proc(item) {MSketchyPhysics3::validate_physicsAttribute("frozen",false) }
				item = stateSubmenu.add_item("Static") { MSketchyPhysics3::togglePhysicsAttribute(selection,"static",false) }
				stateSubmenu.set_validation_proc(item) {MSketchyPhysics3::validate_physicsAttribute("static",false) }
				item = stateSubmenu.add_item("Ignore") { MSketchyPhysics3::togglePhysicsAttribute(selection,"ignore",false) }
				stateSubmenu.set_validation_proc(item) {MSketchyPhysics3::validate_physicsAttribute("ignore",false) }
				#item = stateSubmenu.add_item("NoCollision") { togglePhysicsAttribute(selection,"nocollide",false) }
				#stateSubmenu.set_validation_proc(item) {validate_physicsAttribute("nocollide",false) }
				#item = stateSubmenu.add_item("EmbedGeometry") { togglePhysicsAttribute(selection,"embedgeometry",false) }
				#stateSubmenu.set_validation_proc(item) {validate_physicsAttribute("embedgeometry",false) }
							
				
				shape=selected.get_attribute("SPOBJ", "shape", "default")
				#bFrozen=selection.first.get_attribute("SPOBJ", "frozen", false)
				shapeSubmenu=submenu.add_submenu("Shape:"+shape)
				item=shapeSubmenu.add_item("default") {MSketchyPhysics3::setPhysicsAttribute(selection,"shape",nil)} 
				primList.each{|prim|
					item=shapeSubmenu.add_item(prim) { 
                    	MSketchyPhysics3::setPhysicsAttribute(selection,"shape",prim)
						} 	
						shapeSubmenu.set_validation_proc(item) {MSketchyPhysics3::validate_physicsAttributeString("shape",prim) }
					}		
					
				#~ parentSubmenu=submenu.add_submenu("  Joint connections:"+selected.get_attribute("SPOBJ","numParents",0).to_s)
				#~ item = parentSubmenu.add_item("Set New Parent Joint") {selectParentJoint(selected)} 

				#~ item = parentSubmenu.add_item("Disconnect All") { disconnectAllJoints() }
				#~ item = parentSubmenu.add_item("Select all joints") { selectAllJoints() }
				
				#~ pn=0
				#~ while(selected.get_attribute("SPOBJ","jointParent"+pn.to_s,nil)!=nil)
					#~ pname="jointParent"+pn.to_s
					#~ #type = selected.get_attribute(jname,"type",nil)
					#~ jsm = parentSubmenu.add_submenu("Parent:"+selected.get_attribute("SPOBJ",pname,nil).to_s)
					#~ item = jsm.add_item("Select Joint") {} 
					#~ #puts pn
					#~ item = jsm.add_item("Disconnect All") { disconnectAllJoints() }
					#~ pn=pn+1
				#~ end			
			end			
			#--------------debug
			submenu=menu.add_submenu("Debug")
			
			item = submenu.add_item("Readback Collision Geometry") { MSketchyPhysics3::togglePhysicsAttribute(selection,"showgeometry",false) }
			submenu.set_validation_proc(item) {MSketchyPhysics3::validate_physicsAttribute("showgeometry",false) }

            item = menu.add_item("Physics Copy") { 
                Sketchup.active_model.selection.each{|ent|
                    if(ent.class==Sketchup::Group)
                        SketchyPhysicsClient::physicsSafeCopy()
                    end
                }
            }
		end			
		}
    end
    

file_loaded("SketchyPhysicsUtil.rb")
end #module MSketchyPhysics3

