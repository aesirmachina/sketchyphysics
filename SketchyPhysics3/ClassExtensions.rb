require 'sketchup.rb'
module MSketchyPhysics3
$UniqueObjects=Hash.new()
def registerUniqueObject(id,ent)
    $UniqueObjects[id]=ent  
    
end

def updateUniqueObjects()
    puts "updateUniqueObjects"
    Sketchup.active_model.definitions.each{ |cd|
        cd.instances.each{|ci|
            id=ci.get_attribute("__uniqueid","id",nil)
            if(id!=nil)
                $UniqueObjects[id]=ci 
            end
        }
    }   
end
def findUniqueObject(id)
    ent=$UniqueObjects[id]

    if(ent==nil)
        #puts("failed to find:"+id.to_s)
        updateUniqueObjects()
        ent=$UniqueObjects[id]
    end
       
    return ent
end


class Sketchup::Group
	def getUniqueID #return a (hopefully) unique and perm ID for a given group.
        id=get_attribute("__uniqueid","id",nil)
        if(id==nil)
            id=rand()
            set_attribute("__uniqueid","id",id)
            registerUniqueObject(id,self)
            findUniqueObject(id)
        end
        return id 
	end
end


#this changes the definition of ComponentInstance 
#by adding an entities method that referers to the definition. 
#It vastly simplifies the script but it may be "bad" overall. 
#Can I somehow localize it to my library?
class Sketchup::ComponentInstance
	def entities # make it so you can use CompontentInstance.entites same as Group.entites.
		return(definition.entities)
	end
end

class Sketchup::Group
	def definition # make it so you can use group.definition
		return(entities[0].parent)
	end
    
end
class UI::WebDialog
    #this function should be removed and every call changed to execute_script.
	def execute_script_safe(str) 
        execute_script(str)
	end
    
end

class Sketchup::Model
    
    def bodies
        bodies=[]
        Sketchup.active_model.definitions.each{|cd|
            cd.instances.each{|ci|
                if(ci.parent.class==Sketchup::Model &&!ci.isJoint?)
                    bodies.push(ci)
                end
            }
        }
        return bodies
    end
    def joints
        joints=[]
        Sketchup.active_model.definitions.each{|cd|
            cd.instances.each{|ci|
                if(ci.parent.class==Sketchup::Model &&ci.isJoint?)
                    bodies.push(ci)
                end
            }
        }
        return joints
    end    
   
end

class Geom::Transformation
    def get_scaling
        #figure out the scaling of the xform.
        [ (Geom::Vector3d.new(1.0,0,0).transform!(self)).length,
            (Geom::Vector3d.new(0,1.0,0).transform!(self)).length,
            (Geom::Vector3d.new(0,0,1.0).transform!(self)).length]
    end
    
    def unscaled
        Geom::Transformation.new(xaxis,yaxis,zaxis,origin)
    end
       
end

class Sketchup::Group
    
    def ignore=(flag)
        set_attribute("SPOBJ", "ignore", flag)
    end
    def ignore
        get_attribute("SPOBJ", "ignore", false)
    end
    def shape=(shp)
        set_attribute("SPOBJ", "shape", shp)
    end
    def shape
        get_attribute("SPOBJ", "shape", nil)
    end
    def contains(ent)
        entities.each{|e|
            return true if(e==ent)
            if(e.class==Sketchup::Group)
                if(e.contains(ent))
                    return true
                end
            end
        }
        return false        
    end
    
    def joints()
        entities.select{|ent| 
            ent.class==Sketchup::Group && ent.isJoint?
        }
    end

    def create_collision(world,xform)
        return nil if(ignore)

            #find the real size of the bounding box and offset to center.
        bb=definition.bounds
        scale=xform.get_scaling
        size=[bb.width*scale[0],bb.height*scale[1],bb.depth*scale[2]]
        center=[bb.center.x*scale[0],bb.center.y*scale[1],bb.center.z*scale[2]]

        noscale=xform.unscaled
        center= Geom::Transformation.new(center)        
        finalXform=noscale*center

        col=NewtonServer.createCollision(shape,size.pack('f*'),finalXform.to_a.pack('f*'),
                verts.to_a.pack('f*')) #if convexhull
        #~ col=nil
        #~ case shape
            #~ when "box"
                #~ col=Newton.newtonCreateBox(world,size[0],size[1],size[2],finalXform.to_a.pack('f*'))
            #~ when "sphere"
                #~ col=Newton.newtonCreateSphere(world,size[0],size[1],size[2],finalXform.to_a.pack('f*'))
            #~ when "cylinder"
                #~ col=Newton.newtonCreateCylinder(world,size[0],size[1],size[2],finalXform.to_a.pack('f*'))
            #~ when "cone"
                #~ col=Newton.newtonCreateCone(world,size[0],size[1],size[2],finalXform.to_a.pack('f*'))
            #~ when "capsule"
                #~ col=Newton.newtonCreateCapsule(world,size[0],size[1],size[2],finalXform.to_a.pack('f*'))
            #~ when "chamfer"
                #~ col=Newton.newtonCreateChamfer(world,size[0],size[1],size[2],finalXform.to_a.pack('f*'))
        #~ end
        return col
    end
    def find_shapes(parentXform=Geom::Transformation.new)
        return [] if(ignore)
                
        xform=parentXform*transformation
        sa=[]
        if(shape!=nil)
            sa.push([shape,xform])
            #puts("Collision #{shape} at #{xform.to_a.inspect}")
        else
            entities.each{|ent| 
                if(ent.class==Sketchup::Group||ent.class== Sketchup::ComponentInstance)
                    sa.push(ent.find_shapes(xform))
                end
            }
        end
        if(sa.length==0)
            sa.push(["default",xform])
            #puts("Default collision at #{xform.to_a.inspect}")
        end
        sa.flatten!
        return sa;
    end
    
    def shapes
        return [] if(ignore)
        
        sa=[]
        if(shape!=nil)
            sa.push(self)
        else
            entities.each{|ent| 
                if(ent.class==Sketchup::Group||ent.class== Sketchup::ComponentInstance)
                    sa.push(ent.shapes)
                end
            }
        end
        if(sa.length==0)
            sa.push(self)
        end
        sa.flatten!
        return sa;
    end
    def isBody?
        parent.class==Sketchup::Model
    end
    def isJoint?
        get_attribute("SPJOINT","name",nil)!=nil
    end
    def connections
        get_attribute("SPOBJ","parentJoints",[])
    end  
    def connect(joint)
        name=joint.get_attribute("SPJOINT","name",nil)
        all=get_attribute("SPOBJ","parentJoints",[])
        all.push(name)
        set_attribute("SPOBJ","parentJoints",all)
    end 
end
end #module MSketchyPhysics3
