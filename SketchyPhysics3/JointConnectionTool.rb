require 'sketchup.rb'
module MSketchyPhysics3
#joints
#piston,motor
#icon
#Lin,rot DOF.



def embedIndexInValue(value,int)
    #take value (float) convert to a int that is the binary equiv. 
    #Mask out the bottom 4 bits the OR the index into those bits.
    return [(([value].pack('f').unpack('l')[0])   &0xfffffff0 |int )].pack('l').unpack('f')[0]
end

def extractEmbededIndex(value)
    return (([value].pack('f').unpack('l')[0])   &0xf  ),
            [(([value].pack('f').unpack('l')[0])   &0xfffffff0  )].pack('l').unpack('f')
end


#idea. extend instance to include physics info.
#group
#    physics
#        type (obj or joint)
#        id
#        state
#        parents
#        getchilderen if obj the objs in group. if joint then objs connected to this group
class JointConnectionTool

        def updateDialog
                           
        end
	    def onSetCursor()
          if (PLATFORM=="i386-mswin32")
            @ctrlDown=MSketchyPhysics3::getKeyState(VK_LCONTROL)
          else
            @ctrlDown=MSketchyPhysics3::getKeyState(VK_LOPTION)
          end
            @shiftDown=MSketchyPhysics3::getKeyState(VK_LSHIFT)
            return UI.set_cursor(634) if(@ctrlDown==true)
            return UI.set_cursor(636) if(@shiftDown==true)
        end
  
        
        def doSelectObject(ent)
            if(ent==nil || @selectedObject==ent) 
                return #no change.
            end
    
        #    puts ent.name+": Attached to:"+ent.get_attribute("SPOBJ","parentname","nothing") +ent.get_attribute("SPOBJ","parentid","") 
            
            #~ if(@ctrlDown && @shiftDown && @selectedObject!=nil && ent!=nil)
                #~ JointConnectionTool.attach(ent,@selectedObject)
                #~ ent=@selectedObject
            #~ end                
            
            if(@ctrlDown && !@shiftDown && @selectedObject!=nil && ent!=nil)
                JointConnectionTool.connectJoint(@selectedObject,ent)
                ent=@selectedObject
            end                
            if(@shiftDown && !@ctrlDown && @selectedObject!=nil && ent!=nil)
                disconnectJoint(@selectedObject,ent)
                ent=@selectedObject
            end                

                
            @selectedObject=ent
            Sketchup.active_model.selection.clear()
            if(@selectedObject==nil)
                @selectedIsJoint=false
                @relatedObjects=[]
            else
                
                if(ent.get_attribute("SPJOINT","name",nil)==nil)
                    @selectedIsJoint=false
                    
                else
                    @selectedIsJoint=true
                end

                if(@selectedIsJoint)
                    @relatedObjects=findJointChildren(@selectedObject.get_attribute("SPJOINT","name",nil))
                    
###
##
Sketchup.active_model.selection.add(@selectedObject)
##
###
                else
                    @relatedObjects=findObjectsJoints(@selectedObject)
                    #add to selection to add clairty (if it isnt a joint due to visual bug)           
                    Sketchup.active_model.selection.add(@selectedObject)
                end

                #puts @relatedObjects
            #Sketchup.active_model.selection.add(@relatedObjects)
                
                @selectedBounds=calcBounds(@selectedObject,@selectedParent)
                @relatedBounds=[]
                @relatedObjects.each{|ro|
                    parent=nil
                    if(ro.parent.class!=Sketchup::Model)
                        parent=ro.parent.instances[0]
                    end
                    @relatedBounds.push(calcBounds(ro,parent))
                    }
            
            end
            updateDialog()
        end

        def activate
            #   puts "activate called"
            Sketchup::set_status_text("Click to select. Hold CTRL and click to connect. Hold SHIFT and click to disconnect.")

            @allJoints=Hash.new
            @allChildren=[]
            @dragging=false
            
            @hoverJoint=nil
            @hoverJointGroup=nil
            @hoverGroup=nil
            @selectedObject=nil
            @selectedIsJoint=false
            
            @dropPoint=nil
            @dropGroup=nil
            
            @relatedObjects=[]
            @relatedBounds=[]
            
            @ctrlDown=false
            @shiftDown=false

            Sketchup.active_model.selection.clear()
            
SketchyPhysics.checkVersion

#replace with call to inspectModel.
#return;
     Sketchup.active_model.start_operation "Fix joints"
            Sketchup.active_model.definitions.each{ |cd|
                cd.instances.each{|ci|
                    #~ if(ci.get_attribute("SPOBJ","numParents",nil)!=nil)
                        #~ @allChildren.push(ci)
                    #~ end
                    
                    jnames=JointConnectionTool.getParentJointNames(ci)
                    if(jnames.length>0)
                        @allChildren.push(ci)
                    end
                    
                    if(ci.get_attribute("SPJOINT","name",nil)!=nil)
                        jname=ci.get_attribute("SPJOINT","name",nil)
                        #confirm joint is unique
                        if(false || ci.parent.class!=Sketchup::Model && ci.parent.instances.length>1)
                            puts "warning copied joint "+ci.parent.instances.length.to_s
                            newi=ci.parent.instances[1]
                            newi.make_unique
                            puts "Duplicated joint. Renaming."
                            #rename joint.
                            newname=ci.get_attribute("SPJOINT","type",nil)+newi.entityID.to_s+rand(1000).to_s
                            newi.set_attribute("SPJOINT","name",newname)
                            newi.name=newname
                            #~ while(ci.parent.instances.length>1){|i|
                                #~ ni=ci.parent.instances[i].make_unique
                                #~ ni.set_attribute("SPOBJ","numParents",0)#disconnect from any joints.
                                #~ puts "disconnect"
                                #~ }
                            #oi=ci.parent.instances[1]
                            #jname=oi.get_attribute("SPJOINT","type",nil)+oi.entityID.to_s
                            #oi.parent.instances[1].set_attribute("SPJOINT","name",jname)
                        end
                        if(@allJoints[jname]!=nil)
                            #puts "Duplicated joint. Renaming."
                            #rename joint.
                            #jname=ci.get_attribute("SPJOINT","type",nil)+ci.entityID.to_s
                            #ci.set_attribute("SPJOINT","name",jname)
                            #ci.name=jname
                        end
                        @allJoints[jname]=ci

                    end
                }
            }
     Sketchup.active_model.commit_operation
        end

        def deactivate(view)        
            #puts "deactivate called"
            doSelectObject(nil)
            Sketchup::set_status_text("")
        end

        def draw(view)
            if(@dragging)
                view.line_width=3
                if(@dropGroup!=nil)
                    view.drawing_color="green"
                else
                    view.drawing_color="yellow"
                end
                if(@inputPoint!=nil && @dropPoint!=nil)
                    view.draw_line(@inputPoint.position,@dropPoint.position)
                end
            end
            
            if(@selectedIsJoint)
                view.drawing_color="yellow"
            else
                view.drawing_color="green"
            end
            
            if(@selectedObject!=nil)
                    view.line_width=3
                    view.draw(GL_LINE_STRIP,@selectedBounds)
            end

            if(!@selectedIsJoint)
                view.drawing_color="yellow"
            else
                view.drawing_color="green"
            end
            @relatedBounds.each{|rb|
                view.draw(GL_LINE_STRIP,rb)
                }
                
        #~ normal= @selectedObject.transformation.origin.vector_to([0,0,1].transform!(@selectedObject.transformation))       
		#~ pts=sp_points_on_circle([0,0,30].transform!(@selectedObject.transformation),normal,10, 20,0)
		#~ view.draw(GL_LINE_STRIP, pts)
		#~ pts=sp_points_on_circle([0,0,-30].transform!(@selectedObject.transformation),normal,10, 20,0)
		#~ view.draw(GL_LINE_STRIP, pts)
		#~ pts=sp_points_on_circle(@selectedObject.transformation.origin,normal,10, 20,0)
		#~ view.draw(GL_LINE_STRIP, pts)
        #~    view.draw_line([0,0,-30].transform!(@selectedObject.transformation),[0,0,30].transform!(@selectedObject.transformation))    
            #box=[-0.5,0.5,0.5,
            #boundingbox.corner
            #if(@selectedObject!=nil && !@relatedObjects.empty?)
            #    @relatedObjects.each{|o|
            #        view.draw_line(@selectedObject.transformation.origin,o.transformation.origin)
            #    }
            #end
                #view.draw(GL_LINE_STRIP, pts)
        end
        
        #my func to find the bounding box of a object so I can display it instead of SU.
        def calcBounds(grp,parent)

            xform=grp.transformation
            if(parent!=nil)
                #parent=findParentInstance(grp)
                #puts parent
                xform=parent.transformation*xform
            end
            
            return [  
                grp.definition.bounds.corner(0).transform!(xform),
                grp.definition.bounds.corner(1).transform!(xform),
                grp.definition.bounds.corner(3).transform!(xform),
                grp.definition.bounds.corner(2).transform!(xform),
                grp.definition.bounds.corner(0).transform!(xform),
            
                grp.definition.bounds.corner(4).transform!(xform),
                grp.definition.bounds.corner(5).transform!(xform),
                grp.definition.bounds.corner(7).transform!(xform),
                grp.definition.bounds.corner(6).transform!(xform),
                grp.definition.bounds.corner(4).transform!(xform),

                grp.definition.bounds.corner(6).transform!(xform),
                grp.definition.bounds.corner(2).transform!(xform),
                grp.definition.bounds.corner(3).transform!(xform),
                grp.definition.bounds.corner(7).transform!(xform),
                grp.definition.bounds.corner(5).transform!(xform),
                grp.definition.bounds.corner(1).transform!(xform),
            ]

            #@myBounds.transform!(grp.transformation)            
        end                              

        def getExtents       
            bb = Geom::BoundingBox.new
            if(@inputPoint!=nil)
                bb.add @inputPoint.position
                bb.add @inputPoint.position
            end
        end

        # This is called followed directly by onRButtonDown
        def getMenu(menu)
        end

        def onCancel(reason, menu)                
            puts "onCancel called"
        end

        def onKeyDown(key, rpt, flags, view)
            if( key == COPY_MODIFIER_KEY && rpt == 1 )
		        @ctrlDown=true
            end
            if( key == CONSTRAIN_MODIFIER_KEY && rpt == 1 )
		        @shiftDown=true
            end
        end

        def onKeyUp(key, rpt, flags, view)
            if( key == COPY_MODIFIER_KEY)
		        @ctrlDown=false
	        end
            if( key == CONSTRAIN_MODIFIER_KEY)
		        @shiftDown=false
	        end
        end
        
        def pickEmbeddedJoint(x,y,view)
            ph=view.pick_helper
            num=ph.do_pick x,y
            
            item=nil
            path=ph.path_at(1)
            
            return item if (path==nil)

            path.length.downto(0){|i|
                if(path[i].class==Sketchup::Group && 
                        (path[i].parent==Sketchup.active_model || (path[i].get_attribute("SPJOINT","name",nil)!=nil)))
                    item=path[i]
                    #puts "ParentGroup="+item.to_s
                    break
                end
            }
            return item;
        end
        
        def findJointChildren(jname)
            kids=[]
            Sketchup.active_model.definitions.each{ |cd|
                cd.instances.each{|ci|
                
                    jnames=JointConnectionTool.getParentJointNames(ci)
                    if(jnames.index(jname)!=nil)
                        kids.push(ci)
                    end
                    
                    #~ if(ci.get_attribute("SPOBJ","numParents",nil)!=nil)
                        #~ dict=ci.attribute_dictionaries["SPOBJ"]
                        #~ dict.each_pair { | key, value | 
                            #~ if(key.include?("jointParent") && value==jname)
                                #~ kids.push(ci)
                            #~ end
                        #~ }
                    #~ end
                }
            }
            return kids
        end
        
        def findObjectsJoints(ent)
            joints=[]
            jnames=JointConnectionTool.getParentJointNames(ent)
            jnames.each{|jname|
                jnt=@allJoints[jname]
                if(jnt==nil)
                   puts "Missing joint "+jname.to_s 
                   JointConnectionTool.disconnectJointNamed(ent,jname)
                else
                    joints.push(jnt)#lookup joint
                end
            }
             return joints           
            
            #~ if(ent.get_attribute("SPOBJ","numParents",nil)!=nil)
                #~ dict=ent.attribute_dictionaries["SPOBJ"]
                #~ dict.each_pair { | key, value | 
                    #~ if(key.include?("jointParent") && value!=nil)
                        #~ jnt=@allJoints[value]
                        #~ if(jnt==nil)
                           #~ puts "Missing joint "+value.to_s 
                           #~ disconnectJointNamed(ent,key)
                        #~ else
                            #~ joints.push(jnt)#lookup joint
                        #~ end
                    #~ end
                #~ }
            #~ end
            #~ return joints
        end
        
        # NOTE: Called after onLButtonDown and onLButtonUp. 
        def onLButtonDoubleClick(flags, x, y, view)
            
            if(!@ctrlDown)
                Sketchup.active_model.selection.add(@relatedObjects)
            end
            
            if((@ctrlDown || @shiftDown)&& @relatedObjects.length>0)
                @ctrlDown=false #debounce key
                @shiftDown=false #debounce key
                if(@selectedIsJoint)
                   	keypress = UI.messagebox "Are you sure you want to disconnect these "+@relatedObjects.length.to_s() +" objects?" , MB_YESNO, "Error"
                    if (keypress == 6)
                        puts("joint disconnect "+@relatedObjects.length.to_s() +" from "+ @selectedObject.name)
                        @relatedObjects.each{|e|
                            disconnectJoint(@selectedObject,e)
                        }
                    end
                else
                   	keypress = UI.messagebox "Are you sure you want to disconnect from these "+@relatedObjects.length.to_s() +" joints?" , MB_YESNO, "Error"
                    if (keypress == 6)
                        puts("disconnect "+ @selectedObject.name.to_s()  +" from "+ @relatedObjects.length.to_s())
                        @relatedObjects.each{|e|
                            disconnectJoint(@selectedObject,e)
                        }
                    end
                end
            end
            view.invalidate()
        end

        def onLButtonDown(flags, x, y, view)
            @dragCount=0;
            @lButtonDown=true;
            
            @inputPoint = Sketchup::InputPoint.new
		    @inputPoint.pick view, x, y
            
            ph=view.pick_helper
	        num=ph.do_pick x,y
            ent=ph.best_picked
        
            if(ent.class!=Sketchup::Group && ent.class!=Sketchup::ComponentInstance)
                doSelectObject(nil)
                return
            end
        
            @selectedParent=nil
            
            joint=pickEmbeddedJoint(x,y,view)
            if(joint!=nil && joint.get_attribute("SPJOINT","name",nil))
                @selectedParent=ent
                ent=joint
            end
                
            doSelectObject(ent)
            
            view.invalidate()
        end
        
        def self.convertConnections(group)
            ja=group.get_attribute("SPOBJ","parentJoints",[])
            
            dict=group.attribute_dictionaries["SPOBJ"]
            keys=[]
            dict.each_pair { | key, value | 
                if(key.include?("jointParent")&& value!=nil)
                    ja.push(value)
                    keys.push(key)
                    #group.delete_attribute("SPOBJ",key)
                end
                }
            group.delete_attribute("SPOBJ","numParents") 
            keys.each{|k| group.delete_attribute("SPOBJ",k)}
            if(ja.length>0)
                group.set_attribute("SPOBJ","parentJoints",ja)
            end 
            puts "Converted #{ja.length} connections for #{group}."             
        end
        def self.getParentJointNames(group)
            if(group.get_attribute("SPOBJ","numParents",nil)!=nil)
                convertConnections(group)
            end
            return group.get_attribute("SPOBJ","parentJoints",[])
        end
        def self.disconnectJointNamed(ent,name)
            ja=ent.get_attribute("SPOBJ","parentJoints",[])
            c=ja.length
            ja.delete_if {|jn| jn == name }
            if(c!=ja.length)
                puts "Disconnected #{name} (#{c}/#{ja.length}}"
                ent.set_attribute("SPOBJ","parentJoints",ja)
            end
        end

        def self.disconnectAllJoints(group)
            group.delete_attribute("SPOBJ","parentJoints")
            puts "Disconnected all joints from {#group}"
        end
        
        def self.old_disconnectJointNamed(ent,name)
            if(ent.get_attribute("SPOBJ",name,nil)!=nil)
                ent.delete_attribute("SPOBJ",name) #disconnect joint
                c=ent.get_attribute("SPOBJ","numParents",0)
                ent.set_attribute("SPOBJ","numParents",c-1) #decriment parent count
                
                puts "disconnected "+name
            end
        end

        def disconnectJoint(joint,child)
            
            #verify child not already connected to joint.
            #verify joint isnt inside child
            
            

            
            if(child.get_attribute("SPJOINT","name",nil)!=nil)#check for swap of joint/child
                temp=joint
                joint=child
                child=temp
            end

            Sketchup.active_model.start_operation "Disconnect from joint "+joint.name
            
            jname=joint.get_attribute("SPJOINT","name",nil)
            cname=child.get_attribute("SPJOINT","name",nil)
            puts "Disconnect "+cname.to_s+" from "+jname.to_s
            JointConnectionTool.disconnectJointNamed(joint,cname)
            JointConnectionTool.disconnectJointNamed(child,jname)
            
            if(jname!=nil&&cname!=nil)
                #~ ga=joint.get_attribute("SPJOINT","gearjoint",nil)
                #~ gb=child.get_attribute("SPJOINT","gearjoint",nil)
                #~ if(ga!=nil && ga==child.get_attribute("SPJOINT","name",nil))
                    #~ joint.delete_attribute("SPJOINT","name")
                    #~ joint.delete_attribute("SPJOINT","geartype")
                    #~ joint.delete_attribute("SPJOINT","ratio")
                    #~ puts("Disconnected Gear")
                    
                #~ end
                #~ if(gb!=nil && gb==child.get_attribute("SPJOINT","name",nil))
                    #~ child.delete_attribute("SPJOINT","name")
                    #~ child.delete_attribute("SPJOINT","geartype")
                    #~ child.delete_attribute("SPJOINT","ratio")
                    #~ puts("Disconnected gear")
                    
                #~ end                
                
                puts "Disconnected Gear "+child.name+" from "+joint.name
            end
           
            Sketchup.active_model.commit_operation
        end
        
        def self.attach(parent,child)
            childID=child.getUniqueID()
            parentID=parent.getUniqueID()
            puts "Attach #{child.name}(#{childID}) to #{parent.name}(#{parentID})"+parent.name
            child.set_attribute("SPOBJ","parentname",parent.name)
            child.set_attribute("SPOBJ","parentid",parentID)
            child.set_attribute("SPOBJ","jointtype","hinge")
            
            
        end
        def self.groupIsJoint(group)
            return (group.get_attribute("SPJOINT","name",nil)!=nil)
        end
        def self.connectJoint(joint,child)
           
            if(groupIsJoint(joint) && groupIsJoint(child))#check connect two objects.

#~ #2 joints are geared
#Rule(?) geared joints means joint parent only.

#~ #if joint is in body that is body
#~ #else each joint childeren

#each body could have a parent or a child joint or both

#2 bodies are geared.
#find joint pins.
#if body has single child joint that is the joint
#elsif body has single parent joint that is the joint
#if body has multiple child joints?
#if body has multiple parent joints?




                #TODO: if either is gear and one is joint
                #store joint name in gear
                puts "Connect joint to joint. Make gear?"
                puts joint.parent
                puts child.parent
                    
                atype=joint.get_attribute("SPJOINT","type",nil)
                btype=child.get_attribute("SPJOINT","type",nil)
                if(atype=="hinge" || atype=="servo" || atype=="motor")
                    if(btype=="hinge" || btype=="servo" || btype=="motor")
                        gtype="gear"
                    elsif(btype=="slider" || btype=="piston")
                        gtype="wormgear"
                    end
                elsif(atype=="slider" || atype=="piston")
                    if(btype=="slider" || btype=="pulley")
                        gtype="pulley"
                    elsif(atype=="hinge" || atype=="servo" || atype=="motor")
                        gtype="wormgear"
                        puts "Need swap!!"
                    end
                end
                if(UI.messagebox("Create gear "+gtype+" between "+atype+" and "+btype+"?",MB_OKCANCEL,"Create Gear")==1)
                    joint.set_attribute("SPJOINT","gearjoint",child.get_attribute("SPJOINT","name",nil))
                    joint.set_attribute("SPJOINT","geartype",gtype)
                    joint.set_attribute("SPJOINT","ratio",1.0)
                    joint.set_attribute("SPJOINT", "GearConnectedCollide", false)
                end

            end
            if(!groupIsJoint(joint) && !groupIsJoint(child))#check connect two objects.
                puts "Connect Object "+joint.name+" to "+child.name
                #return
            end
        
            if(child.get_attribute("SPJOINT","name",nil)!=nil)#check for swap of joint/child
                temp=joint
                joint=child
                child=temp
            end
          
            jointParentName=joint.get_attribute("SPJOINT","name",nil)  
                  
            jnames=JointConnectionTool.getParentJointNames(child)
            
            #verify child not already connected to joint.
            if(jnames.index(jointParentName)!=nil)
                puts(child.name+" already connected to "+jointParentName.to_s)
                return;  
            end
            
                #verify joint isnt inside child
            child.entities.each { | ent | 
                if(ent==joint)
                    puts "cant connect to internal joints"
                    return;  
                end
            }

            puts "Connect "+joint.name+" to "+child.name
            Sketchup.active_model.start_operation "Connect to joint "+joint.name
            jnames.push(jointParentName)
            child.set_attribute("SPOBJ","parentJoints",jnames)
            Sketchup.active_model.commit_operation
                  
        end

        def onLButtonUp(flags, x, y, view)
            @lButtonDown=false
            if(@dragging)
                @dragging=false
                if(@dropGroup!=nil && @selectedObject!=nil)
                    if(@ctrlDown)
                        disconnectJoint(@selectedObject,@dropGroup)
                    else
                        JointConnectionTool.connectJoint(@selectedObject,@dropGroup)
                    end
                    doSelectObject(nil)
                end
            end
            view.invalidate()
        end    
        def onMouseMove(flags, x, y, view) 

            if(false) #if(@lButtonDown && @selectedObject!=nil)
                @dragCount=@dragCount+1
                if(@dragCount>10)
                    @dragging=true
                end
            else
                @dragging=false;
            end
            
            ph=view.pick_helper
            ph.do_pick x,y
            ent=ph.best_picked
            if(ent!=nil && (ent.class==Sketchup::Group || ent.class==Sketchup::ComponentInstance))
                @hoverGroup=ent
            else
                @hoverGroup=nil
            end
            
            joint=pickEmbeddedJoint(x,y,view)
            if(joint!=nil && joint.get_attribute("SPJOINT","name",nil) )
                if(@hoverJoint!=joint)
                    @hoverJoint=joint
                    @hoverJointGroup=@hoverGroup
                end
            else
                @hoverJoint=nil
                @hoverJointGroup=nil
            end

            if(@dragging && @selectedObject!=nil)
                ip = Sketchup::InputPoint.new
		        ip.pick(view, x, y)
                @dropPoint=ip
                dc=nil
                if(@selectedIsJoint)
                    dc=@hoverGroup
                else
                    dc=@hoverJoint
                end
                if(validDropTarget(dc) )
                    #puts dc
                    @dropGroup=dc
                else
                    @dropGroup=nil
                end
                Sketchup.active_model.selection.clear()
                Sketchup.active_model.selection.add(@selectedObject)
                if(@dropGroup!=nil)
                    Sketchup.active_model.selection.add(@dropGroup)
                end
                view.invalidate
            else
                @dropPoint=nil;
            end   
        end

        def validDropTarget(ent)
            
            if(@ctrlDown)
                if(@relatedObjects.index(ent)!=nil)
                    return true
                else
                    return false
                end
            end
            
            if(@relatedObjects.index(ent)!=nil)
                return false
            end
            
            if(@selectedIsJoint)
                
                
                #if ent is object 
                #and ent isnt a joint
                #and not already connected 
                #and not joint's group
            else
            end            
            
            return true
        end
        #def onMouseEnter(view)
        #end

        #def onMouseLeave(view)
        #end



        #~ # NOTE: Called after onrButtonDown and onRButtonUp.
        #~ def onRButtonDoubleClick(flags, x, y, view)
                #~ puts "onRButtonDoubleClick called"

        #~ end
        #~ def onMButtonDoubleClick(flags, x, y, view)
                #~ puts "onMButtonDoubleClick called"

        #~ end
        #~ def onRButtonDown(flags, x, y, view)
                #~ #puts "onRButtonDown called"

        #~ end

        #~ def onMButtonDown(flags, x, y, view)
                #~ #puts "onMButtonDown called"

        #~ end
        #~ def onMButtonUp(flags, x, y, view)
                #~ #puts "onMButtonUp called"

        
        #~ end

        #~ # Called not only right after a onRButtonDown, but after a onRButtonDoubleClick?
        #~ def onRButtonUp(flags, x, y, view)
                #~ puts "onRButtonUp called"

        #~ end

        # NOTE: onReturn is followed directly by onKeyDown
        def onReturn(view)
                puts "onReturn called"
        end
                                               
        def onUserText(text, view)
                puts "onUserText called"

        end

        # Called when I double-click middle mouse button (exits out of orbit)
        def resume(view)
                
                #puts "resume called"
            @ctrlDown=false 
            @shiftDown=false
        end

        # Called when I press middle mouse button (goes into orbit)
        def suspend(view)
                
                #puts "suspend called"

        end

end # end of class JointConnectionTool

end #module MSketchyPhysics3

