require 'sketchup.rb'

module MSketchyPhysics3

def self.setDefaultPhysicsSettings()
    Sketchup.active_model.set_attribute( "SPSETTINGS", "defaultobjectdensity", "0.2")
    Sketchup.active_model.set_attribute( "SPSETTINGS", "worldscale", "9.0")
    Sketchup.active_model.set_attribute( "SPSETTINGS", "gravity", "1.0")
    Sketchup.active_model.set_attribute( "SPSETTINGS", "framerate", 3)
    #Sketchup.active_model.set_attribute( "SPSETTINGS", "water", 1)
end



class ControllerContext
    #security. 
    class Kernel
    end  
    class Dir
    end  
    class File
    end  
    class IO
    end  
    class Thread
    end  
    class Process
    end 
end


class SketchyPhysicsClient
  
    def extractScaleFromGroup(group)
        Geom::Transformation.scaling( 
            (Geom::Vector3d.new(1.0,0,0).transform!(group.transformation)).length, 
            (Geom::Vector3d.new(0,1.0,0).transform!(group.transformation)).length, 
            (Geom::Vector3d.new(0,0,1.0).transform!(group.transformation)).length
            )
    end #extractScaleFromGroup(group)

    def findParentInstance(ci)
        ci.definition.instances.each{|di|
            return(di) if(ci==di)
        }
        return nil;
    end #findParentInstance(ci)
    def resetAxis(ent)
        SketchyPhysicsClient::resetAxis(ent)    
    end
     def self.resetAxis(ent)
        if(ent.class==Sketchup::ComponentDefinition)
            cd=ent
        else
            cd=ent.definition
        end
        realBounds= Geom::BoundingBox.new
        
        #Calculate the real bounding box of the entities in the component.
        cd.entities.each{|de| realBounds.add(de.bounds)}

        #desired center.
        center=Geom::Point3d.new(0,0,0)
        
        #if not already centered
        if(realBounds.center!=center)
          
            #transform all the entities to be around the new center
            cd.entities.transform_entities(Geom::Transformation.new( center-realBounds.center), cd.entities.to_a)
            
            #move each instance of this component to account for the entities moving inside the component.
            cd.instances.each{|ci|
                newCenter=realBounds.center.transform(ci.transformation)
                ci.transform!(newCenter-ci.transformation.origin)
            }
        end
    end #resetAxis(ent)

    #~ def copyAndKick(grp,pos=nil,kick=nil)
        #~ if(kick.class!=Array)
            #~ kick=[0,0,kick.to_f]
        #~ end

        #~ grp=Sketchup.active_model.add_instance(ogrp.definition,ogrp.transformation)
        
        #~ @tempObjects.push(grp)
        #~ grp.material=ogrp.material
        #~ grp.name="__copy"
        #~ grp.set_attribute( "SPOBJ", "shape",ogrp.get_attribute( "SPOBJ", "shape", nil))
        #~ collisions=dumpGroupCollision(grp,0,extractScaleFromGroup(grp)*(grp.transformation.inverse))
        
        #~ if(!collisions.empty?)
            #~ if(pos!=nil)
                #~ xform=Geom::Transformation.new(pos) 
            #~ else
                #~ xform=nil
            #~ end
            #~ body=createBodyFromCollision(grp,collisions,xform);

            #~ NewtonServer.addImpulse(body,kick.to_a.pack("f*"),grp.transformation.origin.to_a.pack("f*"))
        #~ end
        
        
            #~ if(grp.valid?)
                #~ newgrp=emitGroup(grp,value)
                #~ if(newgrp!=nil) #sucessfully copied?
                    #~ rate=grp.get_attribute("SPOBJ","emitterrate",10)
                    #~ grp.set_attribute("SPOBJ","lastemissionframe",@@frame)
                    #~ lifetime=grp.get_attribute("SPOBJ","lifetime",0).to_f
                    #~ if(lifetime!=nil && lifetime>0)
                        #~ #puts "dying obj"+lifetime.to_s
                        #~ #newgrp.set_attribute("SPOBJ","diesatframe",nil)
                        #~ @dyingObjects.push([newgrp,@@frame+lifetime])
                    #~ end
                    
                #~ end
            #~ end
        
    #~ end 
    
    def destroyTempObject(grp)
        #toi=@tempObjects.delete(grp)
        body=DL::PtrData.new(grp.get_attribute("SPOBJ","body",nil))
        if(body!=nil)
            NewtonServer.destroyBody(body)
        end
        #grp.hide!    
    
    end

    def emitGroup(grp,xform=nil,strength=15,lifetime=nil)
        if(strength.class==Array)
            kick=[0,0,0].vector_to(strength)
            return nil if(kick.length==0.0&&xform==nil)
        else
            return nil if(strength.to_f==0.0)
            v=grp.transformation.zaxis
            v.length=strength.to_f
            kick=v
        end
xform=grp.transformation if(xform==nil)        
newgrp=Sketchup.active_model.entities.add_instance(grp.definition,xform)
#        newgrp=Sketchup.active_model.entities.add_instance(grp.definition,grp.transformation)
        
        @tempObjects.push(newgrp)
        newgrp.material=grp.material
        newgrp.set_attribute( "SPOBJ", "shape",grp.get_attribute( "SPOBJ", "shape", nil))
        collisions=dumpGroupCollision(newgrp,0,extractScaleFromGroup(newgrp)*(newgrp.transformation.inverse))
        
        if(!collisions.empty?)
            body=createBodyFromCollision(newgrp,collisions);
            #kick=[0,0,15.0]
            NewtonServer.addImpulse(body,kick.to_a.pack("f*"),newgrp.transformation.origin.to_a.pack("f*"))
        end
        
        if(newgrp!=nil) #sucessfully copied?
            rate=grp.get_attribute("SPOBJ","emitterrate",10)
            grp.set_attribute("SPOBJ","lastemissionframe",@@frame)
            
            #get from object if not passed in.
            lifetime=grp.get_attribute("SPOBJ","lifetime",0).to_f if(lifetime==nil)
            
            if(lifetime!=nil && lifetime>0)
                setLifetime(newgrp,lifetime)
            #puts "dying obj"+lifetime.to_s
                #newgrp.set_attribute("SPOBJ","diesatframe",nil)
                #@dyingObjects.push([newgrp,@@frame+lifetime])
            end
            
        end
        
        return newgrp
    end 

    def setLifetime(grp,lifetime)
        @dyingObjects.push([grp,@@frame+lifetime])        
    end
    def copyBody(grp,xform=nil,lifetime=0)
        body=DL::PtrData.new(grp.get_attribute("SPOBJ","body",nil))
        return if(body==nil)
#        grp=body.copy
xform=grp.transformation if(xform==nil)        
newgrp=Sketchup.active_model.entities.add_instance(grp.definition,xform)
        @tempObjects.push(newgrp)
        newgrp.material=grp.material
        newgrp.set_attribute( "SPOBJ", "shape",grp.get_attribute( "SPOBJ", "shape", nil))
        collisions=dumpGroupCollision(newgrp,0,extractScaleFromGroup(newgrp)*(newgrp.transformation.inverse))
        
        if(!collisions.empty?)
            newbody=createBodyFromCollision(newgrp,collisions);
            if(lifetime!=nil && lifetime.is_a?(Numeric) && lifetime>0)
                setLifetime(newgrp,lifetime)    
            end
            #kick=[0,0,15.0]
            #NewtonServer.addImpulse(body,kick.to_a.pack("f*"),grp.transformation.origin.to_a.pack("f*"))
        end
        return newgrp
    end #copyBody(body)

    def pushBody(grp,strength)
        body=DL::PtrData.new(grp.get_attribute("SPOBJ","body",nil))
        return if(body==nil)
        
        if(strength.class==Array)
            kick=[0,0,0].vector_to(strength)
            return nil if(kick.length==0.0)
        else
            return nil if(strength.to_f==0.0)
            v=grp.transformation.zaxis
            v.length=strength.to_f
            kick=v
        end            
        NewtonServer.addImpulse(body,kick.to_a.pack("f*"),grp.transformation.origin.to_a.pack("f*"))
        
    end

    def dumpCollision
        Sketchup.active_model.entities.each { | ent | 
            if (ent.class== Sketchup::Group ||ent.class== Sketchup::ComponentInstance)
                if(ent.attribute_dictionary("SPWATERPLANE")!=nil)
                    density=ent.get_attribute("SPWATERPLANE","density",1.0)
                    linearViscosity=ent.get_attribute("SPWATERPLANE","linearViscosity",1.0)
                    angularViscosity=ent.get_attribute("SPWATERPLANE","angularViscosity",1.0)
                    current=ent.get_attribute("SPWATERPLANE","current",[0,0,0])
                    xform=ent.transformation
                    plane=Geom.fit_plane_to_points(xform.origin,xform.origin+xform.xaxis,xform.origin+xform.yaxis)
                    NewtonServer.setupBouyancy(plane.pack("f*"),
                            [0.0+current[0],0.0+current[1],-9.8+current[2]].pack("f*"),
                            [density,linearViscosity,angularViscosity].pack("f*"))
                end
                if(!ent.get_attribute("SPOBJ", "ignore", false))
                    resetAxis(ent)
                    collisions=dumpGroupCollision(ent,0,extractScaleFromGroup(ent)*(ent.transformation.inverse))
                    createBodyFromCollision(ent,collisions) if(!collisions.empty?)
                end
            end
        }
    end #dumpCollision

    def createBodyFromCollision(group,collisions,newXform=nil)
        id=@DynamicObjectList.length;
        
        if(!group.get_attribute( "SPOBJ", "static", false))
            bDynamic=1
        else
            bDynamic=0
        end
        
        collisions.flatten!
        xform=group.transformation
#NewCode: overide if passed in xform.
xform=newXform if(newXform!=nil)
        
        #figure out the scaling of the xform.
        scale=[ (Geom::Vector3d.new(1.0,0,0).transform!(xform)).length,
                (Geom::Vector3d.new(0,1.0,0).transform!(xform)).length,
                (Geom::Vector3d.new(0,0,1.0).transform!(xform)).length]
                
        #find the real size of the bounding box and offset to center.
        bb=group.definition.bounds
        size=[bb.width*scale[0],bb.height*scale[1],bb.depth*scale[2]]
        tt=Geom::Transformation.new()
        
        
        
        body=NewtonServer.createBody(
            id,collisions.pack('L*'),
            collisions.length,bDynamic, size.pack('f*'),
            xform.to_a.pack('f*')
            )
            
        #save body in obj for later reference.
        group.set_attribute("SPOBJ", "body",body.to_i)
#puts ["saved scale",scale]
        group.set_attribute("SPOBJ", "savedScale", extractScaleFromGroup(group).to_a)
        
        if(group.get_attribute( "SPOBJ", "showgeometry", false))
        	ShowCollision(group,body)
        	#group.set_attribute( "SPOBJ", "showgeometry",false)
        end
        
        UpdateEmbeddedGeometry(group,body)
        jnames=JointConnectionTool.getParentJointNames(group)
        
        @AllJointChildren.push(group) if(jnames.length>0)
        
        #set freeze if needed	
        if(group.get_attribute( "SPOBJ", "frozen", false))
            NewtonServer.bodySetFreeze(body,1)
        else
            NewtonServer.bodySetFreeze(body,0)
        end
        if(group.get_attribute( "SPOBJ", "noautofreeze", false))
#Hack! to force unfreeze of body
NewtonServer.setBodyMagnetic(body,1)
NewtonServer.setBodyMagnetic(body,0)
#hack!
        end
        if(group.get_attribute( "SPOBJ", "magnet", false) &&
            group.get_attribute( "SPJOINT", "type", nil)==nil)
            #strength=group.get_attribute("SPOBJ","strength",0.0)
            #puts strength
            force=NewtonServer.addForce(body,0.0)
            #puts force
            group.set_attribute( "SPOBJ", "__force",force.to_i)
            @allForces.push(group)
        end    
        if(group.get_attribute( "SPOBJ", "thruster", false) &&
            group.get_attribute( "SPJOINT", "type", nil)==nil)
#Hack! to force unfreeze of body
NewtonServer.setBodyMagnetic(body,1)
NewtonServer.setBodyMagnetic(body,0)
#hack!
            @allThrusters.push(group)
        end          
        if(group.get_attribute( "SPOBJ", "tickable", false) &&
            group.get_attribute( "SPJOINT", "type", nil)==nil)
            @allTickables.push(group)
            
            group.set_attribute("SPOBJ","nexttickframe",0) #ensure it ticks right away.
        end          
        if(group.get_attribute( "SPOBJ", "touchable", false))
            NewtonServer.bodySetMaterial(body,1)
            NewtonServer.setBodyCollideCallback(body,NewtonServer::COLLIDE_CALLBACK)
            group.set_attribute("SPOBJ","lasttouchframe",0)
        end          
        if(group.get_attribute( "SPOBJ", "materialid", false) &&
            group.get_attribute( "SPJOINT", "type", nil)==nil)
            matid=group.get_attribute( "SPOBJ", "materialid", 0)
            #puts ["matid",matid]
            NewtonServer.bodySetMaterial(body,matid.to_i)
        end          
        if(group.get_attribute( "SPOBJ", "emitter", false) &&
            group.get_attribute( "SPJOINT", "type", nil)==nil)
            group.set_attribute("SPOBJ","lastemissionframe",0)
            @allEmitters.push(group)
            puts "emitter"
        end    

        if(group.get_attribute( "SPOBJ", "magnetic", false))
            NewtonServer.setBodyMagnetic(body,1)
        end            
        if(group.get_attribute( "SPOBJ", "nocollison", false))
            #puts "notsolid"
            
            NewtonServer.setBodySolid(body,0)
        else
            #puts "solid"
            NewtonServer.setBodySolid(body,1)
        end
            #used by touchable etc.
        @bodyToGroup[body.to_i]=group

        boxCenter=group.definition.bounds.center.transform( extractScaleFromGroup(group))            
        NewtonServer.setBodyCenterOfMass(body,boxCenter.to_a.pack('f*'))
        
        #used in update to lookup object.
        @DynamicObjectList.push(group)
        
    #clear some temporary varibles.
    group.set_attribute("SPOBJ","__lookAtJoint",nil)
    
        #save position of all objects for reset.
        @DynamicObjectResetPositions.push(group.transformation)
        
        #save position of all objects for reset.
        @DynamicObjectBodyRef.push(body)
        return body
    end #createBodyFromCollision(group,collisions)

    def dumpGroupCollision(group, depth,parentXform)
      
        return [] if(group.get_attribute("SPOBJ", "ignore", false))

        xform=parentXform*group.transformation
        shape = group.get_attribute( "SPOBJ", "shape", nil)
        if(shape=="staticmesh" || group.get_attribute("SPOBJ", "staticmesh", false))
            puts "   "*depth+"staticmesh" if $debug
            return [createStaticMeshCollision(group)]
        end
        
        if(shape==nil || shape=="compound")
            groupCollisions=[]
            group.entities.each { | ent | 
                if (ent.class== Sketchup::Group ||ent.class== Sketchup::ComponentInstance)
                    depth=depth+1
                    groupCollisions.push( dumpGroupCollision(ent,depth,xform))
                    depth=depth-1
                end
            }
            return groupCollisions if(groupCollisions.length>1)
        end
        
        # if still nil make this it default shape.
        shape="box" if(shape==nil)

        #figure out the scaling of the xform.
        scale=[ (Geom::Vector3d.new(1.0,0,0).transform!(xform)).length,
                (Geom::Vector3d.new(0,1.0,0).transform!(xform)).length,
                (Geom::Vector3d.new(0,0,1.0).transform!(xform)).length]
        
        #find the real size of the bounding box and offset to center.
        bb=group.definition.bounds
        size=[bb.width*scale[0],bb.height*scale[1],bb.depth*scale[2]]
        center=[bb.center.x*scale[0],bb.center.y*scale[1],bb.center.z*scale[2]]

        #puts center
        center= Geom::Transformation.new(center)        
        noscale=Geom::Transformation.new(xform.xaxis,xform.yaxis,xform.zaxis,xform.origin)
        finalXform=noscale*center
        
        verts=[]
        if( shape=="convexhull")
          #fill with convex hull verts
          verts=createConvexHullVerts(group)
          finalXform=Geom::Transformation.new()
          finalXform=xform;
          return[] if(verts.length<4)
        end

        @AllCollisionEntities.push(group)
        
        #if convexhull
        col=NewtonServer.createCollision(shape,size.pack('f*'),finalXform.to_a.pack('f*'),
                verts.to_a.pack('f*')) 
            
        #for rubydl
        col=col.to_i            
        puts "   "*depth+shape+col.to_s if $debug

        return [col]
        
    end #dumpGroupCollision(group, depth,parentXform)

    def createConvexHullVerts(group)
        verts=[0]#0 is placeholder for vert count.
        group.entities.each{|ent|
            if(ent.typename == "Face")
                ent.vertices.each{|v|
                    verts=verts+v.position.to_a
                }
            end
        }
        verts[0]=(verts.length-1)/3
        return verts
    end #createConvexHullVerts(group)
    
    def createStaticMeshCollision(group)
        tris=[]
        group.entities.each{|ent|
            if(ent.typename == "Face")
                ent.mesh.polygons.each_index{|pi|
                    pts=ent.mesh.polygon_points_at( pi+1 ).each{|pt|
                        tris=tris+pt.to_a
                        }
                    }
            end
        }
        return NewtonServer.createCollisionMesh(tris.to_a.pack('f*'),tris.length/9).to_i 
    end #createStaticMeshCollision(group)

    def ShowCollision (group,body,bEmbed=false)
        if(@collisionBuffer==nil)
                #@collisionBuffer=Array.new.fill(255.chr,0..400*1024).join
                @collisionBuffer=Array.new.fill(0.0,0..400*256).pack('f*').to_ptr
        end
        dat=@collisionBuffer

        if(bEmbed)
            colGroup=group.entities.add_group	
        else        
            colGroup=Sketchup.active_model.entities.add_group	
        end
	    faceCount=NewtonServer.getBodyCollision(body,dat,400*1024)
        puts "Read back collision #{faceCount} faces."           
        cb=@collisionBuffer.to_a("F400")
        pos=0
        scale=Sketchup.active_model.get_attribute( "SPSETTINGS", "worldscale", "9.0").to_f

        while(faceCount>0)
            count=cb.shift
            points=[]
            while(count>0)
                points.push([cb.shift*scale,cb.shift*scale,cb.shift*scale])
                count-=3
            end
            #puts points.inspect
            f=colGroup.entities.add_face(points) if(points.length>0)
            f.erase! if f.valid?
            faceCount-=1
        end
        colGroup.set_attribute("SPOBJ", "ignore", true)
        colGroup.set_attribute("SPOBJ", "EmbeddedGeometryObject", true)
        colGroup.transform!(group.transformation.inverse) if(bEmbed)
        @tempObjects.push(colGroup)
        return (colGroup)

	end #ShowCollision (group,body,bEmbed=false)
	
	def UpdateEmbeddedGeometry(group,body)
		if(group.class==Sketchup::Group)
			cd=group.entities[0].parent
		else
			cd=group.definition
		end
		cd.entities.each{ |e|
			e.erase! if(e.get_attribute("SPOBJ", "EmbeddedGeometryObject", false))
		}
		if(group.get_attribute( "SPOBJ", "showcollision", false))
			ShowCollision(group,body,true)
		end	
	end #UpdateEmbeddedGeometry(group,body)
	
	def FindGroupNamed(name)
        name.downcase!
		@DynamicObjectList.each_index{|di|
		  return @DynamicObjectList[di] if(@DynamicObjectList[di].name.downcase == name)
		}
		puts "Didnt find body "+name if $debug
		return nil
	end	#FindGroupNamed(name)

	def FindBodyNamed(name)
        name.downcase!
		@DynamicObjectList.each_index{|di|
		  return @DynamicObjectBodyRef[di] if(@DynamicObjectList[di].name.downcase == name)
		}
		puts "Didnt find body "+name if $debug
		return nil
	end	#FindBodyNamed(name)
	
	def FindEntityWithID(id)
		@AllCollisionEntities.each{|ent|
		  puts ent.entityID.to_s+"=="+id.to_s if $debug
		  return ent if(ent.entityID == id)
		}
		puts "Didnt find Entity "+id.to_s if $debug
		return nil
	end #FindEntityWithID(id)
	
	def FindBodyWithID(id)
		@DynamicObjectList.each_index{|di|
		  puts @DynamicObjectList[di].entityID.to_s+"=="+id.to_s if $debug
		  return @DynamicObjectBodyRef[di] if(@DynamicObjectList[di].entityID == id)
		}
		puts "Didnt find body "+id.to_s if $debug
		return nil
	end #FindBodyWithID(id)
	
	def FindBodyFromInstance(componentInstance)
		@DynamicObjectList.each_index{|di|
		  return @DynamicObjectBodyRef[di] if(@DynamicObjectList[di] == componentInstance)
		}
		return nil
	end	#FindBodyFromInstance(componentInstance)
	
  def FindJointNamed(name)
    @AllJoints.each{|j|
      return j if(j.get_attribute("SPJOINT","name","none")==name)
    }
    return nil;
  end	

    
    class AutoExplodeInstanceObserver
        def onComponentInstanceAdded (definition,instance)
            puts [definition,instance]
            definition.remove_observer($autoExplodeInstanceObserver)
            $autoExplodeTimer=UI.start_timer(0.25,false) {
                #ents=definition.instances[0].explode
                SketchyPhysicsClient::openComponent(definition.instances[0])
            
            }
        end
    end
    
    def self.physicsSafeCopy()
        puts "copying"
        ents=Sketchup.active_model.selection
        cd=Sketchup.active_model.definitions.add
        if($autoExplodeInstanceObserver==nil)
            $autoExplodeInstanceObserver=AutoExplodeInstanceObserver.new
        end
        cd.add_observer($autoExplodeInstanceObserver)
        ents.each{|ent|
            next if(ent.class!=Sketchup::Group&&ent.class!=Sketchup::ComponentInstance)
            resetAxis(ent)
            grp=cd.entities.add_instance(ent.definition,ent.transformation)
            #newGrps.push(grp)
        }
        resetAxis(cd)
        Sketchup.active_model.place_component(cd)
        puts "done"
        return;        
        
        prefix="_"+rand(10000).to_s
        newGrps=[]
        ents.each{|ent|
            if(ent.class==Sketchup::Group)
            ent.make_unique
            newGrps.push(ent)
            resetAxis(ent)
            ent.joints.each{|j|
                #rename joint.
                j.make_unique
                newname=j.get_attribute("SPJOINT","name",nil)+prefix
                j.set_attribute("SPJOINT","name",newname)
                puts "renamed "+newname
            }
            renamedconnections=[]
            ent.connections.each{|c|
                #rename joint.
                renamedconnections.push(c+prefix) if(c!=nil)
            }
            puts "renamed connections "+renamedconnections.inspect
            ent.set_attribute("SPOBJ","parentJoints",renamedconnections) if(renamedconnections.length>0)
            end   
        }        
    end #openComponent(group)
  def self.openComponent(group)
    puts "exploding"
    ents=group.explode
    prefix="_"+rand(10000).to_s
    ents.each{|ent|
      if(ent.class==Sketchup::Group)
        ent.make_unique
        resetAxis(ent)
        ent.joints.each{|j|
          #rename joint.
          j.make_unique
          newname=j.get_attribute("SPJOINT","name",nil)+prefix
          j.set_attribute("SPJOINT","name",newname)
          puts "renamed "+newname
        }
        renamedconnections=[]
        ent.connections.each{|c|
          puts "rename connection"
          renamedconnections.push(c+prefix) if(c!=nil)
        }
        puts "renamed connections "+renamedconnections.inspect
        ent.set_attribute("SPOBJ","parentJoints",renamedconnections) if(renamedconnections.length>0)
      end   
    }        
  end #openComponent(group)
  
  def self.initDirectInput()
    JoyInput.initInput()
  end #initDirectInput
  
  def freeDirectInput()
    JoyInput.freeInput()
  end #freeDirectInput
  
  def readJoystick()
    JoyInput.readJoystick(@joyDataBuffer)
  end #readJoystick()
  
  def initialize
    puts "starting SP" if $debug
    model = Sketchup.active_model
  
    #buffer used to hold results from reading Joystick
    @joyDataBuffer=Array.new.fill(0.0,0..16).pack('f*').to_ptr
    @@frame = 0;
    @@bPause=false;
    #~ @startTime = Time.now
    #~ @lastTime= Time.now
    @cameraTarget=nil
    @AllComponents=model.definitions
    @DynamicObjectList=Array.new
    @DynamicObjectResetPositions=Array.new
    @DynamicObjectBodyRef=Array.new
    @AllMagnets=Array.new
    @AllJoints=Array.new
    @AllJointChildren=Array.new	
    @AllCollisionEntities=Array.new
    @AllTickObjects=[]
    @controlledJoints=[]

    @tempObjects=[]

    @CursorMagnet=NewtonServer.magnetAdd([0,0,0].to_a.pack("f*"))
    @AllMagnets.push(@CursorMagnet)
    
    @allForces=[]
    @allThrusters=[]
    @allTickables=[]
    @allEmitters=[]
    @dyingObjects=[]
    @bodyToGroup=Hash.new

if($gExperimentalFeatures)
    @controllerContext=MSketchyPhysics3::SP3xControllerContext.new()
else
    @controllerContext=MSketchyPhysics3::ControllerContext.new()
end    
    
    #@recordBuffer=nil
    @recordSamples=nil

    view = model.active_view
  
    puts "initializing newtonserver" if $debug
    NewtonServer.init()
    SketchyPhysics.checkVersion
    Sketchup.active_model.start_operation "SketchyPhysics simulation"
    explodeList=[]
    Sketchup.active_model.entities.each { | ent | 
      explodeList.push(ent) if (ent.get_attribute("SPOBJ","component",false) )
    }
    explodeList.each { | ent | 
      openComponent(ent)
    }
  
    checkModelUnits()

    #parse and set physics constants.
    dict=Sketchup.active_model.attribute_dictionary("SPSETTINGS")
    if(dict==nil)
      MSketchyPhysics3::setDefaultPhysicsSettings()
      dict=Sketchup.active_model.attribute_dictionary("SPSETTINGS")
    end
  
    if(dict!=nil)
      dict.each_pair { | key, value | 
        NewtonServer.newtonCommand("set",key+" "+value.to_s)
        puts key+"="+value if $debug
      }
    end

    @frameRate=Sketchup.active_model.get_attribute( "SPSETTINGS", "framerate", 3)
    dumpCollision()

    puts "Finding joints" if $debug
    Sketchup.active_model.entities.each { | ent | 
      if (ent.class== Sketchup::Group)
        type=ent.get_attribute("SPJOINT","type",nil) 
        if(type=="magnet")
          strength=ent.get_attribute("SPJOINT","strength",0.0)
          range=ent.get_attribute("SPJOINT","range",0.0)
          falloff=ent.get_attribute("SPJOINT","falloff",0.0)
          duration=ent.get_attribute("SPJOINT","duration",0)
          delay=ent.get_attribute("SPJOINT","delay",0)
          rate=ent.get_attribute("SPJOINT","rate",0)
          NewtonServer.addGlobalForce(ent.transformation.origin.to_a.pack("f*"),
            [strength,range,falloff].pack("f*"),duration,delay,rate)
        elsif(type!=nil)
          @AllJoints.push(ent)	#save joints for later processing.
          puts "found joint "+ent.entityID.to_s if $debug
        else
          ent.entities.each { | gent |
            if (gent.class== Sketchup::Group)
              if(gent.get_attribute("SPJOINT","type",nil)!=nil)
                gent.set_attribute("SPOBJ","body",ent.get_attribute("SPOBJ","body",nil))
                @AllJoints.push(gent)	#save joints for later processing.
              end
            end
          }
        end
      end
    }

    #init control sliders.
    puts "initializing joint controllers"
    MSketchyPhysics3::initJointControllers()

    #find joint/joint connections. gears.
    @AllJoints.each{|joint|
      parents=JointConnectionTool.getParentJointNames(joint)
      if(parents.length>0)
        if(joint.get_attribute("SPJOINT","gearjoint",nil)!=nil)
          puts "joint/joint"
          gname=joint.get_attribute("SPJOINT","gearjoint",nil)
          gjoint=FindJointNamed(gname)
          if(joint.parent.class==Sketchup::ComponentDefinition)
            pgrp=joint.parent.instances[0]
            bodya=DL::PtrData.new(pgrp.get_attribute("SPOBJ", "body",nil))
            pina=(pgrp.transformation*joint.transformation).zaxis.to_a
          end
          if(gjoint.parent.class==Sketchup::ComponentDefinition)
            pgrp=gjoint.parent.instances[0]
            bodyb=DL::PtrData.new(pgrp.get_attribute("SPOBJ", "body",nil))
            pinb=(pgrp.transformation*gjoint.transformation).zaxis.to_a
          end
          ratio=joint.get_attribute("SPJOINT","ratio",1.0)
          gtype=joint.get_attribute("SPJOINT","geartype",nil)
          puts "making gear "+gtype
          if(bodya!=nil && bodyb!=nil && gtype!=nil)
            jnt=NewtonServer.createGear(gtype,
            pina.pack("f*"), pinb.pack("f*"),
            bodya, bodyb, ratio )
            #puts jnt    
            if(jnt!=0 && joint.get_attribute( "SPJOINT", "GearConnectedCollide", false))
              #puts "coll on"
              NewtonServer.setJointCollisionState(DL::PtrData.new(jnt.to_i),1)
            end
          end
        end
      end
    }
  
    #now create joints
    Sketchup.active_model.selection.clear
    @AllJointChildren.each{|ent|
      jnames=JointConnectionTool.getParentJointNames(ent)
      jnames.each{|jointParentName|
        puts "connecting foo "+ent.to_s+" to "+jointParentName if $debug
        joint=FindJointNamed(jointParentName)
        if(joint!=nil)
          jointType=joint.get_attribute("SPJOINT","type",nil)
          puts "created "+joint.get_attribute("SPJOINT","name","error") if $debug
          defaultJointBody=nil
          jointChildBody=FindBodyWithID(ent.entityID)	
          #TODO.this might not be needed.
          jointChildBody=0 if jointChildBody==nil
          if(joint.parent.class==Sketchup::ComponentDefinition)
            pgrp=joint.parent.instances[0]
            defaultJointBody=DL::PtrData.new(pgrp.get_attribute("SPOBJ", "body",nil))
            parentXform=pgrp.transformation
          else
            parentXform=Geom::Transformation.new
          end
          xform=parentXform*joint.transformation
          limits=Array.new
          limits.push(joint.get_attribute("SPJOINT","min",0))
          limits.push(joint.get_attribute("SPJOINT","max",0))
          limits.push(joint.get_attribute("SPJOINT","accel",0))
          limits.push(joint.get_attribute("SPJOINT","damp",0))
          limits.push(joint.get_attribute("SPJOINT","rate",0))
          limits.push(joint.get_attribute("SPJOINT","range",0))
          limits.push(joint.get_attribute("SPJOINT","breakingForce",0))
          controller=joint.get_attribute("SPJOINT","controller",nil)
          controller=nil if(controller=="")
        #convert if its a 2.0 joint.  
        MSketchyPhysics3::convertControlledJoint(joint) if(controller!=nil)
          controller=joint.get_attribute("SPJOINT","Controller",nil)
          controller=nil if(controller=="")
               
          #puts controller 
          #allow conversion of old and new style joints
          if(controller==nil)
            jointType="hinge" if(jointType=="servo")
            jointType="slider" if(jointType=="piston")
            #puts "demoted joint" 
          else
            if(jointType=="hinge")
              jointType="servo"
              if(limits[0]==0 && limits[1]==0)
                limits[0]=-180.0
                limits[1]=180.0
              end            
            elsif(jointType=="slider")
              jointType="piston"
              #puts limits
            end
            #puts "promoted joint "+jointType if $debug
          end    

          #puts "jointparents:"+JointConnectionTool.getParentJointNames(joint).inspect

          #detect joint to joint connection
          #detect parent body for each joint
          #determine gear type based on joint types
          #get ratio (where?)

          pinDir=xform.zaxis.to_a+xform.yaxis.to_a
          #old style gears. REMOVE.
          if(jointType=="gear"||jointType=="pulley"||jointType=="wormgear")
            #puts "gear. parent:"+defaultJointBody.class.to_s+" child:"+jointChildBody.to_s                  
            ratio=joint.get_attribute("SPJOINT","ratio",1.0)
            jnt=NewtonServer.createGear(jointType,
            pinDir.pack("f*"), pinDir.pack("f*"),
            jointChildBody, defaultJointBody, ratio )
          else
            jnt=NewtonServer.createJoint(jointType,xform.origin.to_a.pack("f*"),
            pinDir.pack("f*"),
            jointChildBody,defaultJointBody, 
            limits.pack("f*") )	
            #puts joint.get_attribute( "SPJOINT", "ConnectedCollide", 0)
            #puts jnt
            #set collision between connected bodies.
            if(jnt!=0)
              joint.set_attribute("SPJOINT","jointPtr",jnt.to_i)
              if(jnt!=0 && joint.get_attribute( "SPJOINT", "ConnectedCollide", false))
                #puts "coll on" 
                NewtonServer.setJointCollisionState(DL::PtrData.new(jnt),1)
              end
              #handle controlled joints.
              #controller=joint.get_attribute("SPJOINT","controller","")
              if(controller!=nil)
                @controlledJoints.push(joint)
                value=limits[0]+((limits[1]-limits[0])/2)
                value=0.5# utb 0.5 jan 7.
                #MSketchyPhysics3::createController(controller,value,0.0,1.0)
                #puts "controlled joint"
              end

            end
          end    
        end
      }
    }
    #handleAttachments()
    setupCameras()
    MSketchyPhysics3::showControlPanel()
    @startTime = Time.now
    @lastTime= Time.now
  end #initialize

    def  handleOnTouch(bodies)
        bodies.each{|b|
            if(@bodyToGroup[b.to_i]!=nil)
                grp=@bodyToGroup[b.to_i]
                $curEvalGroup=grp    
                func=grp.get_attribute("SPOBJ","ontouch","").to_s
                next if(func=="")
                last=grp.get_attribute("SPOBJ","lasttouchframe",0)
                rate=grp.get_attribute("SPOBJ","touchrate",0).to_i
                #too early?
                next if(@@frame-last<rate)  
        
                begin
                    result=eval(func,@curControllerBinding)
                    grp.set_attribute("SPOBJ","lasttouchframe",@@frame)
                rescue
                    puts "OnTouch error"+" : "+$!
                    next            
                end
        
            end 
        }        
    end
  def updateControlledJoints()
#return
    JoyInput.updateInput()  
    cbinding=@controllerContext.getBinding(@@frame)
    @curControllerBinding=cbinding
    
    @allTickables.each{|grp|
        $curEvalGroup=grp    
        func=grp.get_attribute("SPOBJ","ontick","").to_s
        next if(func=="")
        nextframe=grp.get_attribute("SPOBJ","nexttickframe",0)
        #too early?
        next if(@@frame<nextframe)  

        begin
            rate=grp.get_attribute("SPOBJ","tickrate",0).to_i
            result=eval(func,@curControllerBinding)
            #puts result.class
            if(rate==0 && result.is_a?(Numeric) && result!=0)
                grp.set_attribute("SPOBJ","nexttickframe",@@frame+result.to_i)
            else
                grp.set_attribute("SPOBJ","nexttickframe",@@frame+rate)
            end
        rescue
            puts "OnTick error"+" : "+$!
            next            
        end
    }
    @allForces.each{|grp|
        $curEvalGroup=grp    
        strength=grp.get_attribute("SPOBJ","strength",0.0).to_s
        begin
            value=eval(strength,cbinding).to_f
            value=0.0 if !value.is_a?(Numeric)
            force=DL::PtrData.new(grp.get_attribute("SPOBJ","__force",nil))
            if(force!=nil)
                NewtonServer.setForceStrength(force,value)
            end
        rescue
            puts expression+" : "+$!
            next            
        end

        #puts grp.get_attribute("SPOBJ","__force",nil)
    }
    @allThrusters.each{|grp| 
        $curEvalGroup=grp    
        expression=grp.get_attribute("SPOBJ","tstrength",0.0).to_s
        begin
            value=eval(expression,cbinding).to_f
            next if !value.is_a?(Numeric)
            body=DL::PtrData.new(grp.get_attribute("SPOBJ","body",nil))
            if(body!=nil)
                NewtonServer.bodySetThrust(body,value)
            end
        rescue
            puts expression+" : "+$!
            next            
        end
    }
    @allEmitters.each{|grp|
        $curEvalGroup=grp    
        
        #is it time for this object to dupe itself yet?
        ratestr=grp.get_attribute("SPOBJ","emitterrate",0).to_s
        begin
            rate=eval(ratestr,cbinding)
            next if(rate==0.0 || !rate.is_a?(Numeric))
        rescue
            puts "Emitter rate error:"+ratestr+" : "+$!
            next            
        end
        #puts "rate"+rate.to_s

        last=grp.get_attribute("SPOBJ","lastemissionframe",0)
        #too early?
        next if(@@frame-last<rate)     
        
        expression=grp.get_attribute("SPOBJ","emitterstrength",0.0).to_s
        begin
            value=eval(expression,cbinding)
#TODO:need to check type here.            
            #body=DL::PtrData.new(grp.get_attribute("SPOBJ","body",nil))
            if(grp.valid?)
                newgrp=emitGroup(grp,nil,value)
            end
        rescue
            puts "Emitter:"+expression+" : "+$!
            next            
        end
    }
    
    @dyingObjects.each{|grp,lastframe|
        if(lastframe<=@@frame)
            #puts "killing"+lastframe.to_s
            destroyTempObject(grp) if(grp.valid?)
            @dyingObjects.delete([grp,lastframe])
            grp.erase! if grp.valid?

            #find if in tempObjects
            #if(grp.valid?)
                
            #remove from tempObjects and dyingObjects
            #deleteBody
        end
    }

    @controlledJoints.each{|joint| 
        $curEvalGroup=joint    
        expression=joint.get_attribute("SPJOINT","Controller",nil)
        if(expression!=nil)
            begin
                value=eval(expression,cbinding)
            rescue
                puts expression+" : "+$!
                next            
            end
        end
        case joint.get_attribute("SPJOINT","type",nil)
            when "hinge","servo"
                next if !value.is_a?(Numeric)
                NewtonServer.setJointRotation(DL::PtrData.new(joint.get_attribute("SPJOINT","jointPtr",0)),[value].pack("f*"))
            when "piston","slider"
                next if !value.is_a?(Numeric)
                NewtonServer.setJointPosition(DL::PtrData.new(joint.get_attribute("SPJOINT","jointPtr",0)),[value].pack("f*"))
            when "motor"
                next if !value.is_a?(Numeric)
                maxAccel=joint.get_attribute("SPJOINT","maxAccel",0)
                accel=(value)*maxAccel
                NewtonServer.setJointAccel(DL::PtrData.new(joint.get_attribute("SPJOINT","jointPtr",0)),[accel].pack("f*"))
            when "gyro"
                next if !value.is_a?(Array)
                NewtonServer.setGyroPinDir(DL::PtrData.new(joint.get_attribute("SPJOINT","jointPtr",0)),value.pack("f*"));
        end
    }
        
  end


  def oldupdateControlledJoints()
    frame=@@frame
        
    @controlledJoints.each{|joint| 
      controller=joint.get_attribute("SPJOINT","controller","")
      value=nil
      if(controller.index("oscillator"))
        vals=controller.split(',')
        rate=vals[1].to_f
        inc=(2*3.141592)/rate
        pos=Math.sin(inc*(@@frame))
        $controlSliders[controller].value=(pos/2.0)+0.5
        value=$controlSliders[controller].value
      else
        #value=eval(controller,binding) 
        value=$controlSliders[controller].value
      end
      
      raise "controller failure" if(value==nil)
      
      case joint.get_attribute("SPJOINT","type",nil) 
      when "oscillator"
        rate=joint.get_attribute("SPJOINT","rate",100.0)
        rate=rate/value
        inc=(2*3.141592)/rate
        pos=Math.sin(inc*(@@frame))
        #puts pos if $debug
        NewtonServer.setJointPosition(DL::PtrData.new(joint.get_attribute("SPJOINT","jointPtr",0)),[pos].pack("f*"))
      when "servo","hinge"
        NewtonServer.setJointRotation(DL::PtrData.new(joint.get_attribute("SPJOINT","jointPtr",0)),[value].pack("f*"))
      when "piston","slider"
        NewtonServer.setJointPosition(DL::PtrData.new(joint.get_attribute("SPJOINT","jointPtr",0)),[value].pack("f*"))
      when "motor"
        maxAccel=joint.get_attribute("SPJOINT","maxAccel",0)
        accel=(value)*maxAccel
        NewtonServer.setJointAccel(DL::PtrData.new(joint.get_attribute("SPJOINT","jointPtr",0)),[accel].pack("f*"))
        #puts pos if $debug
      end
    }

  end #updateControlledJoints()
    
  def setupCameras()
    camera=Sketchup.active_model.active_view.camera  
    $savedCameraPosition=Sketchup::Camera.new(camera.eye, camera.target, camera.up,camera.perspective?,camera.fov)
    desc=Sketchup.active_model.pages.selected_page.description.downcase if(Sketchup.active_model.pages.selected_page!=nil)
    return if (desc==nil)
    
    sentences=desc.split('.')
    sentences.each{|l|
      words=l.split(' ')
      if(words[0]=='camera')
        @cameraTarget=FindGroupNamed(words[2]) if(words[1]=='track')
        @cameraParent=FindGroupNamed(words[2]) if(words[1]=='follow')
        puts @cameraTarget
      end
    }
  end #setupCameras()

    

  
	def nextFrame(view)

#inTime=Time.now() 
        #totalTime = Time.now - @startTime
        #elapsedTime=Time.now-@lastTime
        #lastTime=Time.now
        if @@bPause==true
            view.show_frame
            return true
        end

        if($bSPDoRecord)
            if(@recordSamples==nil)
                @recordSamples=[]
                @DynamicObjectList.each_index{|id|
                    if(@DynamicObjectList[id].valid?)
                        @recordSamples[id]=[@DynamicObjectList[id].transformation.to_a]
                    end
                    }
            end                
        end
        
#updateTime=Time.now()
        updateControlledJoints()
        #updateEvalJoints()
        NewtonServer.requestUpdate(@frameRate)  
#updateTime=Time.now()-updateTime           
        cameraPreMoveOffset=Sketchup.active_model.active_view.camera.eye-@cameraParent.bounds.center if(@cameraParent!=nil)
            #fetch positions for all moving objects.
        dat=Array.new.fill(0.0,0..16).pack('f*').to_ptr
#fetchTime=Time.now()
#fetchCount=0
#totalMoveTime=0
        while (id=NewtonServer.fetchSingleUpdate(dat))!=0xffffff 
#        while (fetchCount<367) 
#fetchCount+=1
            matrix=dat.to_a("F",16)
            instance=@DynamicObjectList[id]
#instance=@DynamicObjectList[fetchCount]
            if instance!=nil && instance.valid?
                dest=Geom::Transformation.new(matrix.to_a)
                #reapply the scaling
                #################
               
                #TODO: This is slow. Cache scale in obj
                #dest=dest*extractScaleFromGroup(instance)

                #DONE. But not any faster for some reason.
                upscale=Geom::Transformation.new(instance.get_attribute("SPOBJ", "savedScale", nil))
                dest=dest*upscale


                #################
        #oldTransform=instance.transformation.to_a
                #################
#mt=Time.now
                instance.move!(dest)
#et=Time.now-mt
#totalMoveTime+=et
                if($bSPDoRecord)
                    @recordSamples[id][@@frame]=dest.to_a if(@recordSamples[id]!=nil)
                    #recordFrameBuffer.push([instance,dest])                    
                end                    
                
            end
        end   
#fetchTime=Time.now()-fetchTime     
        
        if(@cameraTarget!=nil)
            camera = Sketchup.active_model.active_view.camera
            camera.set(camera.eye, @cameraTarget.bounds.center, Geom::Vector3d.new(0, 0, 1))
        end
        if(@cameraParent!=nil)
            camera = Sketchup.active_model.active_view.camera
            dest=@cameraParent.bounds.center+cameraPreMoveOffset
            camera.set(dest, dest+camera.direction, Geom::Vector3d.new(0, 0, 1))
        end
		@@frame=@@frame+1
        @frame=@@frame
        Sketchup.set_status_text("Frame:"+@frame.to_s)
#@totalTime=0 if @totalTime==nil
#outTime=Time.now()-inTime
#@totalTime+=outTime#+(@totalTime/@frame).to_s
#$outputbuf.push((@frame.to_s+"T:#{outTime} U:#{updateTime} F:#{fetchTime} Count:#{fetchCount} totalMoveTime:#{totalMoveTime}")) if (@frame%2==0)
        
		view.show_frame
		return true
	end #nextFrame(view)
$outputbuf=[]

	def resetSimulation
        @@bPause=true
        et=Time.now-@startTime
        
        Sketchup.active_model.active_view.camera=$savedCameraPosition
#puts $outputbuf
#$outputbuf=[]

        puts ["Time",et,"Frames",@frame,"Fps",@frame/et].inspect

        #puts "#{@frame} frames in #{et} seconds. #{et/@frames} FPS"
        @tempObjects.each{|ent|ent.erase! if ent.valid?}
        
		@DynamicObjectList.each_index {|ci|
            next if(!@DynamicObjectList[ci].valid?)       
            @DynamicObjectList[ci].move!(@DynamicObjectResetPositions[ci]) 
        }
        Sketchup.active_model.commit_operation
        
        if(@recordSamples!=nil && !@recordSamples.empty?)
           result=UI.messagebox("Save animation?", 
              MB_YESNO, "Save Animation")
            #BUG fix. This cant be above UI.message or SU crashes.
#result=6
            case result
                when 6#yes
            		Sketchup.active_model.start_operation "Save animation"
                    #puts @recordSamples.inspect
                    @DynamicObjectList.each_index {|ci| 
                        if(@recordSamples[ci]!=nil && @DynamicObjectList[ci]!=nil &&@DynamicObjectList[ci].valid?)
                            @DynamicObjectList[ci].set_attribute("SPTAKE","samples",@recordSamples[ci])
                        end
                        }
                    Sketchup.active_model.commit_operation
                    #brand each group.
                    #save anim data in group.
                    #save anim data in model attribs.
                    #return;
                    #compress and embed animtion
                when 7#no
                    #return;
                when 2#cancel
                    #return;
                end
        end
        #this needs to come last or it bugsplats for some reason.
        NewtonServer.stop()
	end #resetSimulation

  def commitSimulation
    puts "Phys commit"
    Sketchup.active_model.abort_operation
		Sketchup.active_model.start_operation "Physics simulation"
    @DynamicObjectList.each_index {|ci| 
      xform=@DynamicObjectList[ci].transformation; 
      @DynamicObjectList[ci].move!(@DynamicObjectResetPositions[ci])
      @DynamicObjectList[ci].transformation=xform
    }
    Sketchup.active_model.commit_operation
  end #commitSimulation

	##################################################start of tool################################################
	# The stop method will be called when SketchUp wants an animation to stop
	# this method is optional.
	def stop
		#puts "stop called" if $debug
		@bWasStopped=true
	end #stop

	# The activate method is called when a tool is first activated.  It is not
	# required, but it is a good place to initialize stuff.
	def activate
		puts "activate called" if $debug
    Sketchup::set_status_text "Click and drag to move. Press CTRL while dragging to lift."	
	end #activate

	def deactivate(view)
		#resetSimulation
        @@bPause=true
        SketchyPhysicsClient::physicsReset()
        MSketchyPhysics3::closeControlPanel()
        freeDirectInput()
	end #deactivate(view)

    def onCancel(a,b)
        deactivate(0)
    end #onCancel()
        
	def suspend(view)
	    puts "suspend called" if $debug
	end #suspend(view)
	
	def resume(view)
		puts "resume called" if $debug
    Sketchup::set_status_text "Click and drag to move. Press SHIFT while dragging to lift."	
	end #resume(view)

	@pickedBody=nil
	@magnetLocation=nil
	
	def onMouseMove(flags, x, y, view)
    @mouseX=x
    @mouseY=y    
		return if(@CursorMagnetBody==nil)
		ip = Sketchup::InputPoint.new
		ip.pick view, x, y
		if( ip.valid? )
      if(@pickedBody!=nil)
        if(@shiftDown)
          #project the input point on a plane described by our normal and center. 
          ep=Geom.intersect_line_plane([view.camera.eye,ip.position], [@attachWorldLocation,view.camera.zaxis])
          @attachWorldLocation=ep
          pos=ep
        else
          ep=Geom.intersect_line_plane([view.camera.eye,ip.position], [@attachWorldLocation,Z_AXIS])
          pos=ep
          @attachWorldLocation=ep
        end
        @magnetLocation=pos
        puts pos if $debug
        NewtonServer.magnetMove(@CursorMagnet,pos.to_a.pack("f*")) if(@CursorMagnet!=nil)
      end
    end    
	end #onMouseMove(flags, x, y, view)

  def getMenu(menu)
    puts menu if $debug 
    ph=Sketchup.active_model.active_view.pick_helper
		ph.do_pick @mouseX,@mouseY
		ent=ph.best_picked
    Sketchup.active_model.selection.add ent
    menu.add_item("Camera track"){
      @cameraTarget=ent
    }
    menu.add_item("Camera follow"){
      @cameraParent=ent
    }
    menu.add_item("Camera clear"){
      @cameraTarget=nil
      @cameraParent=nil
    }
    #menu.add_item("Copy body"){
    #  copyBody(ent)
    #}
  end #getMenu(menu)

	def onMButtonDown(flags, x, y, view)
		ph=view.pick_helper
		ph.do_pick x,y
		ent=ph.best_picked
    puts "safda"
    @cameraTarget=ent
  end #onMButtonDown(flags, x, y, view)
  
  def onRButtonDown(flags, x, y, view)
    puts "rbutton"
    ent=ph.best_picked
    puts ent
  end #onRButtonDown(flags, x, y, view)
  
  def onRButtonUp(flags, x, y, view)
    puts "rbutton"
    ent=ph.best_picked
    puts ent
  end 
  
  def onLButtonDoubleClick(flags, x, y, view)
    ph=view.pick_helper
		ph.do_pick x,y
		ent=ph.best_picked
    if(!MSketchyPhysics3::getKeyState(VK_LSHIFT))
      direction=ent.transformation.origin-view.camera.eye
      direction.normalize!
      direction[0]*=3.0
      direction[1]*=3.0
      direction[2]=15.0
      kick=direction
      body=FindBodyWithID(ent.entityID)	
      NewtonServer.addImpulse(body,kick.to_a.pack("f*"),ent.transformation.origin.to_a.pack("f*")) if(body!=nil)
    else
      ip=view.inputpoint x,y
      #copyBody(ent)
    end
  end #onLButtonDoubleClick(flags, x, y, view)
    
	def onLButtonDown(flags, x, y, view)
    ph=view.pick_helper
    ph.do_pick x,y
    ent=ph.best_picked
    if(@CursorMagnetBody!=nil&&@CursorMagnetBody!=0)
      NewtonServer.bodySetMagnet(@CursorMagnetBody,0,0)
			@CursorMagnetBody=nil
			Sketchup.active_model.selection.clear
		end
		if(ent.class == Sketchup::ComponentInstance || ent.class==Sketchup::Group)
			Sketchup.active_model.selection.clear
			Sketchup.active_model.selection.add ent
			@pickedBody=ent
			gd=ent.entities.parent
			puts "gd.bounds.center "+ gd.bounds.center.to_s if $debug
			@DynamicObjectList.each_index{|doi| 
				if(@DynamicObjectList[doi]==ent)
					#transform input point into component space.
					ip=view.inputpoint x,y
          puts ip.position if $debug
          puts "center" if $debug		
          puts @pickedBody.entities[0].parent.bounds.center.to_s if $debug
          puts @pickedBody.transformation.origin.to_s if $debug
          cdbounds=@pickedBody.entities[0].parent.bounds
          dsize=cdbounds.max-cdbounds.min
          cmass=Geom::Point3d.new(dsize.to_a)
          cmass.x/=2;cmass.y/=2;cmass.z/=2;
          puts "center of mass" if $debug
          puts cmass.to_s if $debug
          puts cdbounds.center.class if $debug
          puts cdbounds.center if $debug
          pcenter=Geom::Point3d.new(dsize.y/2,dsize.x/2,dsize.z/2)
          xlate=(Geom::Transformation.new(pcenter).inverse)
          ip=view.inputpoint x,y
					@attachPoint=ip.position.transform(@pickedBody.transformation.inverse)
          puts "attach" if $debug
          puts @attachPoint if $debug
          @attachWorldLocation=ip.position #used to calc movment planes.
					NewtonServer.bodySetMagnet(@DynamicObjectBodyRef[doi],@CursorMagnet,@attachPoint.to_a.pack("f*"))
					@CursorMagnetBody=@DynamicObjectBodyRef[doi];
        end
      }
      onMouseMove(flags, x, y, view);	#force magnet location to update.
    end
    return
  end #onLButtonDown(flags, x, y, view)
  
	def onLButtonUp(flags, x, y, view)
		@pickedBody=nil
		@magnetLocation=nil
		if(@CursorMagnetBody!=nil)
			NewtonServer.bodySetMagnet(@CursorMagnetBody,nil,nil)
			@CursorMagnetBody=nil
			Sketchup.active_model.selection.clear
		end
	end #onLButtonUp(flags, x, y, view)

	def draw(view)
		if(@pickedBody!=nil &&@magnetLocation!=nil) 
      view.line_width=3
      view.drawing_color="red"
			view.draw_line(@attachPoint.transform(@pickedBody.transformation), @magnetLocation)
      view.drawing_color="blue"
      view.line_width=1
      tp=@magnetLocation.clone()
      tp.z=-100
			view.draw_line(tp, @magnetLocation)
		end
		if(@bWasStopped)
      Sketchup.active_model.active_view.animation = $sketchyPhysicsToolInstance
			@bWasStopped=false
		end
	end #draw(view)

	def onSetCursor()
		cursor = UI::set_cursor(671)
	end #onSetCursor()

  @ctrlDown=false
  @shiftDown = false
  @cursorCount=0
	@delay=0
	
  def onKeyDown(key, rpt, flags, view)
        @ctrlDown=true if( key == COPY_MODIFIER_KEY && rpt == 1 )
        @shiftDown = true if( key == CONSTRAIN_MODIFIER_KEY && rpt == 1 )
  end #onKeyDown(key, rpt, flags, view)

  def onKeyUp(key, rpt, flags, view)
        @ctrlDown=false if( key == COPY_MODIFIER_KEY)
        @shiftDown = false if( key == CONSTRAIN_MODIFIER_KEY)
  end #onKeyUp(key, rpt, flags, view)

  def checkModelUnits
    model = Sketchup.active_model
    manager = model.options
    provider = manager[3]
    puts provider.name if $debug
    provider["SuppressUnitsDisplay"]=true
    provider["LengthFormat"]=0
  end #checkModelUnits
  
  # Start the physics simulation
  def self.startphysics
    initDirectInput()
    if $sketchyPhysicsToolInstance==nil
        begin
            $sketchyPhysicsToolInstance=SketchyPhysicsClient.new
        rescue => e 
            puts e.inspect 
            puts e.backtrace 
            #puts "Error : "+$!
            UI.messagebox("Error starting the simulation:"+" : "+$!+"  \n"+e.backtrace[0],
                MB_OK, "Error")
            return           
        end

      Sketchup.active_model.active_view.animation = $sketchyPhysicsToolInstance
      #turn on physics grab tool
      Sketchup.active_model.select_tool $sketchyPhysicsToolInstance
    end	
  end #self.startphysics

  def self.physicsTogglePlay()
    @lastTime=Time.now
    if $sketchyPhysicsToolInstance==nil
      startphysics()
      @@bPause=false
      return
    end
    if @@bPause
      @@bPause=false
    else
      @@bPause=true
    end
  end #self.physicsTogglePlay()
  
  def self.physicsReset()
    if($sketchyPhysicsToolInstance!=nil)
        Sketchup.active_model.active_view.animation = nil
        begin
            $sketchyPhysicsToolInstance.resetSimulation
        rescue => e 
            puts e.inspect 
            puts e.backtrace 
            #puts "Error : "+$!
            UI.messagebox("Error resetting the simulation:"+" : "+$!+"  \n"+e.backtrace[0],
                MB_OK, "Error")
        end
        $sketchyPhysicsToolInstance=nil
        Sketchup.active_model.select_tool nil
    end
  end

  def self.physicsRecord()
    if(@bDoRecord==true)
        @@bPause=true
        result=UI.messagebox("Recorded #{@@frame} frames. Save animation? Press Cancel to continue recording.", 
            MB_YESNOCANCEL, "Save Animation")
        case result
            when 6#yes
                physicsReset()
                return;
                #compress and embed animtion
            when 7#no
                physicsReset()
                return;
            when 2#cancel
                return;
        end
    end
    @bDoRecord=true
    physicsTogglePlay()
  end #physicsRecord()
    
  def self.physicsStop()
    if $sketchyPhysicsToolInstance==nil
        @@bDoRecord=true
        startphysics()
        return        
    end

    if(@@bDoRecord==true)
        @@bDoRecord=false
        @@bPause=true
        keypress = UI.messagebox "Save recorded animation?" , MB_YESNO, "Error"
        if (keypress == 6)
            # code to respond to YES
            Sketchup.active_model.active_view.animation = nil      
            $sketchyPhysicsToolInstance.commitSimulation
            $sketchyPhysicsToolInstance=nil
            Sketchup.active_model.select_tool nil
        end
    else
        @@bDoRecord=true
    end
  end #self.physicsStop()
  
end # class SketchyPhysicsClient
	##################################################end of tool################################################

#-----------------------------------------------------------------------------
# Add an physics menu items to the Tools menu
if( not file_loaded?("SketchyPhysicsTool.rb") )
   add_separator_to_menu("Help")
   SketchyPhysicsClient_menu = UI.menu("Help")
   SketchyPhysicsClient_menu.add_item("About SketchyPhysics") {UI.messagebox "SketchyPhysics3.\nhttp://code.google.com/p/sketchyphysics2\nCopyright 2009 C Phillips.\nSketchyPhysics uses the Newton Physics SDK"}
end

#main UI setup
toolbar = UI::toolbar "Sketchy Physics"
#toolbar = UI::Toolbar.new "Sketchy Physics" if toolbar==nil
playcmd = UI::Command.new("Play") { SketchyPhysicsClient::physicsTogglePlay() } 

path = Sketchup.find_support_file "SketchyPhysics-PlayPauseButton.png", "plugins/SketchyPhysics3/Images/"
playcmd.small_icon = path
playcmd.large_icon = path
playcmd.tooltip = "Play/Pause physics simulation"
playcmd.status_bar_text = "Play/Pause physics simulation"
playcmd.menu_text = "Play/Pause"

toolbar.add_item playcmd

cmd = UI::Command.new("Reset") { SketchyPhysicsClient::physicsReset()	}

path = Sketchup.find_support_file "SketchyPhysics-RewindButton.png", "plugins/SketchyPhysics3/Images/"
cmd.small_icon = path
cmd.large_icon = path
cmd.tooltip = "Reset physics simulation"
cmd.status_bar_text = "Reset physics simulation"
cmd.menu_text = "Reset"
toolbar.add_item cmd

cmd = UI::Command.new("Record") { SketchyPhysicsClient::physicsStop()}
path = Sketchup.find_support_file "SketchyPhysics-StopButton.png", "plugins/SketchyPhysics3/Images/"
cmd.small_icon = path
cmd.large_icon = path
cmd.tooltip = "Save animation data"
cmd.status_bar_text = "Save animation data and reset"
cmd.menu_text = "Save animation"
#toolbar.add_item cmd


cmd = UI::Command.new("ShowUI") { $physicsInspectorDialog.toggleDialog() }
path = Sketchup.find_support_file "SketchyPhysics-ShowUIButton.png", "plugins/SketchyPhysics3/Images/"

cmd.small_icon = path
cmd.large_icon = path
#cmd.tooltip = "Show UI"

cmd.status_bar_text = "Show UI"
cmd.menu_text = "Show UI"
toolbar.add_item cmd

cmd = UI::Command.new("JointConnectionTool") { Sketchup.active_model.select_tool(MSketchyPhysics3::JointConnectionTool.new()) }
cmd.status_bar_text = cmd.tooltip = "Joint connector"
#cmd.large_icon = cmd.small_icon = "images/JointConnector.png"
cmd.large_icon = cmd.small_icon = Sketchup.find_support_file("JointConnector.png", "plugins/SketchyPhysics3/Images/")
toolbar.add_item(cmd)

toolbar.show

file_loaded("SketchyPhysicsTool.rb")
end #module MSketchyPhysics3::