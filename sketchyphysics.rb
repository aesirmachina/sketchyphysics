#load "sdl"

class SketchyPhysics
    def self.LoadSketchyPhysics2
        load "SketchyPhysics/sketchyphysicstool.rb"
        load "SketchyPhysics/sketchyphysicsutil.rb"
        load "SketchyPhysics/JointTool.rb"	
        load "SketchyPhysics/SketchyReplay.rb"	
        load "SketchyPhysics/attributes.rb"	
        load "SketchyPhysics/clearConsole.rb"	
        load "SketchyPhysics/JointConnectionTool.rb"
        load ("SketchyPhysics/controlPanel.rb") 
        load ("SketchyPhysics/inspector.rb") 
        load "SketchyPhysics/PrimsTool.rb"	
        load "SketchyPhysics/BoxPrimTool.rb" 
    end

    def self.LoadSketchyPhysics3
        load ("SketchyPhysics3/ClassExtensions.rb")     
        load ("SketchyPhysics3/sketchyphysicstool.rb")
        load ("SketchyPhysics3/sketchyphysicsutil.rb")
        load ("SketchyPhysics3/JointTool.rb")
        load ("SketchyPhysics3/JointConnectionTool.rb")
        load ("SketchyPhysics3/controlPanel.rb") 
        load ("SketchyPhysics3/inspector.rb") 
        load ("SketchyPhysics3/newton.rb") 
        load ("SketchyPhysics3/input.rb") 
        load ("SketchyPhysics3/sketchysolids.rb") 
        load ("SketchyPhysics3/attachTool.rb") 

        load ("SketchyPhysics3/SketchyReplay.rb") 
        load ("SketchyPhysics3/ControllerCommands.rb") 
    end
    def self.setLoadVersion(version)
        Sketchup.write_default("SketchyPhysics", "version_to_load",version)
        if(version==0)
            UI.messagebox "SketchyPhysics will be disabled the next time you run Sketchup. It can be re-enabled by selecting a version under Plugins->SketchyPhysics->Version", MB_OK, "SketchyPhysics"
            return
        end
        if($sketchyphysics_version_loaded==0)
            doLoad(version)
            UI.messagebox "SketchyPhysics version #{version} loaded.", MB_OK, "SketchyPhysics"
        else            
            UI.messagebox "SketchyPhysics version #{version} will be loaded the next time you reload Sketchup.", MB_OK, "SketchyPhysics"
        end
    end
    def self.getLoadVersion()
        return Sketchup.read_default("SketchyPhysics", "version_to_load",3)
    end
    def self.doLoad(version)
        case version
            when 0
                $sketchyphysics_version_loaded=0                
            when 2
                $sketchyphysics_version_loaded=2
                LoadSketchyPhysics2()
            when 3
                $sketchyphysics_version_loaded=3
                LoadSketchyPhysics3()
        end	        
    end
end
$gExperimentalFeatures=true

if( not file_loaded?("SketchyPhysics.rb") )

	submenu=UI.menu("Plugins")
	submenu.add_separator
	smenu =submenu.add_submenu("SketchyPhysics")
    if(SketchyPhysics.getLoadVersion()==2)
        item = smenu.add_item("Physics Settings") {editPhysicsSettings()}
        item = smenu.add_item("Create Default Scene") {createDefaultScene() }
    else
        item = smenu.add_item("Physics Settings") {MSketchyPhysics3::editPhysicsSettings()}
        item = smenu.add_item("Buoyancy Settings") {MSketchyPhysics3::setupBuoyancy()}
    end
    vmenu=smenu.add_submenu("Version")
    
    item=vmenu.add_item("SketchyPhysics2"){SketchyPhysics.setLoadVersion(2)}
	vmenu.set_validation_proc(item) {
        #r=MF_UNCHECKED
        if($sketchyphysics_version_loaded==2)	
        	MF_CHECKED
	    else
            MF_UNCHECKED
        end
	    
    }

    item=vmenu.add_item("SketchyPhysics3"){SketchyPhysics.setLoadVersion(3)}
	vmenu.set_validation_proc(item) {
        if($sketchyphysics_version_loaded==3)	
        	MF_CHECKED
	    else
            MF_UNCHECKED
        end
    }
    item=vmenu.add_item("Disabled"){SketchyPhysics.setLoadVersion(0)}
	vmenu.set_validation_proc(item) {
        if($sketchyphysics_version_loaded==0)	
        	MF_CHECKED
	    else
            MF_UNCHECKED
        end
    }    
    SketchyPhysics.doLoad(SketchyPhysics.getLoadVersion())
    
	
end	

#load ("SketchyPhysics4/SketchyPhysics4.rb") 

file_loaded("SketchyPhysics.rb") 

