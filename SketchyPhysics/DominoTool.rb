require 'sketchup.rb'


def findConnectedSegments(edge)
    #ca=segs.index(a)
    connectedEdges=[]
    Sketchup.active_model.selection.each{|e|
        if(e.class==Sketchup::Edge && e!=edge && !e.get_attribute("domino","visited",false))
            va=e.vertices[0].position
            vb=e.vertices[1].position
            ea=edge.vertices[0].position
            eb=edge.vertices[1].position
            if(va==ea || va==eb || vb==ea || vb==eb)
                connectedEdges.push(e)
            end
        end
    }    
    return(connectedEdges)
end

def placeDominosOnEdge(edge)
    #puts "Dominos along "+edge.vertices.to_s
    
    if(edge.get_attribute("domino","visited",false))
        return
    end
    spacing=5.0
    count=edge.length/spacing
    stepVector=edge.vertices[1].position-edge.vertices[0].position
    stepVector.length=stepVector.length/count
    pos=edge.vertices[0].position
    0.upto(count){|i|
        pos=pos+stepVector
        grp=@dominoGroup.copy()
        xform=Geom::Transformation.new(pos+[0,0,3.5],stepVector.cross([0,0,1]),stepVector)
        grp.transform!(xform)
    }
    edge.set_attribute("domino","visited",true)

    ce=findConnectedSegments(edge)
    ce.each{|e|
        placeDominosOnEdge(e)
        }
end

@dominoGroup=nil

def findLoops()
    #find an endpoint
    @dominoGroup=makePhysicsCube(Sketchup.active_model.active_entities,3.0,1.0,7.0)
    Sketchup.active_model.start_operation("place dominos along selected egdes")
    Sketchup.active_model.selection.each{|e|
        if(e.class==Sketchup::Edge)
            e.set_attribute("domino","visited",false)
        end
    }
    Sketchup.active_model.selection.each{|e|
        if(e.class==Sketchup::Edge)
            ce=findConnectedSegments(e)
            if(ce.length==1)
                #puts "start edge="+ce[0].vertices.to_s
                placeDominosOnEdge(e)
            end
        end
    }
    Sketchup.active_model.commit_operation

end
#each point is a node
#each point also has one or more links to other nodes.
#0 links should be impossible
#1 link is a start or a end
#2 links middle segment
#3 or more links branch

#nodes
def buildNodes()
    nodes=Hash.new 
    Sketchup.active_model.selection.each{|e|
        va=e.vertices[0].position
        vb=e.vertices[1].position
        if (nodes[va.to_s]==nil)
            nodes[va.to_s]=[] 
        end
        if (nodes[vb.to_s]==nil)
            nodes[vb.to_s]=[] 
        end
        
        na=nodes[va.to_s]    
        nb=nodes[vb.to_s]

        na.push(vb)
        nb.push(va)
    }
    #build a loop

    nodes.each{|k,v| 
        if(v.length==1)
            loop=[]
            nameA=k.to_s
            nameB=v.to_s
            nodeA=nodes[nameA]
            nodeB=nodes[nameB]
            while(true)

                loop.push(nameA)
                loop.push(nameB)
                nodeB.delete(na)
                
                if(nodes[v.to_s].length>0)
                    tv=v
                    k=v
                    v=nodes[tv.to_s][0]
                else
                    puts loop.join(",")
                    break
                end
            end
        end
    }
    return nil
end

def drawNodes(from,nodes)
            
end
