require 'sketchup.rb'


class Array #Array to Hash. Nice! found on codesnipets.com
  def to_h(&block)
    Hash[*self.collect { |v|
      [v, block.call(v)]
    }.flatten]
  end
end

class SketchyReplay
	def initialize
		@frame=0;
		@bPaused=true;
		@bStopped=true;
		@AnimationObjectList=nil
		@animationRate=2
		
	end

	def findAnimationObjects
		@AnimationObjectList=nil
		@AnimationObjectList=Array.new
		Sketchup.active_model.entities.each{|ent| 
			if(ent.class==Sketchup::Group ||ent.class==Sketchup::ComponentInstance)						
				if(ent.attribute_dictionaries!=nil && ent.attribute_dictionaries["SRPAnimation"]!=nil)
					@AnimationObjectList.push(ent)	#used in update to lookup object.
				end
			end
		}
	end
	
	def start
		@bPaused=false
		@bStopped=false
		camera = Sketchup.active_model.active_view.camera
		@cameraRestore=Sketchup::Camera.new camera.eye, camera.target, camera.up
		setCameraToPage(Sketchup.active_model.pages.selected_page)
	end
	
	# Start the animation
	def play
		@bPaused=!@bPaused
		if(!@bPaused and @bStopped==true)
			start
		end
	end

	def pause
		@bPaused=true
	end
	def isPaused
		return @bPaused
	end

	def reverse
		@animationRate=-@animationRate
	end
		
	def rewind
	

		@bPaused=true;
		
		setCameraToPage(Sketchup.active_model.pages.selected_page)

		cameraPreFrame()
		setFrame(0)
		updateCamera()
		@bStopped=true
		if(@cameraRestore)
			Sketchup.active_model.active_view.camera=@cameraRestore
		end
		@AnimationObjectList=nil
	end
	
	def setFrame(frameNumber)
		@frame=frameNumber

		#if needed find objects.
		if(@AnimationObjectList==nil)
			findAnimationObjects()
		end
		
		@AnimationObjectList.each_index {|ci| 
			dict=@AnimationObjectList[ci].attribute_dictionaries["SRPAnimation"]
			str=dict[frameNumber.to_s];
			
			if(str!=nil)

				str=str.unpack('u*').to_s

				str.gsub!("whank","\n")

				mask=str.unpack('L')[0]
				if(mask!=0)
					arr=str.unpack('xxxxE*')
					matrix=@AnimationObjectList[ci].transformation.to_a
					16.downto(0){|i|
						if(mask[i]!=0)
							matrix[i]=arr.pop	
						end
						}
					xform=Geom::Transformation.new(matrix)
					if(xform!=nil)
						if(frameNumber==0)
							#puts xform.origin.to_a
						end
						@AnimationObjectList[ci].transformation=xform
					end
					
				end

			end
			}
	end
	
	def nextFrame(view)
		if(!@bPaused)
			cameraPreFrame()
			setFrame(@frame+@animationRate)
			updateCamera()
			Sketchup::set_status_text "Frame="+@frame.to_s
		end

		view.show_frame
	    return true
	end





	
	@cameraParent=nil
	@cameraTarget=nil
	@cameraType=nil		#type=fixed,relative,drag
	def findComponentNamed(name)
		if(name==nil)
			return nil
		end
		Sketchup.active_model.definitions.each{|cd| 
			cd.instances.each{|ci|					
				if(ci.name.casecmp(name) == 0)
					return ci
				end
			} 	
		}
		return nil
	end	



	def findPageNamed(name)
		if(name!=nil)
			Sketchup.active_model.pages.each{|p|
				if(p.name.casecmp(name) == 0)
					return p
				end
			}
		end
		return nil
	end	
		

	def setCameraToPage(page)
		@cameraParent=nil
		@cameraTarget=nil
		@cameraType=nil
		@cameraNextPage=nil
		@cameraFrameEnd=nil
			
        if(page==nil)
            return
        end
        
		#Sketchup.active_model.pages.selected_page.description.downcase.gsub(/ /,"").split(";")
		paramArray=page.description.downcase.gsub(/ /,"").split(";")
		params=Hash[*paramArray.collect { |v| 
			[v.split("=")[0], v.split("=")[1]]
		}.flatten]
	
		#if series
		#find right page in series
		#set transition frame and next page
		@cameraParent=findComponentNamed(params["parent"])
		@cameraTarget=findComponentNamed(params["target"])
		@cameraType=params["type"]
		@cameraNextPage=findPageNamed(params["nextpage"])

		#defaults to first (0) and last frame in animation
		@cameraEndFrame=params["endframe"]
		@cameraStartFrame=params["startFrame"]
		
		
		if(params["setframe"]!=nil)
			@frame=params["setframe"].to_i
		end
		if(params["pauseframe"]!=nil)
			@pauseFrame=params["pauseframe"].to_i
		else
			@pauseFrame=nil
		end
		if(params["animationrate"]!=nil)
			@animationRate=params["animationrate"].to_i
		else
			@animationRate=2
		end
		#print @cameraNextPage,",",@cameraEndFrame.to_i
		Sketchup.active_model.active_view.camera=page.camera
		
	end
	
	

	def findCameras()
		@cameraEntity=nil
		@cameraTargetEntity=nil
		@cameraPreMoveOffset=nil
		begin
			params=Sketchup.active_model.pages.selected_page.description.downcase.split(';')
			if(pageDesc.include? "parent=")
				pageDesc.chomp!
				targetname=pageDesc.split("=")[1]
				Sketchup.active_model.entities.each{|ent| 
					if(ent.typename.downcase == "componentinstance" and ent.name.downcase == targetname )
						@cameraTargetEntity=ent
						camera = Sketchup.active_model.active_view.camera
					end
					}
			end
		rescue
		end
	end

	def cameraPreFrame
		if(@cameraEndFrame!=nil and @frame>=@cameraEndFrame.to_i and @cameraNextPage!=nil)
			setCameraToPage(@cameraNextPage)
		end
			
		if(@pauseFrame!=nil and @frame>=@pauseFrame.to_i)
			pause
		end
			
		if(@animationRate<0 and @frame<0)
			pause
			reverse
		end
		
		if(@cameraParent!=nil)
#			@cameraPreMoveOffset=Sketchup.active_model.active_view.camera.eye-@cameraParent.transformation.origin;
			@cameraPreMoveOffset=Sketchup.active_model.active_view.camera.eye-@cameraParent.bounds.center;
		end
	end
	def updateCamera()

#Sketchup.active_model.selection.first.curve.vertices.each{|v| print v.position}
#Sketchup.active_model.selection.first.curve.vertices[curVert].each{|v| print v.position}

		def calcPointAlongCurve (curve,percent)
			curve=Sketchup.active_model.selection.first.curve
			totalLength=0
			curve.edges.each{|e| 
				totalLength+=e.length
			}

			dist=(1.0/totalLength)*percent
			curve.edges.each{|e| 
				dist=dist-e.length
				if dist<0
					return e.line[0]+(e.line[1].length=(e.length-dist))
				end
			}
		end



		camera = Sketchup.active_model.active_view.camera
		if(@cameraParent!=nil)
			#if(@cameraParent.description.downcase.include?("animationpath"))
			#	dest=calcPointAlongCurve(@cameraPath,1.0/(frameEnd-frameStart))+@cameraPreMoveOffset
			#else			
				#dest=@cameraParent.transformation.origin+@cameraPreMoveOffset
				dest=@cameraParent.bounds.center+@cameraPreMoveOffset
			#end
			camera.set(dest, dest+camera.direction, Geom::Vector3d.new(0, 0, 1))
		end
		if(@cameraTarget!=nil)
			target=@cameraTarget
			camera.set(camera.eye, @cameraTarget.bounds.center, Geom::Vector3d.new(0, 0, 1))
		end
	end

	
	# The stop method will be called when SketchUp wants an animation to stop
	# this method is optional.
	def stop
	end

##################################################start of tool################################################

	# The activate method is called when a tool is first activated.  It is not
	# required, but it is a good place to initialize stuff.
	def activate
	end

	def onLButtonDown(flags, x, y, view)
	end

	# onLButtonUp is called when the user releases the left mouse button
	def onLButtonUp(flags, x, y, view)
	end

	# draw is optional.  It is called on the active tool whenever SketchUp
	# needs to update the screen.
	#def draw(view)
	#end
	#def onSetCursor()
	#end

##################################################end of tool################################################



end #class SketchyReplay

def createSketchReplayToolbar()

##################
return;

    toolbar = UI::Toolbar.new "Sketchy Replay"

    replaycmd = UI::Command.new("Play") { 
        animation=
        if @@replayAnimation==nil
            @@replayAnimation=SketchyReplay.new
        end
            @@replayAnimation.play
        if(Sketchup.active_model.active_view.animation=@@replayAnimation and @@replayAnimation.isPaused )
            Sketchup.active_model.active_view.animation = nil
        end
        }
        
    path = Sketchup.find_support_file "SketchyReplay-PlayPauseButton.png", "plugins\\SketchyPhysics\\Images\\"

    replaycmd.small_icon = path
    replaycmd.large_icon = path
        
    replaycmd.tooltip = "Play animation"
    replaycmd.menu_text = "Play"
    toolbar.add_item replaycmd

    cmd = UI::Command.new("Rewind") { 
    #@@replayAnimation.setFrame(1)
    #return;
        @@replayAnimation.rewind
        Sketchup.active_model.active_view.animation = nil
        }

    path = Sketchup.find_support_file "SketchyReplay-RewindButton.png", "plugins\\SketchyPhysics\\Images\\"

    cmd.small_icon = path
    cmd.large_icon = path
    cmd.tooltip = "Rewind to first frame"
    cmd.status_bar_text = "Rewind to first frame"
    cmd.menu_text = "Rewind"
    toolbar.add_item cmd

    cmd = UI::Command.new("Reverse") { 
        @@replayAnimation.reverse
        }
        path = Sketchup.find_support_file "SketchyReplay-ReverseButton.png", "plugins\\SketchyPhysics\\Images\\"

    cmd.small_icon = path
    cmd.large_icon = path

    cmd.tooltip = "Reverse"
    cmd.status_bar_text = "Reverse"
    cmd.menu_text = "Reverse"
    toolbar.add_item cmd



    #~ cmd = UI::Command.new("ClearAnimation") { 
        #~ #@@replayAnimation.rewind
        #~ @@replayAnimation.clearAllAnimation
        #~ Sketchup.active_model.active_view.animation = nil
        #~ }
        
    #~ path = Sketchup.find_support_file "SketchyReplay-ClearAnimationButton.png", "plugins\\SketchyPhysics\\Images\\"

    #~ cmd.small_icon = path
    #~ cmd.large_icon = path	
    #~ cmd.tooltip = "Clear all animation data"
    #~ cmd.status_bar_text = "Clear all animation data"
    #~ cmd.menu_text = "ClearAnimation"
    #~ toolbar.add_item cmd

    toolbar.show


end



#-----------------------------------------------------------------------------
# Add an physics menu items to the Tools menu
if( not file_loaded?("SketchyReplay.rb") )
    #add_separator_to_menu("Help")
    #SketchyReplayClient_menu = UI.menu("Help")
    #SketchyReplayClient_menu.add_item("About SketchyReplay") {UI.messagebox "Version V01.\nWritten by C Phillips"}
end

@@replayAnimation=nil
createSketchReplayToolbar()

file_loaded("SketchyReplay.rb")

