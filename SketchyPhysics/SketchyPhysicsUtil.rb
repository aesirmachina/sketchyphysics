require 'sketchup.rb'

def PickGroupEmbedded(x,y,view)
	ph=view.pick_helper
	num=ph.do_pick x,y
	
	item=nil
	
	path=ph.path_at(1)
    
    return item if (path==nil)
    
	#puts path
	path.length.downto(0){|i|
		if(path[i].class==Sketchup::Group && 
                (path[i].parent==Sketchup.active_model || (path[i].get_attribute("SPJOINT","name",nil)!=nil)))
			item=path[i]
			#puts "ParentGroup="+item.to_s
			break
		end
	}
	
	return item;
	
	puts "element_at 1"+ph.element_at(1).to_s
	topGroup=ph.element_at(1)
	if(topGroup.class==Sketchup::Group)
		puts "topGroup xform:"+topGroup.transformation.origin.to_s
	
		xform=ph.transformation_at(1)
		puts "xform at 1:"+xform.origin.to_s
		
		xform=xform*topGroup.transformation.inverse
		puts "xform 1 inverse:"+xform.origin.to_s
		
		face=ph.picked_face
		if(face.parent.class==Sketchup::ComponentDefinition)
			face.parent.instances.each{|ci|
				puts ci.transformation.origin
				if(ci.transformation.origin==xform.origin)
					#Sketchup.active_model.selection.clear
					#Sketchup.active_model.selection.add ci
					puts "Found" + ci.to_s
					return(ci)
				end
			
			}
		end
	end		
	puts "Found topGroup " + topGroup.to_s
	return topGroup
end

class PhysQueryTool

	def initialize()
	end

	def activate
		Sketchup::set_status_text("Select joint parent object", SB_PROMPT)
		@ip = Sketchup::InputPoint.new
		@iptemp = Sketchup::InputPoint.new
		@displayed = false
    
	end
	def draw(view)
		if( @ip.valid? && @ip.display? )
			@ip.draw view
			@displayed = true
		else
			@displayed = false
		end
	end
	
	def onMouseMove(flags, x, y, view)
		@iptemp.pick view, x, y
		if( @iptemp.valid? )
			changed = @iptemp != @ip
			@ip.copy! @iptemp
			pos = @ip.position;
	        
			# get the text for the position
			msg = @ip.tooltip
	        
			# set the tooltip to show this message
			view.tooltip = msg
	        
			# see if we need to update the display for this point
			if( changed and (@ip.display? or @displayed) )
				# This tells the view that we want it to update itself
				view.invalidate
			end
		end
	    
	end

	def onLButtonDown(flags,x,y,view)

		ip = view.inputpoint x, y
		puts ip.tooltip
		puts "depth "+ip.depth.to_s
		
		ph=view.pick_helper
		num=ph.do_pick x,y

		puts ph.best_picked.to_s
			
	
		#puts "depth at 1 = "+ph.depth_at(1).to_s
		path=ph.path_at(1)
		puts path
		path.length.downto(0){|i|
			puts path[i]
			if(path[i].class==Sketchup::Group)
				item=path[i]
				puts "ParentGroup="+item.to_s
				break
			end
		}
		

		puts "element_at 1"+ph.element_at(1).to_s
		puts "element_at 2"+ph.element_at(2).to_s
		puts "element_at 3"+ph.element_at(3).to_s
		puts "element_at 4"+ph.element_at(4).to_s
		topGroup=ph.element_at(1)
		puts "topGroup="+topGroup.to_s
		if(topGroup.class==Sketchup::Group)
			puts "topGroup xform:"+topGroup.transformation.origin.to_s
		
			xform=ph.transformation_at(1)
			puts "xform at 1:"+xform.origin.to_s
			
			xform=xform*topGroup.transformation.inverse
			puts "xform 1 inverse:"+xform.origin.to_s
			
			face=ph.picked_face
			puts face.parent.to_s
			puts "instances[0]" +face.parent.instances[0].to_s
			
			if(face.parent.class==Sketchup::ComponentDefinition)
				face.parent.instances.each{|ci|
					puts ci.to_s
					puts ci.transformation.origin
					if(ci.transformation.origin==xform.origin)
						Sketchup.active_model.selection.clear
						Sketchup.active_model.selection.add ci
						puts "Found" + ci.to_s
					end
				
				}
			end
		end		
		return
		
		#puts "--------"
		#puts ph.all_picked
		#puts "------------"
		
						
		item= ip.face
		puts item.to_s
		while(item.class!=Sketchup::Model && item.parent!=nil)
			if(item.class==Sketchup::ComponentDefinition)
				puts item.group?
				item.instances.each{|ci|
					puts ci.transformation.origin
					if(ci.transformation.origin==xform.origin)
						Sketchup.active_model.selection.clear
						Sketchup.active_model.selection.add ci
						puts "Found" + ci.to_s
					end
				
				}
			end
			#puts item.class
			item=item.parent
		end
		puts item
		
		#Sketchup::set_status_text("Select joint parent object", SB_PROMPT)
		return
		
		ph=view.pick_helper
		num=ph.do_pick x,y
		puts "all"
		puts ph.all_picked
		puts "end"
		item=ph.best_picked
		puts "best"+item.to_s
		item=ph.picked_face
		puts "face"+item.to_s

		#item=ph.leaf_at 1
		#puts "leaf"+item.to_s
		while(item.class!=Sketchup::Model && item.class!=Sketchup::Group && item.parent!=nil)
			puts item.class
			item=item.parent
		end
		puts item
		#puts ph.depth_at 1
		#puts ph.depth_at 2
	end




	def deactivate(view)
		view.invalidate if @drawn
	end

	def onCancel(flag, view)
		self.reset(view)
	end

	def reset(view)
		Sketchup::set_status_text($exStrings.GetString("XXXX"), SB_PROMPT)
		if( view )
			view.tooltip = nil
			view.invalidate
		end
		
	end



end #class 



def makeComponentsFromGroups
	Sketchup.active_model.selection.each{|ent|  
		if(ent.class==Sketchup::Group) #for each selected group.
			#make component from group.



#			path=Sketchup.find_support_file "BedTraditional.skp" ,"Components/Furniture/"
#model = Sketchup.active_model
#definitions = model.definitions
#componentdefinition = definitions.load path
#component = definitions[0]

#model = Sketchup.active_model
#entities = model.active_entities
#group = entities.add_group

			ent.definition.description="sketchyphysics;type=mesh"
			CheckPhysicsObject(ent)
		end
	}

end

def makePhysicsSolid 
	Sketchup.active_model.selection.each{|ent|  
		if(ent.class==Sketchup::ComponentInstance) #for each selected component.
			ent.definition.description="sketchyphysics;type=mesh"
			CheckPhysicsObject(ent)
		end
	}
end



def setPhysicsParam(key,value)
	#child=blah
	#parent=blah
	#mass=123.45

end
def makePhysicsCollision(shape) 
	Sketchup.active_model.selection.each{|ent|  
		if(ent.class==Sketchup::ComponentInstance) #for each selected component.
			ent.definition.description="sketchyphysics;type=body;shape="+shape
			CheckPhysicsObject(ent)
		end
		if(ent.class==Sketchup::Group) #for each selected component.
			ent.set_attribute("SPOBJ", "shape", shape)
			#CheckPhysicsObject(ent)
		end
	}
end



def CheckPhysicsObject(ent)
	cd=ent.definition
	realBounds= Geom::BoundingBox.new
	cd.entities.each{|de| realBounds.add(de.bounds)} #Calculate the real bounding box of the entities in the component.
	center=Geom::Point3d.new(0,0,0)	#desired center. 
	if(realBounds.center!=center)	#if not already centered
			#transform all the entities to be around the new center
		cd.entities.transform_entities(Geom::Transformation.new( center-realBounds.center), cd.entities.to_a)
			#move each instance of this component to account for the entities moving inside the component.
		cd.instances.each{|ci|
            newCenter=realBounds.center.transform(ci.transformation)
            ci.transform!(newCenter)
            #ci.transform!(realBounds.center)
            }
	end
end
def IsValidContextMenuItem(selection)

	selection.each{|ent|
		if(ent.class==Sketchup::ComponentInstance ||ent.class==Sketchup::Group)
			return true
		end
	}
	
	return false
end
def togglePhysicsAttribute(selection,attrib,defaultValue) 
	selection.each{|ent|  
		if(ent.class==Sketchup::ComponentInstance || ent.class==Sketchup::Group) #for each selected component.
			bNewState=ent.get_attribute("SPOBJ", attrib, defaultValue)
			bNewState=!bNewState
			
			ent.set_attribute("SPOBJ", attrib, bNewState)
			#puts "Set "+ent.to_s+" "+attrib+"="+value.to_s
		end
	}
end
def setPhysicsAttribute(selection,attrib,value) 
	#puts "Set "+attrib+"="+value
	if(selection.class==Sketchup::ComponentInstance || selection.class==Sketchup::Group)#is it really a single group component
		selection.set_attribute("SPOBJ", attrib, value)
		#puts "Set "+selection.to_s+" "+attrib+"="+value.to_s
        if(attrib=="shape"&& value!=nil)
            selection.name=value  #change group name to be shape.
        end        
		return
	end
	
	selection.each{|ent|  
		if(ent.class==Sketchup::ComponentInstance || ent.class==Sketchup::Group) #for each selected component.
			ent.set_attribute("SPOBJ", attrib, value)
			#puts "Set "+ent.to_s+" "+attrib+"="+value.to_s
            if(attrib=="shape" && value!=nil)
                #puts("setting shape")
                ent.name=value  #change group name to be shape.
            end        
		end
	}
end
def validate_physicsAttribute(key,defaultValue)
	selection=Sketchup.active_model.selection
	#return MF_GRAYED if not selection.single_object? 
	if( selection.first.get_attribute("SPOBJ", key, defaultValue))
		return MF_CHECKED
	end
	return MF_UNCHECKED 
end
def validate_physicsAttributeString(key,value)
	selection=Sketchup.active_model.selection
	#return MF_GRAYED if not selection.single_object? 
	val=selection.first.get_attribute("SPOBJ", key, "")
	if( val==value )
		return MF_CHECKED
	end
	return MF_UNCHECKED 
end
def makePhysicsCube(parentEnts,width,height,depth)

	group=parentEnts.add_group
    pts = [[0,0,0], [width,0,0], [width,height,0], [0,height,0], [0,0,0]]
    pts = [[-(width/2),-(height/2),-(depth/2)], 
           [width/2,-(height/2),-(depth/2)], 
           [width/2,height/2,-(depth/2)], 
           [-(width/2),height/2,-(depth/2)], 
           [-(width/2),-(height/2),-(depth/2)]]
    base = group.entities.add_face pts
    depth = -depth if base.normal.dot(Z_AXIS) < 0.0
    base.pushpull depth
    setPhysicsAttribute(group,"shape","box")
    group.name="box"
    return(group)
end
def makePhysicsCylinder(radius,depth)
    
	group=Sketchup.active_model.active_entities.add_group 
    circle = group.entities.add_circle([0,0,0], [0,0,1], radius, 24)
    base = group.entities.add_face circle
    depth = -depth if base.normal.dot(Z_AXIS) < 0.0
    base.pushpull depth
	setPhysicsAttribute(group,"shape","cylinder")
    group.name="cylinder"
    return(group)
end
def createDefaultScene()

	Sketchup.active_model.start_operation "Create Default Physics Scene"
	checkModelUnits()
	cube=makePhysicsCube(Sketchup.active_model.active_entities,1000,1000,-20)
	cube.transform!(Geom::Transformation.new([0,0,-10]))
	setPhysicsAttribute(cube,"shape","staticmesh")#Change to static mesh.
    cube.set_attribute("SPOBJ", "staticmesh", true)
    cube.name="staticmesh"
    
	cube=makePhysicsCube(Sketchup.active_model.active_entities,12,12,12)
	cube.transform!(Geom::Transformation.new([-24,0,6]))

	cyl=makePhysicsCylinder(12/2,12)
	cyl.transform!(Geom::Transformation.new([-36,-14,0]))
    
    grp=create_cone(Sketchup.active_model.active_entities,6,12,10,0) #rad,hei,segs,taper
    grp.set_attribute( "SPOBJ", "shape", "cone")
    grp.transform!(Geom::Transformation.new([-24,-24,0], [0,0,1]))

	grp=create_sphere(6,12)#last number is seg count
    grp.set_attribute( "SPOBJ", "shape", "sphere")
    n=Geom::Vector3d.new([0,0,1])
    n.length=12/2
    center=Geom::Point3d.new([0,0,0])+n
    grp.transform!(Geom::Transformation.new(center, n))
    grp.transform!(Geom::Transformation.new([-24,-36,0], [0,0,1]))

	Sketchup.active_model.commit_operation
end


def disconnectJoint(child,jointName)
	puts "delete "+jointName
	puts Sketchup.active_model.selection.first.get_attribute("SPOBJ",jointName,nil)
	Sketchup.active_model.selection.first.delete_attribute("SPOBJ",jointName)
	puts Sketchup.active_model.selection.first.get_attribute("SPOBJ",jointName,nil)
end
def putsAllAttributes
    Sketchup.active_model.definitions.each{ |cd|
        cd.instances.each{|ci|
            puts ci.to_s+"={"
            if(ci.attribute_dictionaries!=nil)
                ci.attribute_dictionaries.each{|atd|
                puts atd.name+":"
                atd.each_pair { | key, value | puts key.to_s+"="+value.to_s }
                }
            puts "}"
            end
        }
    #get_attribute("SPOBJ",jointName,nil)
        #componentDef.attribute_dictionary("SPOBJ")
    }    
    
end

#~ def findJoint(jointName)
    #~ joints=[]
    #~ Sketchup.active_model.definitions.each{ |cd|
        #~ cd.instances.each{|ci|
            #~ jn=ci.get_attribute("SPJOINT","name",nil)
            #~ if(jn!=nil && jn==jointName)
                #~ joints.push(ci)
            #~ end
            #~ }
        #~ }
    #~ #puts joints
    #~ Sketchup.active_model.selection.add(joints)
    #~ return joints    
#~ end
#~ def disconnectAllJoints()
	#~ pn=0
	#~ while(Sketchup.active_model.selection.first.get_attribute("SPOBJ","jointParent"+pn.to_s,nil)!=nil)
		#~ #pname="jointParent"+pn.to_s
		#~ Sketchup.active_model.selection.first.delete_attribute("SPOBJ","jointParent"+pn.to_s)
		#~ pn=pn+1;
	#~ end
	#~ Sketchup.active_model.selection.first.set_attribute("SPOBJ","numParents",0)
#~ end
#~ def selectAllJoints()
	#~ count=0
    #~ ent=Sketchup.active_model.selection.first
    #~ np=0 #ent.get_attribute("SPOBJ","numParents",0)
    #~ Sketchup.active_model.selection.clear
	#~ while(np<10)
		#~ #pname="jointParent"+pn.to_s
        #~ jn=ent.get_attribute("SPOBJ","jointParent"+np.to_s)
        #~ if(jn!=nil)
            #~ findJoint(jn)
        #~ end
        #~ np=np+1
	#~ end
#~ end



def StartPhysQueryTool()
	tool=PhysQueryTool.new()
	Sketchup.active_model.select_tool tool
end
def purgeAllAnimation
    Sketchup.active_model.entities.each{|ent| 
        if(ent.attribute_dictionaries!=nil)
            ent.attribute_dictionaries.delete("xxanimationdictionary")
            ent.attribute_dictionaries.delete("animationdictionary")
            ent.attribute_dictionaries.delete("SRPAnimation")
        end
        }
end

def setDefaultPhysicsSettings()
    Sketchup.active_model.set_attribute( "SPSETTINGS", "defaultobjectdensity", "0.2")
    Sketchup.active_model.set_attribute( "SPSETTINGS", "worldscale", "9.0")
    Sketchup.active_model.set_attribute( "SPSETTINGS", "gravity", "1.0")
    Sketchup.active_model.set_attribute( "SPSETTINGS", "framerate", 3)
#Sketchup.active_model.set_attribute( "SPSETTINGS", "water", 1)
end
def editPhysicsSettings()
    dict=Sketchup.active_model.attribute_dictionary("SPSETTINGS")
    if(dict==nil)
        setDefaultPhysicsSettings()
        dict=Sketchup.active_model.attribute_dictionary("SPSETTINGS")
    end
    
        #get framerate or set default.
    @frameRate=Sketchup.active_model.set_attribute( "SPSETTINGS", "framerate", 
        Sketchup.active_model.get_attribute( "SPSETTINGS", "framerate", 3))
    
    prompts=dict.keys		
    values=dict.values		
    results = inputbox prompts, values,"Physics Settings"
    if (results != values && results != false)
        0.upto(prompts.length-1) do |xx|
           dict[prompts[xx]]=results[xx]
        end
    end
end
        
if( not file_loaded?("SketchyPhysicsUtil.rb") )
	submenu=UI.menu("Plugins")
#	submenu.add_separator
#	item =submenu.add_item("SketchyPhysics"){}
#	item = submenu.add_item("  Physics Settings") {editPhysicsSettings()}
#	item = submenu.add_item("  Create Default Scene") {createDefaultScene() }
	#item = submenu.add_item("  Purge embedded animation") {purgeAllAnimation() }
	#item = submenu.add_item("  Reload scripts") {reload() }
	
	#item = submenu.add_item("  Create Wheel") {}
	#item = submenu.add_item("  Create Door") {}

	#item = submenu.add_item("  PhysQueryTool") {	StartPhysQueryTool() }
	#submenu.add_separator		
			

	#submenu.set_validation_proc(item) {return MF_CHECKED }
	
	
	UI.add_context_menu_handler { |menu|
		selection=Sketchup.active_model.selection
		if(IsValidContextMenuItem(selection))
			#submenu=menu.add_submenu("SketchyPhysics")
			submenu=menu
			submenu.add_separator
			submenu.add_item("SketchyPhysics"){}
#			menu=submenu.add_submenu("SketchyPhysics")
            
            primList=["box","sphere","cylinder","cone","capsule","chamfer","convexhull","staticmesh"]
		
			if(!selection.single_object?)
				stateSubmenu=menu.add_submenu("Group State Change")
				item = stateSubmenu.add_item("Freeze") { setPhysicsAttribute(selection,"frozen",true) }
				item = stateSubmenu.add_item("UnFreeze") { setPhysicsAttribute(selection,"frozen",false) }
				item = stateSubmenu.add_item("Static") { setPhysicsAttribute(selection,"static",true) }
				item = stateSubmenu.add_item("Movable") { setPhysicsAttribute(selection,"static",false) }
				item = stateSubmenu.add_item("Ignore") { setPhysicsAttribute(selection,"ignore",true) }
				item = stateSubmenu.add_item("Clear Ignore flag") { setPhysicsAttribute(selection,"ignore",false) }
				#item = stateSubmenu.add_item("Embed geometry") { setPhysicsAttribute(selection,"embedgeometry",true) }
				#item = stateSubmenu.add_item("Do not embed geometry") { setPhysicsAttribute(selection,"embedgeometry",false) }
				
                shapeSubmenu=menu.add_submenu("Group Shape Change")
                item=shapeSubmenu.add_item("default") {setPhysicsAttribute(selection,"shape",nil)} 	
                primList.each{|prim|
					item=shapeSubmenu.add_item(prim) { 
                    	setPhysicsAttribute(selection,"shape",prim)
						} 	
						
					}		
					
			
			else
			#if(selection.single_object?)
				selected=selection.first

				tstr="  State:"
				if(selected.get_attribute("SPOBJ", "frozen", false))
					tstr+="Frozen "
				end
				if(selected.get_attribute("SPOBJ", "static", false))
					tstr+="Static "
				end
				if(selected.get_attribute("SPOBJ", "ignore", false))
					tstr+="Ignore "
				end
				if(selected.get_attribute("SPOBJ", "nocollide", false))
					tstr+="Nocollide "
				end
				stateSubmenu=menu.add_submenu(tstr)
				item = stateSubmenu.add_item("Frozen") { togglePhysicsAttribute(selection,"frozen",false) }
				stateSubmenu.set_validation_proc(item) {validate_physicsAttribute("frozen",false) }
				item = stateSubmenu.add_item("Static") { togglePhysicsAttribute(selection,"static",false) }
				stateSubmenu.set_validation_proc(item) {validate_physicsAttribute("static",false) }
				item = stateSubmenu.add_item("Ignore") { togglePhysicsAttribute(selection,"ignore",false) }
				stateSubmenu.set_validation_proc(item) {validate_physicsAttribute("ignore",false) }
				#item = stateSubmenu.add_item("NoCollision") { togglePhysicsAttribute(selection,"nocollide",false) }
				#stateSubmenu.set_validation_proc(item) {validate_physicsAttribute("nocollide",false) }
				#item = stateSubmenu.add_item("EmbedGeometry") { togglePhysicsAttribute(selection,"embedgeometry",false) }
				#stateSubmenu.set_validation_proc(item) {validate_physicsAttribute("embedgeometry",false) }
							
				
				shape=selected.get_attribute("SPOBJ", "shape", "default")
				#bFrozen=selection.first.get_attribute("SPOBJ", "frozen", false)
				shapeSubmenu=submenu.add_submenu("  Shape:"+shape)
				item=shapeSubmenu.add_item("default") {setPhysicsAttribute(selection,"shape",nil)} 
				primList.each{|prim|
					item=shapeSubmenu.add_item(prim) { 
                    	setPhysicsAttribute(selection,"shape",prim)
						} 	
						shapeSubmenu.set_validation_proc(item) {validate_physicsAttributeString("shape",prim) }
					}		
					
				#~ parentSubmenu=submenu.add_submenu("  Joint connections:"+selected.get_attribute("SPOBJ","numParents",0).to_s)
				#~ item = parentSubmenu.add_item("Set New Parent Joint") {selectParentJoint(selected)} 

				#~ item = parentSubmenu.add_item("Disconnect All") { disconnectAllJoints() }
				#~ item = parentSubmenu.add_item("Select all joints") { selectAllJoints() }
				
				#~ pn=0
				#~ while(selected.get_attribute("SPOBJ","jointParent"+pn.to_s,nil)!=nil)
					#~ pname="jointParent"+pn.to_s
					#~ #type = selected.get_attribute(jname,"type",nil)
					#~ jsm = parentSubmenu.add_submenu("Parent:"+selected.get_attribute("SPOBJ",pname,nil).to_s)
					#~ item = jsm.add_item("Select Joint") {} 
					#~ #puts pn
					#~ item = jsm.add_item("Disconnect All") { disconnectAllJoints() }
					#~ pn=pn+1
				#~ end			
			end			
			#--------------debug
			submenu=menu.add_submenu("  Debug")
			
			item = submenu.add_item("Readback Collision Geometry") { togglePhysicsAttribute(selection,"showgeometry",false) }
			submenu.set_validation_proc(item) {validate_physicsAttribute("showgeometry",false) }
		end			
		}
end
file_loaded("SketchyPhysicsUtil.rb")


