
$LOAD_PATH[0,0]=Sketchup.find_support_file("plugins")+"/SketchyPhysics/"
require 'Win32API' #needed to call the user32.dll
$LOAD_PATH.delete_at(0)

$win32GetKeyStateFunc=Win32API.new("user32.dll", "GetKeyState", ['N'], 'N')
Win32_VK_LSHIFT=0xa0
Win32_VK_LCONTROL=0xa2
#win32GetKeyState(Win32_VK_LSHIFT)
#win32GetKeyState(Win32_VK_LCONTROL)
def win32GetKeyState(key)
        #
      return (($win32GetKeyStateFunc.call(key)>>16)!=0)
end

def clearConsole
		#setup the win32 calls
	findWindow = Win32API.new("user32.dll", "FindWindow", ['P','P'], 'N')
	findWindowEx = Win32API.new("user32.dll", "FindWindowEx", ['N','N','P','P'], 'N')
	sendMessage = Win32API.new("user32.dll", "SendMessage", ['N','N','N','P'], 'N')

		#find the ruby console
	pw=findWindow.call(0,"Ruby Console")
		
		#get the first child control. Its the text input control.
	h=findWindowEx.call(pw,0,"Edit",0) 
	
		#get the second child control. its the ruby output control. 
	h=findWindowEx.call(pw,h,"Edit",0) 
	
		#send the control a "WM_SETTEXT" message (0x000C) with an empty string.
	sendMessage.call(h,0x000C,0,"")			
end

def closeConsole
		#setup the win32 calls
	findWindow = Win32API.new("user32.dll", "FindWindow", ['P','P'], 'N')
	sendMessage = Win32API.new("user32.dll", "SendMessage", ['N','N','N','P'], 'N')

		#find the ruby console
	pw=findWindow.call(0,"Ruby Console")

        #send the control a "WM_CLOSE" message (0x0010) 
	sendMessage.call(pw,0x0010,0,"")			
end

def moveConsole(x,y,w,h)

        #setup the win32 calls
    findWindow = Win32API.new("user32.dll", "FindWindow", ['P','P'], 'N')
    setWindowPos= Win32API.new("user32.dll", "SetWindowPos",['P','P','N','N','N','N','N'], 'N')

        #find the ruby console
    pw=findWindow.call(0,"Ruby Console")

    setWindowPos.call(pw,0,x,y,w,h,0);
end 

def getConsoleLocation
    findWindow = Win32API.new("user32.dll", "FindWindow", ['P','P'], 'N')
    getWindowRect= Win32API.new("user32.dll", "GetWindowRect",['P','PP'], 'N')
    pw=findWindow.call(0,"Ruby Console")
    
        #create a char buffer large enough for the rectangle(4 ints)
    rect=Array.new.fill(0.chr,0..4*4).join
    getWindowRect.call(pw,rect);
        #turn char buffer into an array of ints.
    rect=rect.unpack("i*")

    #turn rectangle into x,y,w,h
    rect[2]=rect[2]-rect[0] #w=x2-x1
    rect[3]=rect[3]-rect[1] #h=y2-y1
    return rect
end


