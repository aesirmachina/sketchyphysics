require 'sketchup.rb'
require 'Win32API'



#this changes the definition of ComponentInstance 
#by adding an entities method that referers to the definition. 
#It vastly simplifies the script but it may be "bad" overall. 
#Can I somehow localize it to my library?
class Sketchup::ComponentInstance
	def entities # make it so you can use CompontentInstance.entites same as Group.entites.
		return(definition.entities)
	end
end

class Sketchup::Group
	def definition # make it so you can use group.definition
		return(entities[0].parent)
	end
end
$UniqueObjects=Hash.new()
def registerUniqueObject(id,ent)
    $UniqueObjects[id]=ent  
    
end




def updateUniqueObjects()
    puts "updateUniqueObjects"
    Sketchup.active_model.definitions.each{ |cd|
        cd.instances.each{|ci|
            id=ci.get_attribute("__uniqueid","id",nil)
            if(id!=nil)
                $UniqueObjects[id]=ci 
            end
        }
    }   
end
def findUniqueObject(id)
    ent=$UniqueObjects[id]

    if(ent==nil)
        #puts("failed to find:"+id.to_s)
        updateUniqueObjects()
        ent=$UniqueObjects[id]
    end
       
    return ent
end


class Sketchup::Group
	def getUniqueID #return a (hopefully) unique and perm ID for a given group.
        id=get_attribute("__uniqueid","id",nil)
        if(id==nil)
            id=rand()
            set_attribute("__uniqueid","id",id)
            registerUniqueObject(id,self)
            findUniqueObject(id)
        end
        return id 
	end
end
def extractScaleFromGroup(group)
    Geom::Transformation.scaling( 
        (Geom::Vector3d.new(1.0,0,0).transform!(group.transformation)).length, 
        (Geom::Vector3d.new(0,1.0,0).transform!(group.transformation)).length, 
        (Geom::Vector3d.new(0,0,1.0).transform!(group.transformation)).length
        )
end

def findParentInstance(ci)
    ci.definition.instances.each{|di|
        if(ci==di)
            return(di)
        end
    }
    return nil;
end


class SketchyPhysicsClient
     def resetAxis(ent)
        cd=ent.definition
        realBounds= Geom::BoundingBox.new
        cd.entities.each{|de| realBounds.add(de.bounds)} #Calculate the real bounding box of the entities in the component.
        center=Geom::Point3d.new(0,0,0)	#desired center. 
        if(realBounds.center!=center)	#if not already centered
                #transform all the entities to be around the new center
            cd.entities.transform_entities(Geom::Transformation.new( center-realBounds.center), cd.entities.to_a)
                #move each instance of this component to account for the entities moving inside the component.
            cd.instances.each{|ci|
                #ci.transform!(realBounds.center);
                #newXform=(ci.transformation.inverse*Geom::Transformation.new(realBounds.center))*ci.transformation
                #ci.transform!(newXform)

                newCenter=realBounds.center.transform(ci.transformation)
                ci.transform!(newCenter-ci.transformation.origin)
            }
        end
    end
    def copyBody(body)
        
        #@bulletGroup=@pickedBody
        #~ if(@bulletGroup==nil)
            #~ grp=create_sphere(radius,12)
            #~ grp.set_attribute( "SPOBJ", "shape", "sphere")
            #~ @bulletGroup=grp
        #~ else
            #~ grp=@bulletGroup.copy
        #~ end
        
        grp=body.copy
        grp.material=body.material
#puts @pickedBody.get_attribute( "SPOBJ", "shape", nil)
        grp.set_attribute( "SPOBJ", "shape",body.get_attribute( "SPOBJ", "shape", nil))
        #grp.transformation=(Geom::Transformation.new(position, [0.0,0.0,1.0]))
                
        collisions=dumpGroupCollision(grp,0,extractScaleFromGroup(grp)*(grp.transformation.inverse))
        #collisions=dumpGroupCollision(ent,0,extractScaleFromGroup(ent))
        if(!collisions.empty?)
            body=createBodyFromCollision(grp,collisions);
            #kick=[10-rand(20),10-rand(20),15.0]
            kick=[0,0,15.0]
            @newtonAddImpluse.Call(body,kick.to_a.pack("f*"),grp.transformation.origin.to_a.pack("f*"))
        end

    end
    def createBullet(position,direction,radius,speed)
        
        #@bulletGroup=@pickedBody
        #~ if(@bulletGroup==nil)
            #~ grp=create_sphere(radius,12)
            #~ grp.set_attribute( "SPOBJ", "shape", "sphere")
            #~ @bulletGroup=grp
        #~ else
            #~ grp=@bulletGroup.copy
        #~ end
        
        grp=@pickedBody.copy
        grp.material=@pickedBody.material
puts @pickedBody.get_attribute( "SPOBJ", "shape", nil)
        grp.set_attribute( "SPOBJ", "shape",@pickedBody.get_attribute( "SPOBJ", "shape", nil))
        #grp.transformation=(Geom::Transformation.new(position, [0.0,0.0,1.0]))
                
        collisions=dumpGroupCollision(grp,0,extractScaleFromGroup(grp)*(grp.transformation.inverse))
        #collisions=dumpGroupCollision(ent,0,extractScaleFromGroup(ent))
        if(!collisions.empty?)
            body=createBodyFromCollision(grp,collisions);
            #kick=[10-rand(20),10-rand(20),15.0]
            kick=[0,0,15.0]
            @newtonAddImpluse.Call(body,kick.to_a.pack("f*"),grp.transformation.origin.to_a.pack("f*"))
        end

    end
    def dumpCollision
        Sketchup.active_model.entities.each { | ent | 
            if (ent.class== Sketchup::Group ||ent.class== Sketchup::ComponentInstance)

                if(ent.attribute_dictionary("SPWATERPLANE")!=nil)
                    density=ent.get_attribute("SPWATERPLANE","density",1.0)
                    linearViscosity=ent.get_attribute("SPWATERPLANE","linearViscosity",1.0)
                    angularViscosity=ent.get_attribute("SPWATERPLANE","angularViscosity",1.0)
                    xform=ent.transformation
                    plane=Geom.fit_plane_to_points(xform.origin,xform.origin+xform.xaxis,xform.origin+xform.yaxis)
                    @newtonSetupBouyancy.Call(plane.pack("f*"),0, #[0.0,1.0,-9.8].pack("f*"),
                            [density,linearViscosity,angularViscosity].pack("f*"))
                end

                #ent.transform!(Geom::Point3d.new([1,0,0]))
                #ent.transform!(Geom::Point3d.new([-1,0,0]))
                if(!ent.get_attribute("SPOBJ", "ignore", false))
                    #    break
                    #end

                    resetAxis(ent)
                    #collisions=dumpGroupCollision(ent,0,Geom::Transformation.new())
                    collisions=dumpGroupCollision(ent,0,extractScaleFromGroup(ent)*(ent.transformation.inverse))
                    #collisions=dumpGroupCollision(ent,0,extractScaleFromGroup(ent))
                    if(!collisions.empty?)
                        createBodyFromCollision(ent,collisions);
                    end
                end
            end
            }
    end

    def createBodyFromCollision(group,collisions)
        id=@DynamicObjectList.length;
       
        if(!group.get_attribute( "SPOBJ", "static", false))
            bDynamic=1
        else
            bDynamic=0
        end

        boxSize=group.definition.bounds.max-group.definition.bounds.min
        collisions.flatten!
       
        xform=group.transformation
                   #figure out the scaling of the xform.
        scale=[ (Geom::Vector3d.new(1.0,0,0).transform!(xform)).length,
                (Geom::Vector3d.new(0,1.0,0).transform!(xform)).length,
                (Geom::Vector3d.new(0,0,1.0).transform!(xform)).length]

            #find the real size of the bounding box and offset to center.
        bb=group.definition.bounds
        size=[bb.width*scale[0],bb.height*scale[1],bb.depth*scale[2]]
        
        #center=[bb.center.x*scale[0],bb.center.y*scale[1],bb.center.z*scale[2]]

        #puts center
        #center= Geom::Transformation.new(center)        
        #noscale=Geom::Transformation.new(xform.xaxis,xform.yaxis,xform.zaxis,xform.origin)
        #finalXform=noscale*center
       
       
        tt=Geom::Transformation.new()
        body=@newtonCreateBody.Call(id,collisions.pack('L*'),
            collisions.length,bDynamic, size.pack('f*'),
            group.transformation.to_a.pack('f*')
            #tt.to_a.pack('f*')
            
            )

            #save body in obj for later reference.
        group.set_attribute("SPOBJ", "body",body)
        group.set_attribute("SPOBJ", "savedScale", scale)
        
        if(group.get_attribute( "SPOBJ", "showgeometry", false))
        	ShowCollision(group,body)
        	group.set_attribute( "SPOBJ", "showgeometry",false)
        end	
        UpdateEmbeddedGeometry(group,body)
                
        jnames=JointConnectionTool.getParentJointNames(group)
        if(jnames.length>0)
            @AllJointChildren.push(group)
        end
            #set freeze if needed	
        if(group.get_attribute( "SPOBJ", "frozen", false))
            @newtonBodySetFreeze.Call(body,1)
        else
            @newtonBodySetFreeze.Call(body,0)
        end
             
        boxCenter=group.definition.bounds.center.transform( extractScaleFromGroup(group))            
        @newtonSetBodyCenterOfMass.Call(body,boxCenter.to_a.pack('f*'))
        @DynamicObjectList.push(group)	#used in update to lookup object.
        @DynamicObjectResetPositions.push(group.transformation) #save position of all objects for reset.
        @DynamicObjectBodyRef.push(body) #save position of all objects for reset.
        return body
    end

    def dumpGroupCollision(group, depth,parentXform)

        if(group.get_attribute("SPOBJ", "ignore", false))
            return []
        end
        
        xform=parentXform*group.transformation
      
        #primList=["box","sphere","cylinder","cone","capsule","chamfer","convexhull","staticmesh"]
        shape = group.get_attribute( "SPOBJ", "shape", nil)
        
        if(shape=="wheel")
            #hide this
            #create a chamfer at root and transform so it best matches bbox.
            #figure out best fit
            #add hinge
            #set parent to this body.
            #return[]
        end
        


        if(shape=="staticmesh" || group.get_attribute("SPOBJ", "staticmesh", false))
            #puts "   "*depth+"staticmesh"
            return [createStaticMeshCollision(group)]
            #return["   "*depth+"staticmesh"]
        end
        
        if(shape==nil || shape=="compound")
            #groupCollisions=["   "*depth+"<compound>"]
            groupCollisions=[]
            group.entities.each { | ent | 
                if (ent.class== Sketchup::Group ||ent.class== Sketchup::ComponentInstance)
                    depth=depth+1
                    groupCollisions.push( dumpGroupCollision(ent,depth,xform) )
                    depth=depth-1
                end
            }
            if(groupCollisions.length>1)
                return groupCollisions
            end
            
        end
     
        if(shape==nil) # if still nil make this it default shape.
            shape="box"
        end        
            
            #figure out the scaling of the xform.
        scale=[ (Geom::Vector3d.new(1.0,0,0).transform!(xform)).length,
                (Geom::Vector3d.new(0,1.0,0).transform!(xform)).length,
                (Geom::Vector3d.new(0,0,1.0).transform!(xform)).length]

            #find the real size of the bounding box and offset to center.
        bb=group.definition.bounds
        size=[bb.width*scale[0],bb.height*scale[1],bb.depth*scale[2]]
        center=[bb.center.x*scale[0],bb.center.y*scale[1],bb.center.z*scale[2]]

        #puts center
        center= Geom::Transformation.new(center)        
        noscale=Geom::Transformation.new(xform.xaxis,xform.yaxis,xform.zaxis,xform.origin)
        finalXform=noscale*center
        
        verts=[]
        if( shape=="convexhull")
            verts=createConvexHullVerts(group)   #fill with convex hull verts
            finalXform=Geom::Transformation.new()
            finalXform=xform;
            if(verts.length<4)
                return[]; #empty. fixes a bug.
            end
        end
#puts size
#puts finalXform.to_a().inspect
        @AllCollisionEntities.push(group)
        col=@newtonCreateCollision.Call(shape,size.pack('f*'),finalXform.to_a.pack('f*'),
                verts.to_a.pack('f*')) #if convexhull
            
        #puts "   "*depth+shape+col.to_s

        return [col]
        
    end

    def createConvexHullVerts(group)
        verts=[0]#0 is placeholder for vert count.
    #verts[0]=0	
        group.entities.each{|ent|
            if(ent.typename == "Face")
                ent.vertices.each{|v|
                    verts=verts+v.position.to_a
                }
            end
        }
        verts[0]=(verts.length-1)/3
        return verts

    end
    def createStaticMeshCollision(group)
        tris=[]
        group.entities.each{|ent|
            if(ent.typename == "Face")
                ent.mesh.polygons.each_index{|pi|
                    pts=ent.mesh.polygon_points_at( pi+1 ).each{|pt|
                        tris=tris+pt.to_a
                        }
                    }
            end
        }
        return @newtonCreateCollisionMesh.Call(tris.to_a.pack('f*'),tris.length/9) 
        
    end

	def ShowCollision (group,body,bEmbed=false)
			if(@collisionBuffer==nil)
					@collisionBuffer=Array.new.fill(255.chr,0..400*1024).join
			end
			dat=@collisionBuffer.clone
            

			faceCount=@newtonGetBodyCollision.Call(body,dat,dat.length)
			#puts "CollisionBuffer face count "+faceCount.to_s
			if(bEmbed)
				colGroup=group.entities.add_group	
			else        
				colGroup=Sketchup.active_model.entities.add_group	
			end
			colGroup.set_attribute("SPOBJ", "ignore", true)
			colGroup.set_attribute("SPOBJ", "EmbeddedGeometryObject", true)

			0.upto(faceCount-1) { |i|
				vertCount=(dat.slice!(0,4).unpack("f")[0])

				#puts "vertCount "+vertCount.to_s
				#puts "CollisionBuffer vertCount "+vertCount.to_s	
				colBuffer=dat.slice!(0,(4*vertCount)).unpack("f"+vertCount.to_s)
				
				verts=[]
				vi=0;
				while (vi<vertCount)	
					verts.push(Geom::Point3d.new(colBuffer[vi]*9,colBuffer[vi+1]*9,colBuffer[vi+2]*9))
					vi+=3;
				end
				#colGroup.entities.add_face(verts[0],verts[1],verts[2])
				#puts verts.to_s
				1.upto(verts.length-1)	{|vi|
					colGroup.entities.add_line(verts[vi-1],verts[vi])
				}
			}
            #colGroup.transform!(group.transformation.inverse)
            #colGroup.transform!(group.transformation)
			if(bEmbed)
				colGroup.transform!(group.transformation.inverse)
			end
			
			return (colGroup)
	end
	def UpdateEmbeddedGeometry(group,body)

		if(group.class==Sketchup::Group)
			cd=group.entities[0].parent
		else
			cd=group.definition
		end

		cd.entities.each{ |e|
			if(e.get_attribute("SPOBJ", "EmbeddedGeometryObject", false))
				e.erase!
			end
		}
		if(group.get_attribute( "SPOBJ", "showcollision", false))
			ShowCollision(group,body,true)
            puts group
		end	

	end
	def FindGroupNamed(name)
		@DynamicObjectList.each_index{|di|
		if(@DynamicObjectList[di].name.downcase == name)
			return @DynamicObjectList[di]
		end
		}
		#puts "Didnt find body "+name
		return nil
	end	

	def FindBodyNamed(name)
		@DynamicObjectList.each_index{|di|
		if(@DynamicObjectList[di].name.downcase == name)
			return @DynamicObjectBodyRef[di]
		end
		}
		#puts "Didnt find body "+name
		return nil
	end	
	def FindEntityWithID(id)
		@AllCollisionEntities.each{|ent|
		#puts ent.entityID.to_s+"=="+id.to_s
		if(ent.entityID == id)
			return ent
		end
		}
		puts "Didnt find Entity "+id.to_s
		return nil
	end
	def FindBodyWithID(id)
		@DynamicObjectList.each_index{|di|
		#puts @DynamicObjectList[di].entityID.to_s+"=="+id.to_s
		
		if(@DynamicObjectList[di].entityID == id)
		
			return @DynamicObjectBodyRef[di]
		end
		}
		#puts "Didnt find body "+id.to_s
		return nil
	end
	def FindBodyFromInstance(componentInstance)
		@DynamicObjectList.each_index{|di|
		if(@DynamicObjectList[di] == componentInstance)
			return @DynamicObjectBodyRef[di]
		end
		}
		return nil
	end	
#oct 29
#3937 4003   
#19856 27201
#13858 17149
#8563  10835


	def initialize

        model = Sketchup.active_model
		@AllComponents=model.definitions

		view = model.active_view

		@frame = 0;
		@startTime = Time.now
		@@lastTime= Time.now
		@cameraTarget=nil


			#load and bind the newton wrapper dll
		dllName=Sketchup.find_support_file("NewtonServer2.dll","plugins\\SketchyPhysics\\")

		@newtonInit = Win32API.new(dllName, "init", [], 'V')
        @newtonStop = Win32API.new(dllName, "stop", [], 'V')
		@newtonUpdate = Win32API.new(dllName, "update", ['N'], 'V')
		@newtonFetchSingleUpdate = Win32API.new(dllName, "fetchSingleUpdate", ['PP'], 'N')
		@newtonFetchAllUpdates = Win32API.new(dllName, "fetchAllUpdates", [], 'P')
			
		@newtonRequestUpdate = Win32API.new(dllName, "requestUpdate", ['N'], 'V')
		@newtonCommand = Win32API.new(dllName, "NewtonCommand", ['P','P'], 'P')
		@newtonCreateCollision = Win32API.new(dllName, "CreateCollision", ['P','P','P','P'], 'N')
		@newtonCreateCollisionMesh = Win32API.new(dllName, "CreateCollisionMesh", ['P','N'], 'N')
		@newtonCreateBody = Win32API.new(dllName, "CreateBody", ['N','P','N','N','P','P'], 'N')
		@newtonCreateBallJoint = Win32API.new(dllName, "CreateBallJoint", ['P','N','N'], 'N')
		@newtonCreateJoint = Win32API.new(dllName, "CreateJoint", ['P','P','P','N','N','P'], 'N')

        @newtonSetJointData = Win32API.new(dllName, "setJointData", ['P','P'], 'V')

        @newtonSetJointRotation = Win32API.new(dllName, "setJointRotation", ['P','P'], 'V')
        @newtonSetJointPosition = Win32API.new(dllName, "setJointPosition", ['P','P'], 'V')
        @newtonSetJointAccel = Win32API.new(dllName, "setJointAccel", ['P','P'], 'V')
        @newtonSetJointDamp = Win32API.new(dllName, "setJointDamp", ['P','P'], 'V')
		
		@newtonGetBodyCollision = Win32API.new(dllName, "GetBodyCollision", ['P','PP','N'], 'N')
		@newtonSetBodyCenterOfMass = Win32API.new(dllName, "SetBodyCenterOfMass", ['P','P'], 'V')
		@newtonBodySetFreeze = Win32API.new(dllName, "BodySetFreeze", ['P','N'], 'V')


		@newtonMagnetAdd = Win32API.new(dllName, "MagnetAdd", ['P'], 'N')
		@newtonBodySetMagnet = Win32API.new(dllName, "BodySetMagnet", ['N','N','P'], 'V')
		@newtonMagnetMove = Win32API.new(dllName, "MagnetMove", ['N','P'], 'V')

		@newtonAddGlobalForce = Win32API.new(dllName, "addGlobalForce", ['P','P','N','N','N'], 'N')
        
        @newtonSetupBouyancy=Win32API.new(dllName, "setupBouyancy", ['P','P','P'], 'V')
        #(dFloat* plane,dFloat* gravity,dFloat density,dFloat linearViscosity,dFloat angularViscosity)

        #CGlobalForce *addGlobalForce(float *location,[float strength,float range,float falloff],
		#int duration,int delay,int rate)
        @newtonAddImpluse=Win32API.new(dllName, "addImpulse", ['P','P','P'], 'V')
        @newtonReadJoystick=Win32API.new(dllName, "readJoystick", ['PP'], 'N')
        #int readJoystick(dFloat *data)
        #addImpulse(NewtonBody *body,dFloat* direction)

		@newtonInit.Call()
		
		@DynamicObjectList=Array.new
		@DynamicObjectResetPositions=Array.new
		@DynamicObjectBodyRef=Array.new
		@AllMagnets=Array.new
		@AllJoints=Array.new
		@AllJointChildren=Array.new	
		@AllCollisionEntities=Array.new

        @controlledJoints=[]

		@CursorMagnet=@newtonMagnetAdd.Call([0,0,0].to_a.pack("f*"))
		@AllMagnets.push(@CursorMagnet)
########
##### start of new init routine.
#version checking
SketchyPhysics.checkVersion
#####
#######

     Sketchup.active_model.start_operation "SketchyPhysics simulation"
        
        checkModelUnits()
        
		#parse and set physics constants.
        dict=Sketchup.active_model.attribute_dictionary("SPSETTINGS")
        if(dict==nil)
            setDefaultPhysicsSettings()
            dict=Sketchup.active_model.attribute_dictionary("SPSETTINGS")
        end
        if(dict!=nil)
            dict.each_pair { | key, value | 
                @newtonCommand.Call("set",key+" "+value.to_s)
                #puts key+"="+value
            }
           
        end

        @frameRate=Sketchup.active_model.get_attribute( "SPSETTINGS", "framerate", 3)

        #(dFloat* plane,dFloat* gravity,dFloat density,dFloat linearViscosity,dFloat angularViscosity)
        
        dumpCollision()

        #Sketchup.active_model.commit_operation() 
		
        #Sketchup.active_model.start_operation "SketchyPhysics sim"

		#puts "Finding joints"
		Sketchup.active_model.entities.each { | ent | 
			if (ent.class== Sketchup::Group)
                type=ent.get_attribute("SPJOINT","type",nil)
				if(type=="magnet")
                    strength=ent.get_attribute("SPJOINT","strength",0.0)
                    range=ent.get_attribute("SPJOINT","range",0.0)
                    falloff=ent.get_attribute("SPJOINT","falloff",0.0)
                    duration=ent.get_attribute("SPJOINT","duration",0)
                    delay=ent.get_attribute("SPJOINT","delay",0)
                    rate=ent.get_attribute("SPJOINT","rate",0)
                 
					@newtonAddGlobalForce.Call(ent.transformation.origin.to_a.pack("f*"),
                            [strength,range,falloff].pack("f*"),duration,delay,rate)
				elsif(type!=nil)
					@AllJoints.push(ent)	#save joints for later processing.
					#puts "found joint "+ent.entityID.to_s
				else
					ent.entities.each { | gent |
					if (gent.class== Sketchup::Group)
						if(gent.get_attribute("SPJOINT","type",nil)!=nil)
							gent.set_attribute("SPOBJ","body",ent.get_attribute("SPOBJ","body",nil))
							@AllJoints.push(gent)	#save joints for later processing.
						end
					end
					}
				end
			end
		}
        
            #init control sliders.
        initJointControllers()
			
			#
			#now create joints
			#
            
		Sketchup.active_model.selection.clear
		@AllJointChildren.each{|ent|
            jnames=JointConnectionTool.getParentJointNames(ent)
            jnames.each{|jointParentName|
                joint=nil
				@AllJoints.each{|j|
					if(j.get_attribute("SPJOINT","name","none")==jointParentName)
						joint=j
						break;
					end
				}
                
                if(joint!=nil)
                    jointType=joint.get_attribute("SPJOINT","type",nil)
                    #puts "created "+joint.get_attribute("SPJOINT","name","error")					
                    defaultJointBody=0
                    
                    jointChildBody=FindBodyWithID(ent.entityID)	
                    
                    #TODO.this might not be needed.
                    jointChildBody=0 if jointChildBody==nil
                
                    if(joint.parent.class==Sketchup::ComponentDefinition)
                        pgrp=joint.parent.instances[0]

                        defaultJointBody=pgrp.get_attribute("SPOBJ", "body",0)
                        parentXform=pgrp.transformation
                    else
                        parentXform=Geom::Transformation.new
                    end
                    
                    xform=parentXform*joint.transformation
                    
                    limits=Array.new
                    limits.push(joint.get_attribute("SPJOINT","min",0))
                    limits.push(joint.get_attribute("SPJOINT","max",0))

                    limits.push(joint.get_attribute("SPJOINT","accel",0))
                    limits.push(joint.get_attribute("SPJOINT","damp",0))

                    limits.push(joint.get_attribute("SPJOINT","rate",0))
                    limits.push(joint.get_attribute("SPJOINT","range",0))
                        
controller=joint.get_attribute("SPJOINT","controller",nil)
controller=nil if(controller=="")
#puts controller

if(controller==nil)
    jointType="hinge" if(jointType=="servo")
    jointType="slider" if(jointType=="piston")
#puts "demoted joint"
else
    if(jointType=="hinge")
        jointType="servo"
        if(limits[0]==0 && limits[1]==0)
            limits[0]=-180.0
            limits[1]=180.0
        end            
    elsif(jointType=="slider")
        jointType="piston"
        puts limits
    end
puts "promoted joint "+jointType
end    

                    pt=Geom::Point3d.new(0,0,1)
                    pt.transform!(xform)
                    pinDir=xform.zaxis.to_a+xform.yaxis.to_a
                    pinDir.flatten!
                    #puts pinDir
                    jnt=@newtonCreateJoint.Call(jointType,xform.origin.to_a.pack("f*"),
                        pinDir.pack("f*"),
                        jointChildBody,defaultJointBody, 
                        limits.pack("f*") )	
                            #if we got back a jointptr its a potentialy controlled joint.
                    if(jnt!=0)
                        joint.set_attribute("SPJOINT","jointPtr",jnt)
                        
                            #handle controlled joints.
#                        controller=joint.get_attribute("SPJOINT","controller","")
                        if(controller!=nil)
                            @controlledJoints.push(joint)
                            value=limits[0]+((limits[1]-limits[0])/2)
                     value=0.5# utb 0.5 jan 7.
                            createController(controller,value,0.0,1.0)
                        end
                    end
                end

                
            }
            
            setupCameras()
            
			#~ jn=0
			#~ c=ent.get_attribute("SPOBJ","numParents",0)
			#~ while(ent.get_attribute("SPOBJ","jointParent"+jn.to_s,nil)!=nil)
			
				#~ child=ent
				#~ #puts "child.parent"+child.parent.to_s
				
				#~ jointName="jointParent"+jn.to_s
				#~ jointParentName=ent.get_attribute("SPOBJ",jointName,nil)
				#~ #jointParentName=ent.get_attribute("SPJOINT","name",nil)

				#~ joint=nil
				#~ @AllJoints.each{|j|
					#~ if(j.get_attribute("SPJOINT","name","none")==jointParentName)
						#~ joint=j
						#~ break;
					#~ end
				#~ }
				#~ #puts "class "+ joint.class.to_s
				#~ if(joint!=nil)
					#~ jointType=joint.get_attribute("SPJOINT","type",nil)
                    #~ puts "created "+joint.get_attribute("SPJOINT","name","error")					
					#~ defaultJointBody=0
					
					#~ jointChildBody=FindBodyWithID(ent.entityID)	
				
					#~ if(joint.parent.class==Sketchup::ComponentDefinition)
						#~ pgrp=joint.parent.instances[0]

						#~ defaultJointBody=pgrp.get_attribute("SPOBJ", "body",0)
						#~ parentXform=pgrp.transformation
					#~ else
						#~ parentXform=Geom::Transformation.new
					#~ end
					
					#~ xform=parentXform*joint.transformation
					
					#~ limits=Array.new
					#~ limits.push(joint.get_attribute("SPJOINT","min",0))
					#~ limits.push(joint.get_attribute("SPJOINT","max",0))

					#~ limits.push(joint.get_attribute("SPJOINT","accel",0))
					#~ limits.push(joint.get_attribute("SPJOINT","damp",0))

					#~ limits.push(joint.get_attribute("SPJOINT","rate",0))
					#~ limits.push(joint.get_attribute("SPJOINT","range",0))

					#~ pt=Geom::Point3d.new(0,0,1)
					#~ pt.transform!(xform)
                    #~ pinDir=xform.zaxis.to_a+xform.yaxis.to_a
                    #~ pinDir.flatten!
                    #~ #puts pinDir
					#~ jnt=@newtonCreateJoint.Call(jointType,xform.origin.to_a.pack("f*"),
						#~ pinDir.pack("f*"),
						#~ jointChildBody,defaultJointBody, 
						#~ limits.pack("f*") )	
                            #~ #if we got back a jointptr its a potentialy controlled joint.
                        #~ if(jnt!=0)
                            #~ joint.set_attribute("SPJOINT","jointPtr",jnt)
                            
                                #~ #handle controlled joints.
                            #~ controller=joint.get_attribute("SPJOINT","controller","")
                            #~ if(controller!="")
                                #~ @controlledJoints.push(joint)
                                #~ value=limits[0]+((limits[1]-limits[0])/2)
                                #~ value=0.5
                                #~ createController(controller,value,0.0,1.0)
                            #~ end
                        #~ end
                    

				#~ end
				#~ jn=jn+1
			#~ end
		}

        #handleAttachments()
        showControlPanel()
    @startTime=Time.now    
#path = Sketchup.find_support_file("c813.wav", "plugins\\SketchyPhysics\\Sounds\\")
#UI.play_sound(path) if(path!=nil) 
	end
    def attachBodies(parent,child)
            #find parent object by unique Id.
        parentBodyID=parent.get_attribute("SPOBJ","body",nil)
           #puts "parentGroup:#{parentID} #{parentGroup}"
        if(parentBodyID!=nil)
            #childName=child.name
            childBodyID=child.get_attribute("SPOBJ","body",nil)
            
            jointType=child.get_attribute("SPOBJ","jointtype","error")

    #puts("Attaching #{child.name}(id:#{childBodyID} grp:#{child}) to #{parent.name}(id:#{parentBodyID} grp:#{parent}) joint:#{jointType}")
            #puts("bodies P:#{parentBodyID} C:#{childBodyID}")
        #find group by unique id.
            end
#return

            #pivot location
        #xform=parentXform*joint.transformation
xform=child.transformation
            #pin direction
        pinDir=xform.zaxis.to_a+xform.yaxis.to_a
        pinDir.flatten!
        
            #joint settings.
        limits=[0,0,0,0,0,0] #min,max,accel,damp,rate,range
#if servo/piston and no controller
#demote to hinge/slider
        jnt=@newtonCreateJoint.Call(jointType,xform.origin.to_a.pack("f*"),
            pinDir.pack("f*"),
            childBodyID,parentBodyID, 
            limits.pack("f*") )	
                #if we got back a jointptr its a potentialy controlled joint.
            if(jnt!=0)
                joint.set_attribute("SPJOINT","jointPtr",jnt)
                
                    #handle controlled joints.
                controller=joint.get_attribute("SPJOINT","controller","")
                if(controller!="" && controller!=nil)
                    @controlledJoints.push(joint)
                    value=limits[0]+((limits[1]-limits[0])/2)
                    value=0.5
                    createController(controller,value,0.0,1.0)
                end
            end
    end

	def handleAttachments()
		@DynamicObjectList.each {|body| 
            #parentName=body.get_attribute("SPOBJ","parentname",nil)
            parentID=body.get_attribute("SPOBJ","parentid",nil)
            if(parentID!=nil)
                parent=findUniqueObject(parentID)
                #raise if(parent==nil)
                attachBodies(parent,body)
            end
            }
        
    end

    def updateControlledJoints()
        
    #~ if(@thrustBody!=nil)
        #~ #puts "thrust"
        #~ thrust=[0,0,5.0*$controlSliders["joyLY"].value]
        #~ thrust=@thrustEnt.transformation.zaxis
        #~ thrust.length=0.5+($controlSliders["joyLY"].value*2)
        #~ #thrust.length=$controlSliders["joyLY"].value*-35.0
        
        #~ @newtonAddImpluse.Call(@thrustBody,thrust.to_a.pack("f*"),@thrustEnt.transformation.origin.to_a.pack("f*"))
    #~ end


    @joyDataBuffer=Array.new.fill(0.chr,0..8*4).join();
        if(@newtonReadJoystick.call(@joyDataBuffer)>0)
            joyData=@joyDataBuffer.unpack('f*')
            $controlSliders["joyLX"].value=0.5+(joyData[0]/2) if($controlSliders["joyLX"]!=nil)
            $controlSliders["joyLY"].value=0.5+(joyData[1]/2) if($controlSliders["joyLY"]!=nil)
            $controlSliders["joyRX"].value=0.5+(joyData[2]/2) if($controlSliders["joyRX"]!=nil)
            $controlSliders["joyRY"].value=0.5+(joyData[3]/2) if($controlSliders["joyRY"]!=nil)           
        end
        #int readJoystick(dFloat *data)
        
        @controlledJoints.each{|joint| 

            controller=joint.get_attribute("SPJOINT","controller","")

            if(controller.index("oscillator"))
                vals=controller.split(',')
                rate=vals[1].to_f
                inc=(2*3.141592)/rate
                pos=Math.sin(inc*(@frame))
                $controlSliders[controller].value=(pos/2.0)+0.5
            end

            value=$controlSliders[controller].value
            if(value==nil)
                raise "controller failure"
            end
            
            case joint.get_attribute("SPJOINT","type",nil) 
                when "oscillator"
                    rate=joint.get_attribute("SPJOINT","rate",100.0)
                    rate=rate/value
                    inc=(2*3.141592)/rate
                    pos=Math.sin(inc*(@frame))
                    #puts pos
                    @newtonSetJointPosition.Call(joint.get_attribute("SPJOINT","jointPtr",0),[pos].pack("f*"))
                when "servo","hinge"
                    @newtonSetJointRotation.Call(joint.get_attribute("SPJOINT","jointPtr",0),[value].pack("f*"))
                when "piston","slider"
                    @newtonSetJointPosition.Call(joint.get_attribute("SPJOINT","jointPtr",0),[value].pack("f*"))
                when "motor"
                    maxAccel=joint.get_attribute("SPJOINT","maxAccel",0)
                    accel=(value)*maxAccel
                    @newtonSetJointAccel.Call(joint.get_attribute("SPJOINT","jointPtr",0),[accel].pack("f*"))
                    #puts pos
           end
        }
    end
    
    def setupCameras()
        desc=Sketchup.active_model.pages.selected_page.description.downcase if(Sketchup.active_model.pages.selected_page!=nil)
        return if (desc==nil)
        
        sentences=desc.split('.')
        sentences.each{|l|
            words=l.split(' ')
            if(words[0]=='camera')
                @cameraTarget=FindGroupNamed(words[2]) if(words[1]=='track')
                @cameraParent=FindGroupNamed(words[2]) if(words[1]=='follow')
                puts @cameraTarget
            end
            }
    end
    
	def nextFrame(view)
#inTime=Time.now()

		totalTime = Time.now - @startTime
		elapsedTime=Time.now-@@lastTime
		lastTime=Time.now
		if @@bPause==true
			view.show_frame
			return true
		end
#updateTime=Time.now()
        updateControlledJoints()

		@newtonRequestUpdate.Call(@frameRate)
#updateTime=Time.now()-updateTime       
        if(@cameraParent!=nil)
            cameraPreMoveOffset=Sketchup.active_model.active_view.camera.eye-@cameraParent.bounds.center
        end            
#fetchTime=Time.now()

			#fetch positions for all moving objects.
		dat=Array.new.fill(0.chr,0..16*4).join
		while (id=@newtonFetchSingleUpdate.Call(dat))!=0xffffff 
			matrix=dat.unpack("f*")
			instance=@DynamicObjectList[id]
			if instance!=nil
				dest=Geom::Transformation.new(matrix)
                #reapply the scaling
#################
#TODO: This is slow. Cache scale in obj
                dest=dest*extractScaleFromGroup(instance)
#################
#################
				#oldTransform=instance.transformation.to_a
				instance.move!(dest)
				
				if false #bDoRecord	#do record
					mask=0
					output=Array.new
                        #update matrix to include scaling.
                    matrix=dest.to_a            
					if(@frame==0) #if bKeyframe
						matrix.each_index{|di|
							mask=mask|(1<<di)
							output.push(matrix[di])
						}
						#bKeyframe=false
					else
						matrix.each_index{|di|
							#only save delta (changes)
							if matrix[di]!=oldTransform[di]
								mask=mask|(1<<di)
								output.push(matrix[di])
							end
							}
					end
					ps=[mask].pack("L")
					if(mask!=0)
						ps=ps+output.pack("d*")
					end
					ps.gsub!(Regexp.compile("\n"),"whank")# stupid bug (i think) makes \n unable to be packed.
					ps=ps.to_a.pack('u*')
					instance.set_attribute "SRPAnimation", @frame.to_s, ps;
				end
			end
		end 
#fetchTime=Time.now()-fetchTime     

        if(@cameraTarget!=nil)
            camera = Sketchup.active_model.active_view.camera
            camera.set(camera.eye, @cameraTarget.bounds.center, Geom::Vector3d.new(0, 0, 1))
        end
        if(@cameraParent!=nil)
            camera = Sketchup.active_model.active_view.camera
            dest=@cameraParent.bounds.center+cameraPreMoveOffset
            camera.set(dest, dest+camera.direction, Geom::Vector3d.new(0, 0, 1))
        end

		
		@frame=@frame+1
        
#@totalTime=0 if @totalTime==nil
#outTime=Time.now()-inTime
#@totalTime+=outTime#+(@totalTime/@frame).to_s
#$outputbuf.push((@frame.to_s+"T:#{outTime} U:#{updateTime} F:#{fetchTime} ")) if (@frame%2==0)
		view.show_frame
		return true
	end
$outputbuf=[]
	def resetSimulation
        #puts "Phys reset"
         et=Time.now-@startTime
puts $outputbuf
$outputbuf=[]
        puts ["Time",et,"Frames",@frame,"Fps",@frame/et].inspect
        
        @newtonStop.Call()
		@DynamicObjectList.each_index {|ci| @DynamicObjectList[ci].move!(@DynamicObjectResetPositions[ci]) }
        Sketchup.active_model.abort_operation
                
		#Sketchup.active_model.start_operation "Physics end"
		#@DynamicObjectList.each_index {|ci| @DynamicObjectList[ci].transformation=@DynamicObjectList[ci].transformation }
		#@DynamicObjectList.each_index {|ci| @DynamicObjectList[ci].transformation=Geom::Transformation.new([0,0,0]) }
		#Sketchup.active_model.commit_operation
        #Sketchup.undo
        #sketchup.undo
	end

    def commitSimulation
        puts "Phys commit"
        Sketchup.active_model.abort_operation
		Sketchup.active_model.start_operation "Physics simulation"
        @DynamicObjectList.each_index {|ci| 
            xform=@DynamicObjectList[ci].transformation; 
            @DynamicObjectList[ci].move!(@DynamicObjectResetPositions[ci])
            @DynamicObjectList[ci].transformation=xform
            }
		Sketchup.active_model.commit_operation
    end

	##################################################start of tool################################################
	# The stop method will be called when SketchUp wants an animation to stop
	# this method is optional.
	def stop
		#puts "stop called"
		@bWasStopped=true
	end

	# The activate method is called when a tool is first activated.  It is not
	# required, but it is a good place to initialize stuff.
	def activate
		#puts "activate called"
        Sketchup::set_status_text "Click and drag to move. Press CTRL while dragging to lift."	
	end

	def deactivate(view)
		#resetSimulation
        @@bPause=true
        physicsReset()
        closeControlPanel()
		puts "deactivate called"
	end

    def onCancel()
        deactivate(0)
    end
        
	def suspend(view)
	    #puts "suspend called"
	end
	def resume(view)
		#puts "resume called"
        Sketchup::set_status_text "Click and drag to move. Press SHIFT while dragging to lift."	
	end

	@pickedBody=nil
	@magnetLocation=nil
	def onMouseMove(flags, x, y, view)

        @mouseX=x
        @mouseY=y
        
		if(@CursorMagnetBody==nil)
			return
		end

		ip = Sketchup::InputPoint.new
		ip.pick view, x, y
		if( ip.valid? )
            if(@pickedBody!=nil)
                if(@shiftDown)
                    #project the input point on a plane described by our normal and center.
                    
                    #center=@pickedBody.transformation.origin
                    #normal=view.camera.eye-center
                    #ep=Geom.intersect_line_plane([view.camera.eye,ip.position], [center,normal])

                    ep=Geom.intersect_line_plane([view.camera.eye,ip.position], [@attachWorldLocation,view.camera.zaxis])
                    @attachWorldLocation=ep
                    pos=ep
                else
                    ep=Geom.intersect_line_plane([view.camera.eye,ip.position], [@attachWorldLocation,Z_AXIS])
                    pos=ep
                    @attachWorldLocation=ep
                end
                @magnetLocation=pos
                #puts pos
        
                if(@CursorMagnet!=nil)
                    @newtonMagnetMove.Call(@CursorMagnet,pos.to_a.pack("f*"))
                end
            end
		end
	    
	end

    def getMenu(menu)
        #puts menu
        #menu.add_separator
        
        ph=Sketchup.active_model.active_view.pick_helper
		ph.do_pick @mouseX,@mouseY
		ent=ph.best_picked
        Sketchup.active_model.selection.add ent
         #return
        menu.add_item("Camera track"){
            #ent=Sketchup.active_model.selection[0]
            @cameraTarget=ent
        }
        menu.add_item("Camera follow"){
            #ent=Sketchup.active_model.selection[0]
            @cameraParent=ent
        }
        menu.add_item("Camera clear"){
            @cameraTarget=nil
            @cameraParent=nil
        }
        menu.add_item("Copy body"){
            copyBody(ent)
        }
    end 

	def onMButtonDown(flags, x, y, view)
		ph=view.pick_helper
		ph.do_pick x,y
		ent=ph.best_picked
        puts "safda"
        #@mbuttonDownCount=0
        @cameraTarget=ent
    end
    def onRButtonDown(flags, x, y, view)
        puts "rbutton"
        ent=ph.best_picked
        puts ent
    end
    def onRButtonUp(flags, x, y, view)
        puts "rbutton"
        ent=ph.best_picked
        puts ent
    end
    def onLButtonDoubleClick(flags, x, y, view)
        		ph=view.pick_helper
		ph.do_pick x,y
		ent=ph.best_picked
#puts @shiftDown
        #~ if(win32GetKeyState(Win32_VK_LSHIFT))#&&win32GetKeyState(Win32_VK_LCONTROL))
            #~ @thrustEnt=ent
            #~ @thrustBody=FindBodyWithID(ent.entityID)
            #~ #puts "launch"
           #~ return 
        #~ end

        if(!win32GetKeyState(Win32_VK_LSHIFT))
            direction=ent.transformation.origin-view.camera.eye

            direction.normalize!
            
            direction[0]*=3.0
            direction[1]*=3.0
            direction[2]=15.0
#            kick=[0,0,15.0]
            kick=direction
            body=FindBodyWithID(ent.entityID)	
            if(body!=nil)
                @newtonAddImpluse.Call(body,kick.to_a.pack("f*"),ent.transformation.origin.to_a.pack("f*"))

#        path = Sketchup.find_support_file("misc127.wav", "plugins\\SketchyPhysics\\Sounds\\")
#        UI.play_sound(path) if(path!=nil)
            end
        else
            ip=view.inputpoint x,y
            #@pickedBody=ent
            copyBody(ent)
            #createBullet(ip.position,[0.0,0.0,100.0],10,10)  
            #@pickedBody=nil
#path = Sketchup.find_support_file("misc128.wav", "plugins\\SketchyPhysics\\Sounds\\")
#UI.play_sound(path) if(path!=nil)
        end
return;
        #@mbuttonDownCount=0
        @cameraTarget=ent
        if(@ctrlDown || @shiftDown)
            @cameraParent=ent
        else
            @cameraParent=nil
        end
    end
    
	def onLButtonDown(flags, x, y, view)
		ph=view.pick_helper
		ph.do_pick x,y
		ent=ph.best_picked
        
        #ip=view.inputpoint x,y
        #puts ip.position
        #@newtonAddGlobalForce.Call(ip.position.to_a.pack("f*"),[-20000,0,0].pack("f*"),20,0,0)
        #@newtonAddGlobalForce.Call([100,-100,0].pack("f*"),[1000000,0,0].pack("f*"),100,60,0)
        #@newtonAddGlobalForce.Call([100,100,0].pack("f*"),[100000,0,0].pack("f*"),50,0,0)
        
        #        @newtonAddGlobalForce.Call([100,-100,0].pack("f*"),[-5000000,0,0].pack("f*"),100,160,0)
                
                
		if(@CursorMagnetBody!=nil)
			@newtonBodySetMagnet.Call(@CursorMagnetBody,0,0)
			@CursorMagnetBody=nil
			Sketchup.active_model.selection.clear
		end
		#puts ph.best_picked.class

		if(ent.class == Sketchup::ComponentInstance || ent.class==Sketchup::Group)
			Sketchup.active_model.selection.clear
			Sketchup.active_model.selection.add ent
			@pickedBody=ent
			
			gd=ent.entities.parent
			#puts "gd.bounds.center "+ gd.bounds.center.to_s
			@DynamicObjectList.each_index{|doi| 
				if(@DynamicObjectList[doi]==ent)
					#transform input point into component space.
					ip=view.inputpoint x,y
					#puts ip.position

					#puts "center"				
					#puts @pickedBody.entities[0].parent.bounds.center.to_s
					
					#puts @pickedBody.transformation.origin.to_s
					
			cdbounds=@pickedBody.entities[0].parent.bounds
			dsize=cdbounds.max-cdbounds.min
			
			cmass=Geom::Point3d.new(dsize.to_a)
			cmass.x/=2;cmass.y/=2;cmass.z/=2;
			
			#puts "center of mass"
			#puts cmass.to_s
			#puts cdbounds.center.class
			#puts cdbounds.center
			pcenter=Geom::Point3d.new(dsize.y/2,dsize.x/2,dsize.z/2)
			
			xlate=(Geom::Transformation.new(pcenter).inverse)
					
					#attach point is relative to center of mass. 
					#
					
					#@attachPoint=ip.position.transform(@pickedBody.transformation)
                    
                    ip=view.inputpoint x,y
					@attachPoint=ip.position.transform(@pickedBody.transformation.inverse)
                    #puts "attach"
                    #puts @attachPoint
                    
				    @attachWorldLocation=ip.position #used to calc movment planes.

#createBullet(ip.position,[0.0,0.0,100.0],10,10)

					@newtonBodySetMagnet.Call(@DynamicObjectBodyRef[doi],@CursorMagnet,@attachPoint.to_a.pack("f*"))
					@CursorMagnetBody=@DynamicObjectBodyRef[doi];
				end
				}
			onMouseMove(flags, x, y, view);	#force magnet location to update.
		end
		return
	end
	# onLButtonUp is called when the user releases the left mouse button
	def onLButtonUp(flags, x, y, view)
		@pickedBody=nil
		@magnetLocation=nil
		if(@CursorMagnetBody!=nil)
			@newtonBodySetMagnet.Call(@CursorMagnetBody,0,0)
			@CursorMagnetBody=nil
			Sketchup.active_model.selection.clear
		end
		
		#Sketchup::set_status_text ""
	end

	def draw(view)
		if(@pickedBody!=nil) 
            view.line_width=3
            view.drawing_color="red"
			view.draw_line(@attachPoint.transform(@pickedBody.transformation), @magnetLocation)

            view.drawing_color="blue"
            view.line_width=1
            #view.line_stipple = "-.-"
            tp=@magnetLocation.clone()
            tp.z=-100
			view.draw_line(tp, @magnetLocation)
		end
		if(@bWasStopped)
			Sketchup.active_model.active_view.animation = @@client
			@bWasStopped=false
		end
	end

	def onSetCursor()
		#cursor = UI::set_cursor(@@MagnetCursorIcon)
			#cursor = UI::set_cursor(@@cursorCount)
			cursor = UI::set_cursor(671)
			#if(cursor)
			#	@@delay=@@delay+1
			#	if(@@delay>100)
			#		@@delay=0
			#       @@cursorCount=@@cursorCount+1
			#	end
			#    #puts cursor
		#		puts @@cursorCount
		#	else
		#		@@cursorCount=@@cursorCount+1
		#	end
	end

    @ctrlDown=false
    @shiftDown = false
    def onKeyDown(key, rpt, flags, view)
        if( key == COPY_MODIFIER_KEY && rpt == 1 )
            @ctrlDown=true
        end

        if( key == CONSTRAIN_MODIFIER_KEY && rpt == 1 )
            @shiftDown = true
        end

    end

    def onKeyUp(key, rpt, flags, view)
        if( key == COPY_MODIFIER_KEY)
            @ctrlDown=false
        end

        if( key == CONSTRAIN_MODIFIER_KEY)
            @shiftDown = false
        end
    end

    @@cursorCount=0
	@@delay=0

end # class SketchyPhysicsClient
	##################################################end of tool################################################

def checkModelUnits
        model = Sketchup.active_model
        manager = model.options
        provider = manager[3]
        #puts provider.name
        #provider.each { |key, value| 
		#	puts ((key.to_s) +"="+(value.to_s)) 
		#	}

        provider["SuppressUnitsDisplay"]=true
        provider["LengthFormat"]=0

end


# Start the physics simulation
def startphysics
    
	if @@client==nil
		@@client=SketchyPhysicsClient.new
		Sketchup.active_model.active_view.animation = @@client
		
		#turn on physics grab tool
		Sketchup.active_model.select_tool @@client
            

	end	
end

def physicsTogglePlay()
    @@lastTime=Time.now
	if @@client==nil
		#checkModelUnits()
        

		startphysics()
		@@bPause=false
        return
    end
	if @@bPause
		@@bPause=false
	else
		@@bPause=true
	end
end
def physicsReset()
	if(@@client!=nil)
        
        Sketchup.active_model.active_view.animation = nil 
		@@client.resetSimulation
        @@client=nil
		Sketchup.active_model.select_tool nil
	end
end

def physicsRecord()
    #if recording
    #offer to save
    #continue or reset?
    #@bDoRecord=true
    #if not playing
    #start
    
end
def physicsStop()
	if @@client==nil
		return
	end
	@@bPause=true
	keypress = UI.messagebox "Save recorded animation?" , MB_YESNO, "Error"
	#Sketchup.active_model.commit_operation
	if (keypress == 6)
		# code to respond to YES
		Sketchup.active_model.active_view.animation = nil 
        
        @@client.commitSimulation
        @@client=nil
		Sketchup.active_model.select_tool nil

	end
    
end
#-----------------------------------------------------------------------------
# Add an physics menu items to the Tools menu
if( not file_loaded?("SketchyPhysicsTool.rb") )
   
   add_separator_to_menu("Help")
   SketchyPhysicsClient_menu = UI.menu("Help")
   SketchyPhysicsClient_menu.add_item("About SketchyPhysics") {UI.messagebox "SketchyPhysics2.0b1.\nhttp://code.google.com/p/sketchyphysics2\nCopyright 2008 C Phillips.\nSketchyPhysics uses the Newton Physics SDK"}
   #SketchyPhysicsClient_menu.add_item("Sketchy Observer"){watchSelection()}
 
end

#main UI setup
toolbar = UI::toolbar "SketchyPhysics"
#toolbar = UI::Toolbar.new "Sketchy Physics" if toolbar==nil
playcmd = UI::Command.new("Play") { physicsTogglePlay() } 


path = Sketchup.find_support_file "SketchyPhysics-PlayPauseButton.png", "plugins\\SketchyPhysics\\Images\\"
playcmd.small_icon = path
playcmd.large_icon = path
playcmd.tooltip = "Play/Pause physics simulation"
playcmd.status_bar_text = "Play/Pause physics simulation"
playcmd.menu_text = ">/|| Sketchyphysics"

toolbar.add_item playcmd

cmd = UI::Command.new("Reset") { physicsReset()	}

path = Sketchup.find_support_file "SketchyPhysics-RewindButton.png", "plugins\\SketchyPhysics\\Images\\"
cmd.small_icon = path
cmd.large_icon = path
cmd.tooltip = "Restore all items to original position"
cmd.status_bar_text = "Restore all items to original position"
cmd.menu_text = "Reset physics positions"
toolbar.add_item cmd

cmd = UI::Command.new("Record") { physicsStop()}

path = Sketchup.find_support_file "SketchyPhysics-StopButton.png", "plugins\\SketchyPhysics\\Images\\"

cmd.small_icon = path
cmd.large_icon = path
cmd.tooltip = "Save animation data"
cmd.status_bar_text = "Save animation data and reset"
cmd.menu_text = "Save animation"
#toolbar.add_item cmd
#toolbar.add_separator

    cmd = UI::Command.new("ShowUI") { togglePhysicsInspectorDialog() }
    path = Sketchup.find_support_file "SketchyPhysics-ShowUIButton.png", "plugins\\SketchyPhysics\\Images\\"

    cmd.small_icon = path
    cmd.large_icon = path
    #cmd.tooltip = "Show UI"

    cmd.status_bar_text = "Show UI Module"
    cmd.menu_text = "Show UI Module"
    toolbar.add_item cmd
    
    cmd = UI::Command.new("JointConnectionTool") { Sketchup.active_model.select_tool(JointConnectionTool.new()) }
    cmd.status_bar_text = "Joint Connector"
    cmd.tooltip = "Connect a joint to a surface, line, path, or group"
    #cmd.large_icon = cmd.small_icon = "images/JointConnector.png"
    cmd.large_icon = cmd.small_icon = Sketchup.find_support_file("JointConnector.png", "plugins\\SketchyPhysics\\Images\\")
    toolbar.add_item(cmd)


#
#toolbar.add_separator


#reloadcmd = UI::Command.new("Reload Scripts") { reload() }
#toolbar.add_item reloadcmd	

toolbar.show

@@client=nil
@@bPause=true
file_loaded("SketchyPhysicsTool.rb")
